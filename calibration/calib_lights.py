# auto-grad #
#import autograd.numpy as np
import numpy as np

# other #
import ipdb
import math
import matplotlib.pyplot as plt
import scipy.io as sio

# optimization #
from scipy.optimize import minimize as sci_min


def vec3d_norm(in_vec):
    '''
    return the norm-2 of a 3D vector
    '''
    return np.power(in_vec[0]**2 + in_vec[1]**2 + in_vec[2]**2, .5)


def vec3d_square_norm(in_vec):
    '''
    return the norm-2 of a 3D vector
    '''
    return in_vec[0]**2 + in_vec[1]**2 + in_vec[2]**2


def vec3d_cross(a, b):
    return np.asarray([a[1]*b[2]-a[2]*b[1],
        a[2]*b[0]-a[0]*b[2], a[0]*b[1]-a[1]*b[0]])


def fit_3d_plane(points_3d):
    '''
    fit 3d plane in the naive least-square sense
    inputs:
        points_3d - npt x 3 array, each row is a point location
    outputs: 
        a, b, d - the parameter of the 3d plane such that ax + by + c + d = 0
    '''
    npts = points_3d.shape[0]
    B_ = -points_3d[:,2]
    A_ = np.zeros((npts,3))
    A_[:, 0] = points_3d[:, 0]
    A_[:, 1] = points_3d[:, 1]
    A_[:, 2] = 1
    abd_ = np.linalg.pinv(A_).dot(B_)

    return abd_


def square_dist_var_sphere_center(sphere_centers, pos_LEDs, HL_centers, fc_c, sphere_R, if_return_intersec=False):
    '''
    Given the sphere center, LED psotions and the highlight position in the image,
    get the LED-to-reflected-light distance
    inputs: sphere_center - 1D array of 3 nLED: the light source positions
            pos_LEDs - nLED x 3 array: the LED positions 
            HL_centers - list of nSphere arrays, each array is nLED x 3: the normalized 
            image coordinates of the highlight centers
            fc_c - the focal length, normalized
            sphere_R - the physical dimension of the calibration sphere
            if_return_intersec
    outputs: dist_sum_LED2RL: the sum of squared distance between the LED positions to 
             the reflected light directions
    '''

    nSphere = len(HL_centers)
    nLED = pos_LEDs.shape[0]
    P_INT = []
    N_INT = []
    S_INT = []

    total_cost = 0.0
    for isphere in range(nSphere):
        S_intsec = np.zeros((nLED, 3)) 
        P_intsec = np.zeros((nLED, 3))
        hl_centers_c = HL_centers[isphere]
        sphere_center = np.zeros(3)
        sphere_center = sphere_centers[3*isphere : 3*isphere+3]

        for iled in range(nLED):
            u = hl_centers_c[iled,0]
            v = hl_centers_c[iled,1]
            xc = sphere_center[0]
            yc = sphere_center[1]
            zc = sphere_center[2]
            a_ = u**2 + v**2 + fc_c**2
            a_ = a_ * (sphere_R)**2
            b_ = 2 * sphere_R * (u* (u*zc - fc_c*xc) + v * (v*zc - fc_c*yc))
            c_ = (u* zc - fc_c*xc)**2 + (v* zc - fc_c*yc)**2 - (sphere_R * fc_c)**2  
            k_root = [(-b_ - np.power(b_**2 - 4*a_*c_, .5) ) /(2*a_),
                    (-b_ + np.power(b_**2 - 4*a_*c_, .5)) /(2*a_)] 

        
            nz_ = np.min(k_root)
            nx_ = (u * sphere_R * nz_ + u * zc - fc_c * xc ) / (fc_c * sphere_R)
            ny_ = (v * sphere_R * nz_ + v * zc - fc_c * yc ) / (fc_c * sphere_R)
        
            normal_intsec = np.asarray([nx_, ny_, nz_])
            point_intsec = np.asarray([xc, yc, zc]) + sphere_R * normal_intsec
        

#            print( type(normal_intsec) )

            norm_point_intsec =\
                    np.power(point_intsec[0]**2 + point_intsec[1]**2 + point_intsec[2]**2, .5)

            point_intsec_n_ = point_intsec / norm_point_intsec
            alpha_1 = 2 * norm_point_intsec * np.dot(normal_intsec, -point_intsec_n_) 

            # Then get the line direction passing through the intersection point and 
            # pointing to the light source
            s_intsec = alpha_1 * normal_intsec + point_intsec

            pt_ = pos_LEDs[iled, :]

            cross_pa_l_ = vec3d_cross(pt_ - point_intsec, s_intsec)
            dist_p2l_ = vec3d_norm(cross_pa_l_) / vec3d_norm(s_intsec)
            total_cost = total_cost + dist_p2l_**2

            if if_return_intersec is True:
                S_intsec[iled,:] = s_intsec
                P_intsec[iled,:] = point_intsec

        if if_return_intersec is True:
            P_INT.append(P_intsec)
            S_INT.append(S_intsec)


                    

        ###
#    nLines = Lines_V.shape[0]
#    sum_dist = 0.
#
#    for iline in range(nLines):
#        linev_ = Lines_V[iline, :]
#        linep_ = Pts_Lines[iline, :]
#        pt_l_dist_ = dist_Point2Line(v_Pt, linev_, linep_)**2
#        sum_dist = sum_dist + pt_l_dist_
#    return sum_dist
        ###
            
#    ipdb.set_trace()
    if if_return_intersec is False:
        return total_cost
    else:
        return total_cost, P_INT, S_INT


def E_calib_3d_var_sphere(sphere_centers, pos_LEDs, HL_centers, fc_c, sphere_R, fun_var_sphere):
    '''
    output: total_cost, grad_total_cost
    '''

    total_cost = square_dist_var_sphere_center(sphere_centers, pos_LEDs, HL_centers, fc_c, sphere_R)
    return total_cost, fun_var_sphere(sphere_centers)

def dist_Point2Line(point, line_v, line_p):
    '''
    get the point to line distance:
    input: 
        point - array of 3: the 3D location of the point
        line_v- array of 3: the direction of the line
        line_p- array of 3: one point on the line
    return p2l_dist - scalar: point to line distance
    '''
    pa_ = point - line_p
#    norm_line_v = np.power(line_v[0]**2 + line_v[1]**2 + line_v[2]**2, .5)
    return np.linalg.norm(np.cross(pa_, line_v)) / (np.linalg.norm(line_v) + np.finfo(np.float64).eps)
#    return np.linalg.norm(np.cross(pa_, line_v)) / ( norm_line_v + np.finfo(np.float64).eps)

def square_dist_Point2Line_sum(v_Pt, Lines_V, Pts_Lines):  
    '''
    Get the sum of the square point-to-line distances for all lines
    Inputs:
        v_Pt   - 3 elements array : the 3D location of the variable point 
        Lines_V - nlines x 3 array : the direction of the lines
        Pts_Lines - nlines x 3 array: the points passed through by the lines
    Return: 
        sum_dist - the sum of the squared norm2 distance from the point to the lines
    '''
    
    nLines = Lines_V.shape[0]
    sum_dist = 0.

    for iline in range(nLines):
        linev_ = Lines_V[iline, :]
        linep_ = Pts_Lines[iline, :]
        pt_l_dist_ = dist_Point2Line(v_Pt, linev_, linep_)**2
        sum_dist = sum_dist + pt_l_dist_
            
    return sum_dist


def E_calib_3d_pt(point, Lines_V, Pts_Lines, fun_grad_dist_sum):
    '''
    Get the sum of the square point-to-line distances for all lines
    Inputs:
        v_Pt   - 3 elements array : the 3D location of the variable point 
        Lines_V - nlines x 3 array : the direction of the lines
        Pts_Lines - nlines x 3 array: the points passed through by the lines
        fun_grad_dist_sum - functor of the gradient of the sum of squared distance
    Return: 
        sum_dist - the sum of the squared norm2 distance from the point to the lines
        grad_dist - the gradient of sum_dist wrt to the point location
    '''
    sum_dist = square_dist_Point2Line_sum(point, Lines_V, Pts_Lines)
    return sum_dist, fun_grad_dist_sum(point)



def E_calib_3d_test(point, line_v, line_p, fun_grad_dist):
    '''
    The energy for calibration 
    input: 
        point - array of 3: the 3D location of the point to evaluate
        line_v- array of 3: the direction of the line
        line_p- array of 3: one point on the line
        fun_grad_dist - the functor of the gradiant of the distance, get from
        autograd (HIPS)
    output: 
        dist_sum - the sum of the squared distance
        grad_dist_sum - the gradient of dist_sum w.r.t. point location
    '''

    dist_square_ = dist_Point2Line(point, line_v, line_p)**2
#    l_dist_square = lambda x:dist_Point2Line(x, line_v, line_p)
#    grad_dist_square = grad(l_dist_square)
    value_grad_dist_square_ = fun_grad_dist(point)

    # TODO: 
    # the distance and gradient of distance for ALL points #

    return dist_square_, value_grad_dist_square_


####################   
# Functions to calibrate per-image light source
# intensity:  In this setting, we have a checkerboard and Lambertian patch on
# the same  plane of the checkerboard.  We will calibrate the checkerboard to
# get the  plane postion&orientation.   Then we pick one/more points from the
# Lambertian  patch, of which we know the plane parameters.  Then we can get
# intersection  points where the line-of-sight rays passing through the picked
# points  intersecting with the Lambertian patch. The intersection point is the
# 3D points  P.     Given the light source positions S, the surface normal of
# the plane and assuming the albedo of the  Lambertian plane is one, we can get
# the light source intensity Se through:  I_i = Se_i * rho (S_i - P).dot(N) /
# (|S_i - P|^3) 
####################
def LED_Uniform_getIntec(img, calib_file, img_indx):
    '''
    Given the image, pick one point in the image, find out the 3D location 
    of the picked point 
    input:
        img - the given image, it should include the checkerboard, with
        the 3D corner locations in YYs[:,:, img_indx]. YYs is got from the
        camera calibration process 
        calib_file - the .mat file storing the calibration results and YYs
        YYs is the 3D location of the checker corners got from camera calibration
        img_indx - the image indx of img in the calibration sequence. 
        In other words, YYs[:,:, img_indx] is the 3D location of the corner points
    output:
        p_intsec, n_intsec - the 3D point location and surface normal of the 
        picked point 
    '''
    import scipy.io as sio
    mat_info = sio.loadmat(calib_file)

    # Get the calibration info #
    fc = mat_info['fc'][0][0] # the focal length
    cc = mat_info['cc'] # camera center 

    # Get the plane parameters #
    P3D_CORNERS = sio.loadmat('{}'.format(calib_file))['YYs']
    p3d_corners = P3D_CORNERS[:,:,img_indx].transpose() # each row is a point

    # Change the coordinate signs of S such that it confirms with the image coordinates k
    p3d_corners[:, 2] = -p3d_corners[:, 2]
    p3d_corners[:, 1] = -p3d_corners[:, 1]

    abd_ = fit_3d_plane(p3d_corners)
    plane_paras = np.asarray([abd_[0], abd_[1], 1, abd_[2]])

    PL_n_ = plane_paras[:3] / np.linalg.norm(plane_paras[:3])
    PL_n_[1] = -PL_n_[1]
    PL_n_[0] = -PL_n_[0]
    normal_intsec = PL_n_

    # Pick one point from the image #
    plt.figure()
    plt.imshow(img)
    X = plt.ginput(1, timeout = -1)
    X = X[0]
    plt.close()

    X_c = X - cc.flatten()
    norm_scalar = np.max(img.shape)
    X_c = X_c / norm_scalar
    fc_c = fc / norm_scalar

    u =  X_c[0]
    v = -X_c[1]

    z_s = -plane_paras[3] / ( plane_paras[0] / fc_c * u +\
        plane_paras[1] / fc_c *v + plane_paras[2])
    x_s =  np.abs(z_s) * u / fc_c
    y_s =  np.abs(z_s) * v / fc_c
    point_intsec = np.asarray([x_s, y_s, z_s]) 

    return point_intsec, normal_intsec, X

def LED_Uniform_getIntec_Rec(img, calib_file, img_indx, log_scale = 1,
        if_use_fixed_rect = False, fixed_rect = [0]):
    '''
    Pick a rect. Reterns the point_intsec, normal_intsec and Xs inside it
    Return: 
    Points_Intesec: nX x nY x 3 tensor 
    normal_intsec: 3 ele array 
    X_gird: list of 2 elements [X_coord, Y_coord] 
    X_coord / Y_coord : nX x nY array, the XY coordinates for points in rect.
    '''
    import scipy.io as sio
    mat_info = sio.loadmat(calib_file)

    # Get the calibration info #
    fc = mat_info['fc'][0][0] # the focal length
    cc = mat_info['cc'] # camera center 

    # Get the plane parameters #
    P3D_CORNERS = sio.loadmat('{}'.format(calib_file))['YYs']
    p3d_corners = P3D_CORNERS[:,:,img_indx].transpose() # each row is a point
    # p3d_corners[:, 2] = -p3d_corners[:, 2]
    # p3d_corners[:, 0] = -p3d_corners[:, 0]
    abd_ = fit_3d_plane(p3d_corners)
    plane_paras = np.asarray([abd_[0], abd_[1], 1, abd_[2]])

    PL_n_ = plane_paras[:3] / np.linalg.norm(plane_paras[:3])
    # PL_n_[1] = -PL_n_[1]
    # PL_n_[0] = -PL_n_[0]
    normal_intsec = PL_n_

    if normal_intsec[2] > 0:
        normal_intsec = -normal_intsec

    # Pick points from the image #
    if if_use_fixed_rect is True:
        X_rec = fixed_rect
        print('Use fixed X_rect')
    else:
        plt.figure()
        plt.title('pick the upper-left and lower-right corners of the rect')
        if log_scale ==1:
            plt.imshow(np.log(img))
        else:
            plt.imshow(img)
        plt.grid()
        X_rec = plt.ginput(2, timeout = -1)
        plt.close()

    X_rec = [np.asarray(x_rec).astype(np.int) for x_rec in X_rec]
    X_grid = np.meshgrid(np.arange(X_rec[0][0], X_rec[1][0]+1), np.arange(X_rec[0][1], X_rec[1][1]+1) )

    # Get the intersection points and normals at the intersection points #
    nX = (X_grid[0]).shape[1]
    nY = (X_grid[0]).shape[0]
    Points_Intsec = np.zeros((nY, nX, 3))
    for iY in range(nY):
        for iX in range(nX):
            X_per = [X_grid[0][iY, iX], X_grid[1][iY, iX]]
            X_c = X_per - cc.flatten()
            norm_scalar = np.max(img.shape)
            X_c = X_c / norm_scalar
            fc_c = fc / norm_scalar
            u =  X_c[0]
            v =  X_c[1]
            z_s = -plane_paras[3] / ( plane_paras[0] / fc_c * u +\
                plane_paras[1] / fc_c *v + plane_paras[2])
            x_s =  np.abs(z_s) * u / fc_c
            y_s =  np.abs(z_s) * v / fc_c
            point_intsec = np.asarray([x_s, y_s, z_s]) 
            Points_Intsec[iY, iX,:] = point_intsec

    return Points_Intsec, normal_intsec, X_grid


def LED_Uniform_getSEs( IMG_LEDs, S, calib_file, img_indx, if_return_debug_info = False,
        if_use_fixed_rect = False, fixed_rect = [[0, 0],[-1, -1]]):
    '''
    inputs:
    IMG_LEDs: the images captured with different LEDs turned on
    S: nLED x 3 array: the light source locations 
    calib_file: the .mat file storing the calibration info for the camera
    img_indx : the image indx of img in the calibration sequence. 
        In other words, YYs[:,:, img_indx] is the 3D location of the corner points
    outputs:
    SEs: the nX x nY x nLED array of light source intensities 
    '''

    # First get the intersection point and surface normal #
    P_intsec, n_intsec, X_grid= LED_Uniform_getIntec_Rec(IMG_LEDs[:,:,0], calib_file, img_indx,
            if_use_fixed_rect = if_use_fixed_rect, fixed_rect = fixed_rect)

    # Then get Se assuming albedo = 1 #
    nLED = S.shape[0]
    nX = X_grid[0].shape[1]
    nY = X_grid[0].shape[0]

    SEs = np.zeros((nY, nX, nLED))
    img_vs = []
    for iY in range(nY):
        for iX in range(nX):
            X = [X_grid[0][iY, iX], X_grid[1][iY, iX]]
            p_intsec = P_intsec[iY, iX, :]
            Se = np.zeros(nLED)
            img_vs.append( IMG_LEDs[np.int(X[1]), np.int(X[0]), :] )
            for iled in range(nLED):
                I_per = IMG_LEDs[np.int(X[1]), np.int(X[0]), iled]
                S_per = S[iled, :]
                shadingT = (S_per - p_intsec).dot(n_intsec) / (np.linalg.norm(S_per - p_intsec)**3)
                # Assuming albedo = 1 #
                Se[iled] = I_per / shadingT 
            SEs[iY, iX, :] = Se

    img_vs = np.vstack(img_vs)
    # ipdb.set_trace()
    if if_return_debug_info is True:
        return SEs, X_grid, img_vs, P_intsec, n_intsec
    else:
        return SEs, X_grid, img_vs

def LED_Uniform_getSe( IMG_LEDs, S, calib_file, img_indx):
    '''
    inputs:
    IMG_LEDs: the images captured with different LEDs turned on
    S: nLED x 3 array: the light source locations 
    calib_file: the .mat file storing the calibration info for the camera
    img_indx : the image indx of img in the calibration sequence. 
        In other words, YYs[:,:, img_indx] is the 3D location of the corner points
    outputs:
    Se: the array of light source intensities 
    '''

    # First get the intersection point and surface normal #
    p_intsec, n_intsec, X = LED_Uniform_getIntec(IMG_LEDs[:,:,0], calib_file, img_indx)
    # p_intsec = p_intsec / 1000.0 # such that unit is meter 

    # Then get Se assuming albedo = 1 #
    nLED = S.shape[0]
    print(nLED)
    Se = np.zeros(nLED)

    normT = np.zeros(nLED)
    for iled in range(nLED):
        I_per = IMG_LEDs[np.int(X[1]), np.int(X[0]), iled]
        # S_per = S[iled, :] / 1000.0 # such that unit is meter
        S_per = S[iled, :]
        shadingT = (S_per - p_intsec).dot(n_intsec) / (np.linalg.norm(S_per - p_intsec)**3)
        normT[iled] = np.linalg.norm(S_per - p_intsec)
        Se[iled] = I_per / shadingT 

    return Se, normT  


