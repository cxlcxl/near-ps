'''
nearPS module 
The initializer for the depth z 
'''

##################
# 
#   DONE  *) Blur the first guess of the depth map weighted by the confidence map
#
#   *) Better initialization & confidence map using the initial guess of the
#   surface normal:
#       The better initial depth map can be got by looking at the TWO positions
#       of pairs of light sources when the Nterm = 0 (more specifically, one
#       pair where the point-lightsource distances are same for two light
#       sources in the pair; one pair where s_t.dot(n) = 0, i.e. the direction
#       of light source change is perpendicular to the surface normal
#       direction) 
#
#   DONE *) Naive PS based on the initial guess to get surface normal, 
#      then use the surface normal to refine the initial z map
#
#   DONE  *) Wrap the initialization into a function 
#   
#   DONE *) Optimize for per-vertice depth given the initial guess from this module.
#      Can we get better estimations ? YES
##################

#import numpy as np
# import autograd.numpy as np
import numpy as np
import ipdb
import math
#from termcolor import colored
import scipy.sparse as sparse 


####################### 
# Functions for depth map filtering #


def gaussian(x, sigma):
    return (1.0 / (2 * math.pi * (sigma ** 2))) * np.exp(- (x ** 2) / (2 * sigma ** 2))


def apply_bilateral_filter(source, weightMap, filtered_image,
        local_gdist_patch, 
        x, y, filter_radius, sigma_i, sigma_s):

    local_wmap_patch = weightMap[x-filter_radius:x + filter_radius+1,
                                 y-filter_radius:y + filter_radius+1] 
    local_dmap_patch = source[x-filter_radius:x + filter_radius+1,
                                 y-filter_radius:y + filter_radius+1] 

    Wp = np.sum(local_wmap_patch * local_gdist_patch)
    i_filtered= np.sum(local_dmap_patch * local_wmap_patch * local_gdist_patch)

    i_filtered = i_filtered / Wp
    filtered_image[x][y] = i_filtered

def weighted_filter(source, weightMap, filter_radius , sigma_s, weight_thresh = .25 ):
    '''
    The weighted filter for the depth map 
    '''
    filter_radius = np.int(filter_radius)
    filtered_image = np.zeros(source.shape)
    local_wmap_patch = np.zeros((filter_radius*2+1, filter_radius*2+1))
    local_dmap_patch = np.zeros((filter_radius*2+1, filter_radius*2+1))

    X_, Y_ = np.meshgrid(np.arange(-filter_radius, filter_radius+1),
            np.arange(-filter_radius, filter_radius+1))
    local_gdist_patch = gaussian(np.sqrt(X_**2 + Y_**2), sigma_s)
    local_gdist_patch = local_gdist_patch / np.sum(local_gdist_patch)

    i = 0
    sigma_i = 0.0 # not sued 
    while i < len(source):
        j = 0
        while j < len(source[0]):
            if source[i][j] > 0 and weightMap[i][j] < weight_thresh:
                apply_bilateral_filter(source, weightMap, filtered_image,
                        local_gdist_patch, 
                        i, j, filter_radius, sigma_i, sigma_s)
            else:
                filtered_image[i][j] = source[i][j]

            j += 1
        i += 1
    return filtered_image

# END: functions for depth map filtering #
####################### 


def fit_line_2d(point):
    '''
    Fit a line in 2d in a least-square sense
    Assume the line pass through zero
    inputs:
        point - 2 array: each row is an observations
    outputs:
        paras - the parameters of the line
    '''
    point_n = point / np.linalg.norm(point)
    paras = np.zeros(3)

    x = point_n[0]
    y = point_n[1]

    if np.abs(x) < 0.0000001:
        paras[0] = 1.0
        paras[1] = -x / y
    else:
        paras[1] = 1.0
        paras[0] = -y / x

    paras[2] =  0.0

    paras = paras / np.linalg.norm(paras)

    return paras


def dist2_line2d(paras_line, points):
    '''
    Get the point-to-line distances
    inputs: paras_line - the parameters (a,b,c) of the 2d line 
            points - npts x 2 array: the 2D points locations
    outputs: p2l_dists - npts array: the point-to-line distances
    '''
    p2l_dists = np.abs(paras_line[0] * points[:,0] + \
            paras_line[1] * points[:,1] + paras_line[2])
    return p2l_dists


def init_z_naivePS(obs_It, S, points_3d, shadow_thresh = 0.1):
    '''
    Get the initial guess of the surface normals, using the naive photometric stereo 

    inputs:
    obs_It - npt x nLED array. The observed image intensities for all light source positions 
    S - nLED x 3 array. The positions of the the light sources 
    points_3d - npt x 3 array: the 3D positions of the observed points

    outputs:
    snormals - npt x 3 array: the surface normals for the points
    '''
#
    npt, nLED = obs_It.shape
    snormals = np.zeros((npt, 3))

    for ipt in range(npt):
        pt_pos_ = points_3d[ipt, :]
        Light_M_ = S - pt_pos_.reshape((1, 3))
        Light_M_norm = np.linalg.norm(Light_M_, axis=1)
        Light_M_ = Light_M_ / (Light_M_norm.reshape((Light_M_norm.shape[0],1))**3)

        pt_obs_ = obs_It[ipt, : ]
#        pt_pos_[1] = -pt_pos_[1]
        
        # shadow #
        id_valid  = pt_obs_ > shadow_thresh

        pt_snormal_ = np.linalg.pinv(Light_M_[id_valid,:]).dot(pt_obs_[id_valid])
        snormals[ipt, :] = pt_snormal_

    # normalize #
    snormals = snormals / np.linalg.norm(snormals, axis=1).reshape((npt,1))
        
    return snormals 

def initialize_z_Tterm_Nxy(obs_Idt, obs_It, S, St, KUV, z_candi, obs_weights): 
    '''
    Initialize the depth z by approximate I_dt as the T term 
    ie. 
    I_dt = s_t.dot(p) * It / [norm(s-p)**2]
    It is the raw image measurement

    inputs:
        obs_Idt - nLED-by-npts array. The I_dt terms for the variables

        obs_It - nLED x npts array. The raw image measurements It

        S, St - nLED x 3 arrays. The postions / change of positions of the light sources

        KUV - npts x 3 array. the kuv coordinates for the points

        z_candi - ncandi array. candidate z's 

        obs_weights - nLED x npts array. 
        The weights for each point under each lighting condition
        
    outputs:
        depth_init - npts array, the initialized depth map

        conf_m - npts array, the confidance score for each initialization
    '''
    nobs = obs_Idt.shape[0]
    npts = obs_Idt.shape[1]
    nz_candi = z_candi.shape[0]
    err_zcandi = np.zeros((nz_candi, npts))

    # For each candidate depth, we will construct a Tterm (same size as
    # obs_Idt), then compare that with the obs_Is 
    Tterm = np.zeros(obs_Idt.shape)
    for i_z in range(nz_candi):
        P_PIX = KUV * z_candi[i_z]

        # Get Tterm #
        for iobs in range(nobs):
            sp0 = np.reshape(S[iobs, :], (1,3)) - P_PIX 
            norm_sp0 = np.linalg.norm(sp0, axis = 1)
            st0 = St[iobs, :]
            stp0 = np.sum(st0.reshape((1,3)) * P_PIX, axis = 1)
            t_term_ = 3 * (stp0 * obs_It[iobs,:]) / (norm_sp0 **2) 
            Tterm[iobs, :] = t_term_

        err_zcandi[i_z, :] = np.sum((Tterm - obs_Idt)**2 * obs_weights, axis = 0)

    indx_depth_init = np.argmin(err_zcandi, axis=0)
    depth_init = [ z_candi[indx_] for indx_ in indx_depth_init ]
    depth_init = np.asarray(depth_init)
    conf_m = np.sum(np.abs(obs_Idt) * obs_weights, axis = 0)

    return depth_init, conf_m

def initialize_z_Tterm(obs_Is, obs_It, S, St, KUV, z_candi, obs_weights, use_ratio_img = False): 
    '''
    Initialize the depth z by approximate I_dt0 + I_dt1 as the sum of T terms
    inputs: 
        obs_Is - nobs-by-npts array. The observed sum of Idt's

        obs_It - a list of two elements. Each elements: same size as obs_Is
        The raw measured intensities at time t. More specifically, 
        obs_It[0] is the image intensities at time t for light source at one end of the line
        connecting the two light sources 
        obs_It[1] corresponds to the other end of the line.

        S, St - nLED x 3 arrays. The postions / change of positions of the light sources

        KUV - npts x 3 array. the kuv coordinates for the points

        z_candi - ncandi array. candidate z's 

        obs_weights - nobs x npts array. The weights for each point under each lighting condition

    outputs: 
        indx_depth_init - npts array. The index of the depth (in z_candi), for which 
        the difference between the Tterm and obs_Is is minimized 

        depth_init - npts array, the initialized depth map

        conf_m - npts array, the confidance score for each initialization
    '''

#    if use_ratio_img is True:
#        print(colored('initialize_z_Tterm(): Use ratio image to initialize z term !', 
#                'blue'))

    nobs = obs_Is.shape[0]
    npts = obs_Is.shape[1]
    nz_candi = z_candi.shape[0]
    err_zcandi = np.zeros((nz_candi, npts))

    # For each candidate depth, we will construct a Tterm (same size as
    # obs_Is), then compare that with the obs_Is 

    Tterm = np.zeros(obs_Is.shape)
    for i_z in range(nz_candi):
        P_PIX = KUV * z_candi[i_z]
        # Get Tterm #
        for iobs in range(nobs):
            sp0 = np.reshape(S[iobs, :], (1,3)) - P_PIX 
            sp1 = np.reshape(S[iobs + nobs, :], (1,3))  - P_PIX 
            norm_sp0 = np.linalg.norm(sp0, axis = 1)
            norm_sp1 = np.linalg.norm(sp1, axis = 1)
            st0 = St[iobs, :]
            st1 = St[iobs + nobs, :]
            stp0 = np.sum(st0.reshape((1,3)) * P_PIX, axis = 1)
            stp1 = np.sum(st1.reshape((1,3)) * P_PIX, axis = 1)
            t_term_ = 3 * (stp0 * obs_It[0][iobs,:] / (norm_sp0 **2) + \
                    stp1 * obs_It[1][iobs,:] / (norm_sp1 ** 2))

            if use_ratio_img is True:
                t_term_ = t_term_ / obs_It[0][iobs, :]

            Tterm[iobs, :] = t_term_

        err_zcandi[i_z, :] = np.sum((Tterm - obs_Is)**2 * obs_weights, axis = 0)

    indx_depth_init = np.argmin(err_zcandi, axis=0)
    depth_init = [ z_candi[indx_] for indx_ in indx_depth_init ]
    depth_init = np.asarray(depth_init)
    conf_m = np.sum(np.abs(obs_Is) * obs_weights, axis = 0)
    return indx_depth_init, depth_init, conf_m




class Initer:
    '''
    Initializer for the depth values 
    '''
    def __init__(self, IMGs, I_mask, Kcam_inv, S, fem_d = 1, scale = 1.0, perform_knn_pix = True):
        '''
        NOTE:
        For indexed variables, the order is in 'F' order, rather than
        the 'C' order 

        Vars    IMGs - the photometric image stacks 
                observations: nLED x nPts array: the observations for observed points
                I_mask - the mask image 
                Kcam_inv - the inverse of the intrinsic matric of the camera 
                S - nLED x 3 array: the light source positions 
                nPts - the number of observation points 
        '''
        self.IMGs = IMGs 
        self.I_mask = I_mask 
        self.K = Kcam_inv 
        self.S = S
        self.nLED = S.shape[0]
        self.use_ratio_img = False

        # Scale, used for multi-scale optimization #
        self.scale = scale
        # Intialize the observations, observations_dt, St etc.  #
        self.IndM_2d_grid()
        # FEM step
        self.fem_d = fem_d
        # Get the differential terms #
        self.est_St(fem_d)
        self.est_Idt(fem_d)

        self.img_dmap_init_Idt = [] 

        # Get KUV #
        UV = np.zeros((3, self.nPts))
        for ipt in range(self.nPts):
            pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
            indx_var = self.varIndx_Map[pt_irow, pt_icol]
            indx_ = np.ravel_multi_index((pt_irow, pt_icol),
                    dims = I_mask.shape, order = 'F')
            UV[:, indx_var] = self.UV_mask[:, indx_]
        self.KUV= self.K.dot(UV)

        
        # set # of candidate for depth #
        self.set_zcandi(dmin = 0, dmax = 15, n_candi = 50)

        # Get slic_segs #
        print('Getting SLIC super-pixels...')
        # For real data #
        self.get_slicSeg(nSeg = 1000, compactness = .5)
        # For simulation #
#        self.get_slicSeg(nSeg = 5000, compactness = 3)
        print('Done')
        print('')

        # For each pixel, find out the KNN pixels, then average them #
        if perform_knn_pix is True:
            print('KNN on each pixel ...')
            self.knn_spix(K = 64)
            print('Done')
            print('')

    def knn_spix(self, K = 225):
        print('KNN_spix(): K ={}'.format(K))
        w_sigma = .1 
        n_avg = K 

        # Get It and Idt for each super-pixel. Here, instead of using the raw
        # pixel values, we will approximate the pixel value for each point with
        # the mean value of the pixels sharing the same spix-index #
        self.seg_observations = np.zeros((self.nLED, self.nPts))
        self.seg_observations_dt = np.zeros((self.nLED, self.nPts))
        import matplotlib.pyplot as plt
        
        iseg_pre = -1 
        for ipt in range(self.nPts):

            if ipt%1000 ==0:
                print('ipt = {} of {}'.format(ipt, self.nPts))

            pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
            indx_var = self.varIndx_Map[pt_irow, pt_icol]
            seg_indx = self.slic_Indx[indx_var]

            # Taking the mean value is a bad idea. Once the spix includes some
            # 'outliers', then those outliers will push up/down the mean value.
#            self.seg_observations[:,indx_var] = np.mean(self.observations[:, 
#                    self.slic_Indx == seg_indx], axis=1)

            # Instead, we take the weighted average for each point #
            pix_indx_in_seg = self.slic_Indx == seg_indx
            adj_pix_observations = self.observations[:, pix_indx_in_seg]
            adj_pix_weights = adj_pix_observations - self.observations[:, indx_var].reshape((self.nLED,1)) 
            adj_pix_weights = adj_pix_weights / ((self.observations[:, indx_var] + 1e-9).reshape((self.nLED, 1)) )
            adj_pix_weights = np.sum(np.exp(-adj_pix_weights**2 / w_sigma), axis=0)

            # Only take the closest npix_avg points 
            if n_avg >= len(adj_pix_weights):
                idx_avg_ = np.argsort(adj_pix_weights)
            else:
                idx_avg_ = np.argsort(adj_pix_weights)[:-n_avg+1]
            adj_pix_weights[idx_avg_] = 0.0
            
            if np.sum(adj_pix_weights) == 0.0:
                adj_pix_weights = np.ones(len(adj_pix_weights)) / len(adj_pix_weights)
            else:
                adj_pix_weights = adj_pix_weights/ np.sum(adj_pix_weights)

            avg_signal = np.sum(adj_pix_weights.reshape((1, len(adj_pix_weights))) * adj_pix_observations, axis = 1)
            self.seg_observations[:, indx_var] = avg_signal

            # Smoothing #
            # Smoothing does not work either #
#            avg_signal = np.sum(adj_pix_weights.reshape((1, len(adj_pix_weights))) * adj_pix_observations, axis = 1)
#            avg_signal = smoothing.smooth(avg_signal, window_len = 5, window = 'blackman' )
#            self.seg_observations[:, indx_var] = avg_signal[2:-2] 

            # DEBUG: pause to look at the image profiles for clusters of points
#            if iseg_pre != np.int(seg_indx):
#                iseg_pre = seg_indx
#                print('iseg = {}'.format(seg_indx))
#                ipdb.set_trace()
#                print('debug #')

        # Re-estimate Idt #
        fem_d = self.fem_d
        for iled in range(self.nLED):
            jled_ = np.int(iled) - fem_d # previous LED
            kled_ = np.int(iled) + fem_d # next LED
            if jled_ < 0:
                jled_ = (jled_ + self.nLED) 
            if kled_ >= self.nLED:
                # kled_ = 0
                kled_ =  kled_ % self.nLED
            self.seg_observations_dt[iled, :] = \
                    (self.seg_observations[kled_, :] -\
                    self.seg_observations[jled_, :]) / np.float(fem_d*2)

    def smooth_observations(self, window_l= 9, polyorder=3):
        print('smoothing the signal ')
        observations_smooth = np.zeros(self.observations.shape)
        import scipy.signal as ssignal
#        import matplotlib.pyplot as plt
        for ipt in range(self.nPts):
            pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
            indx_var = self.varIndx_Map[pt_irow, pt_icol]
            observations_smooth[:,indx_var] = ssignal.savgol_filter(\
                    self.observations[:,indx_var], window_l, polyorder)
        print('done')
        self.observations = observations_smooth
#        ipdb.set_trace()
        self.est_Idt(self.fem_d)


    def get_slicSeg(self, nSeg= 1000, compactness = .1):
        '''
        Get the slic segmentation map
        '''
        mask_scalar = 10.0
        from skimage.segmentation import slic
        from skimage.segmentation import mark_boundaries
        slic_segs = slic(np.dstack((self.IMGs/self.IMGs.max(), 
                self.I_mask.astype(np.float)*mask_scalar) ), 
                compactness=compactness, n_segments= nSeg, multichannel = True)
#        slic_segs = slic(np.dstack(( np.sum(self.IMGs/self.IMGs.max(), axis=2), 
#                self.I_mask.astype(np.float)*mask_scalar) ), 
#                compactness=compactness, n_segments= nSeg, multichannel = True)
        self.img_slic_Indx = slic_segs

        # Get the slic indx for each point #
        self.slic_Indx = np.zeros(self.nPts)
        for ipt in range(self.nPts):
            pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
            indx_var = self.varIndx_Map[pt_irow, pt_icol]
            self.slic_Indx[indx_var] = self.img_slic_Indx[pt_irow, pt_icol] 
        self.nSlic_Seg = len(np.unique(self.slic_Indx))
        self.unique_slic_indx = np.unique(self.slic_Indx)
        self.ISEG_PIX_inmask = np.zeros(self.nPts)
        for ipt in range(self.nPts):
            pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
            indx_var = self.varIndx_Map[pt_irow, pt_icol]
            iseg_pix_inImage = self.slic_Indx[indx_var]
            iseg_pix_in_mask = np.nonzero(self.unique_slic_indx == iseg_pix_inImage)[0][0]
            self.ISEG_PIX_inmask[indx_var] = np.int(iseg_pix_in_mask)
         

    def set_zcandi(self, dmin, dmax, n_candi = 50):
        '''
        Set the # of z candidates 
        '''
        self.ncandi_z= n_candi
        self.z_candi = np.linspace(dmin, dmax, n_candi)


    def IndM_2d_grid(self):
        '''
        Re-organize the image data, such that the image intensities are in the
        variable 'observations': nLED x nPts
        '''
        from z2n import Zmap2Nmap as z2n
        dMap_dummy = np.zeros(self.I_mask.shape)

        UV_mask, IndxM_mask, IndxM_var, F_vars, UV_vars, F_local, z_nz, varIndx_Map \
                = z2n.IndM_2d_grid(self.I_mask, dMap_dummy, if_clockwise = False)

        varIndx_Map = varIndx_Map.astype(np.int64)
        self.varIndx_Map = varIndx_Map
        self.varLoc_multiIndx = np.nonzero(varIndx_Map+1) 

        self.IndxM_mask = IndxM_mask
        self.IndxM_var = IndxM_var
        # self.nPts = np.int(np.sum(self.I_mask)) # number of observed points 
        self.nPts = np.int(len(self.varLoc_multiIndx[0])) 

        # Scale KUV for multiscale optimization
        # ipdb.set_trace()
        UV_mask[:2, :] = UV_mask[:2, :] * self.scale
        UV_vars[:2, :] = UV_vars[:2, :] * self.scale

        self.UV_mask = UV_mask

        # Set the observations variblae #
        observations = np.zeros((self.nLED, self.nPts)) 
        for ipt in range(np.min([self.nPts, len(self.varLoc_multiIndx[0])])): 
            pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
            indx_var = self.varIndx_Map[pt_irow,pt_icol]
            observations[:, indx_var] = self.IMGs[pt_irow, pt_icol, :]
        self.observations = observations 

    def debug_spix_senergy_init(self):
        import matplotlib.pyplot as plt 
        from skimage.segmentation import mark_boundaries
        plt.figure()
        plt.imshow(self.img_dmap_spix_init_Idt_spixEnergy, cmap='jet', vmin= 0, vmax = self.z_candi.max() )
        X = plt.ginput(1)
        plt.close()
        pt_irow, pt_icol = np.int(X[0][1]), np.int(X[0][0])
        indx_var = self.varIndx_Map[pt_irow,pt_icol]
        indx_seginMask = np.int(self.ISEG_PIX_inmask[indx_var])

        # Get the modeled It, Idt #
        snorm_  = self.snorm_init[indx_var, :]
        pix_3d_ = self.KUV[:, indx_var] * self.spix_depth_init_Idt_spixEnergy[indx_seginMask] 
        pix_sp_ =  self.S - pix_3d_.reshape((1,3))
        rho_se_ = self.img_rhose_init[pt_irow, pt_icol]
        It_pix_modeled  = rho_se_ * pix_sp_.dot(snorm_)  / (np.linalg.norm(pix_sp_, axis=1)**3)
        Idt_nterm = self.St.dot(snorm_) * rho_se_ / (np.linalg.norm(pix_sp_, axis=1)**3)
        Idt_tterm = 3 * self.St.dot(pix_3d_) * self.seg_observations[:, indx_var]/ (np.linalg.norm(pix_sp_, axis=1)**2)
        Idt_pix_modeled = Idt_nterm + Idt_tterm

        plt.figure(figsize = (10,10))
        plt.subplot(3,1,1)
        err_v = np.sqrt(self.Err_SPix_zcandi[:, indx_seginMask]) / self.nLED
        plt.plot(self.z_candi, err_v)
        plt.ylim([np.min(err_v), np.max(err_v)])
        plt.grid()
        plt.title('Idt energy vs. z candi')

        plt.subplot(3,1,2)
        plt.title('knn obs(k) vs raw obs(r) vs. modeled (g) ')
        plt.plot(self.observations[:, indx_var], 'kx-')
        plt.plot(self.seg_observations[:, indx_var], 'rx--')
        plt.plot(It_pix_modeled, 'go--')
        plt.grid()

        plt.subplot(3,1,3)
        plt.title('knn obs(k) vs raw obs(r) vs. modeled (g) vs. nterm (b) vs. tterm (y) ')
        plt.plot(self.observations_dt[:, indx_var], 'kx-')
        plt.plot(self.seg_observations_dt[:, indx_var], 'rx--')
        plt.plot(Idt_pix_modeled, 'go--')
        plt.plot(Idt_nterm, 'bo--')
        plt.plot(Idt_tterm, 'yo--')
        plt.grid()

        plt.show()


    def debug_spix_init(self, npts_pick = 1):
        import matplotlib.pyplot as plt 
        from skimage.segmentation import mark_boundaries

        plt.figure()
        plt.imshow(self.img_dmap_spix_init_Idt, cmap='jet', vmin= 0, vmax = self.z_candi.max() )
        X = plt.ginput(npts_pick)
        plt.close()

        for ipix in range(npts_pick):
            pt_irow, pt_icol = np.int(X[ipix][1]), np.int(X[ipix][0])
            indx_var = self.varIndx_Map[pt_irow,pt_icol]

            print('pt_irow, pt_icol = {}, {}'.format(pt_irow, pt_icol))
            print('arg dmin = {}'.format(self.spix_depth_init_Idt[indx_var]) )
            print('arg dmin in image = {}'.format(self.img_dmap_spix_init_Idt[pt_irow, pt_icol]) )

            # Get the modeled It, Idt #
            snorm_  = self.snorm_init[indx_var, :]
            pix_3d_ = self.KUV[:, indx_var] * self.spix_depth_init_Idt[indx_var] 
            pix_sp_ =  self.S - pix_3d_.reshape((1,3))
            rho_se_ = self.img_rhose_init[pt_irow, pt_icol]
            It_pix_modeled  = rho_se_ * pix_sp_.dot(snorm_)  / (np.linalg.norm(pix_sp_, axis=1)**3)
            Idt_nterm = self.St.dot(snorm_) * rho_se_ / (np.linalg.norm(pix_sp_, axis=1)**3)
            Idt_tterm = 3 * self.St.dot(pix_3d_) * self.seg_observations[:, indx_var]/ (np.linalg.norm(pix_sp_, axis=1)**2)
            Idt_pix_modeled = Idt_nterm + Idt_tterm

            plt.figure(figsize = (10,10))
            plt.subplot(3,1,1)
            # err_v = np.log(self.Err_zcandi[:, indx_var])
#            err_v = np.sqrt(self.Err_zcandi[:, indx_var]) / self.nLED
            err_v = np.sqrt(self.Err_SPix_zcandi[:, indx_var]) / self.nLED
            plt.plot(self.z_candi, err_v)
            plt.ylim([np.min(err_v), np.max(err_v)])
            plt.grid()
            plt.title('Idt energy vs. z candi')

            plt.subplot(3,1,2)
            plt.title('It: raw obs(k) vs knn obs(r) vs. modeled (g) ')
            plt.plot(self.observations[:, indx_var], 'kx-')
            plt.plot(self.seg_observations[:, indx_var], 'rx-')
            plt.plot(It_pix_modeled, 'go--')
            plt.ylim([0, .1])
            plt.grid()

            plt.subplot(3,1,3)
            plt.title('Idt: raw obs(k) vs knn obs(r) vs. modeled (g) vs. nterm (b) vs. tterm (y) ')
            plt.plot(self.observations_dt[:, indx_var], 'kx-')
            plt.plot(self.seg_observations_dt[:, indx_var], 'rx-')
            plt.plot(Idt_pix_modeled, 'go--')
            plt.plot(Idt_nterm, 'bo--')
            plt.plot(Idt_tterm, 'yo--')
            plt.grid()
            plt.ylim([-.10, .10])
            plt.savefig('./tmp_res/spix_init/debug/profiles_pt{}.png'.format(ipix))
            plt.close()

            plt.figure(figsize=(10,10)) 
            axs1 = plt.subplot(3,1,1 )
            plt.imshow(self.img_dmap_spix_init_Idt, cmap='jet', vmin=0, vmax = self.z_candi.max())
            plt.plot(X[ipix][0], X[ipix][1], 'y*')

            plt.subplot(3,1,2, sharex = axs1, sharey = axs1)
            plt.imshow((self.img_snorm_init+1)/2)
            plt.plot(X[ipix][0], X[ipix][1], 'k*')

            plt.subplot(3,1,3, sharex = axs1, sharey = axs1)
            img_rhose_init_show = np.abs(self.img_rhose_init)
            plt.imshow(mark_boundaries(img_rhose_init_show/ img_rhose_init_show.max(), self.img_slic_Indx))
            plt.plot(X[ipix][0], X[ipix][1], 'r*')
            plt.savefig('./tmp_res/spix_init/debug/loc_pt{}.png'.format(ipix))
            plt.close()
#            plt.show()



    def est_Idt(self, fem_d):
        '''
        Given the raw observations, get the differential values (I_dt)
        '''
        self.observations_dt = np.zeros(self.observations.shape)
        for iled in range(self.nLED):
            # jled_ = np.int(iled) - 1 # previous LED
            # kled_ = np.int(iled) + 1 # next LED
            jled_ = np.int(iled) - fem_d # previous LED
            kled_ = np.int(iled) + fem_d # next LED

            if jled_ < 0:
                jled_ = (jled_ + self.nLED) 
            if kled_ >= self.nLED:
                # kled_ = 0
                kled_ =  kled_ % self.nLED

            self.observations_dt[iled, :] = \
                    (self.observations[kled_, :] - self.observations[jled_, :]) / np.float(fem_d*2)

    def est_St(self, fem_d):
        '''
        Given the positions of the lihgts on the LED ring, 
        find St (differential lighting directions)
        St - nLED x 3 array. The light source positions 
        '''
        self.St = np.zeros(self.S.shape)
        self.Sh = np.zeros(self.S.shape)# For compensating the offset when fem_d is large enough to make a difference
        for iled in range(self.nLED):
            jled_ = np.int(iled) - fem_d # previous LED
            kled_ = np.int(iled) + fem_d # next LED

            if jled_ < 0:
                jled_ = (jled_ + self.nLED) 
            if kled_ >= self.nLED:
                # kled_ = 0
                kled_ =  kled_ % self.nLED

            self.St[iled, :] = (self.S[kled_, :] - self.S[jled_, :]) / np.float(fem_d * 2)
            self.Sh[iled, :] = (self.S[kled_, :] + self.S[jled_, :]) / 2.0

    def init_nxy_I_dt(self):
        '''
        initialize the depth z using I_dt and the AC component of I_t 
        '''
        S_t_xy = self.St[:, 0:2]
        S_t_xy_inv = np.linalg.pinv(S_t_xy)
        S = self.S
        S_xy = S[:, 0:2]

        n_observ = np.int(np.sum(self.I_mask))
        UV_obs = np.zeros((3, self.nPts))
        for ipt in range(self.nPts):
            pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
            indx_var = self.varIndx_Map[pt_irow, pt_icol]
            indx_ = np.ravel_multi_index((pt_irow, pt_icol),
                    dims = self.I_mask.shape, order = 'F')
            UV_obs[:, indx_var] = self.UV_mask[:, indx_]
        KUV_obs_ = self.K.dot(UV_obs)

        # For each pixel, get the initialization for the x and y components of
        # surface normal #
        Z_INIT = np.zeros((self.nLED, self.nPts))
        snorm_xy = np.zeros((2, self.nPts))

        for ipt in range(self.nPts):
            pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
            indx_var = self.varIndx_Map[pt_irow, pt_icol]
            Idt = self.observations_dt[:, indx_var]      
            SIt = S_t_xy_inv.dot(Idt)
            snorm_xy[:, indx_var] = SIt / np.linalg.norm(SIt)
            # invert the ydirection such that y positive point to the upper side of the image
#            snorm_xy[1, indx_var] = -snorm_xy[1, indx_var] 

        img_snormal_init_XY = np.zeros((self.I_mask.shape[0], self.I_mask.shape[1], 2))
        for ipt in range(self.nPts):
            pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
            indx_var = self.varIndx_Map[pt_irow,pt_icol] 
            img_snormal_init_XY[pt_irow, pt_icol, :] = snorm_xy[:, indx_var]

        self.img_snormal_init_XY = img_snormal_init_XY
        self.snorm_init_XY = snorm_xy



    def filter_init_depth_bs(self):
        
        print('Filter initial depth map...')

        img_dmap_init_BS_filtered = weighted_filter(self.img_dmap_init_BS, self.img_conf_map_BS,
                filter_radius = 15, sigma_s = 36.0, weight_thresh = 10)
        self.img_dmap_init_BS_filtered = img_dmap_init_BS_filtered

        self.init_depth_bs_filtered = \
                img_dmap_init_BS_filtered[np.unravel_index(self.IndxM_mask[0,:], \
                self.I_mask.shape, order='F')]

        self.init_depth_bs_filtered = np.zeros(self.init_depth_bs.shape)
        for ipt in range(self.nPts):
            pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
            indx_var = self.varIndx_Map[pt_irow, pt_icol]
            self.init_depth_bs_filtered[ipt] = \
                    img_dmap_init_BS_filtered[pt_irow, pt_icol]

        print('DONE')

    def build_LM(self, d_max, d_min = 0.0):
        '''
        Build the light-source-setup-specific lighting matrix LM
        Then inverse it to get LM_inv
        Note that we only need to do this once for one experiment setup
        if we calculate LM_inv for all pixels 
        TODO:
        1)Implement the inner for-loop in C++
        2)DONE: Do the matrix inversion for-loop part on multiple cors or GPUs
        '''
        z_candi = np.linspace(d_min, d_max, self.ncandi_z)
        
        # For all z_candidates #
        print('Building the LM matrix ...')
        self.LM_INV_allZ = []
        for i_z_candi in range(len(z_candi)):
            print('i_z_candi = {} of {}'.format(i_z_candi, self.ncandi_z))
            z_c = z_candi[i_z_candi]

            # For each variable point, we need to build one light matrix #
            LM_INV_allPts = []
            for ipt in range(self.nPts):
                pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
                indx_var = self.varIndx_Map[pt_irow, pt_icol]
                P_var_candi =  self.KUV[:, indx_var] * z_c
                Light_M_ = self.S - P_var_candi.reshape([1,3])
                SP_norm = np.linalg.norm(Light_M_,axis=1)
                Light_M_ = Light_M_ / np.linalg.norm(Light_M_, axis=1).reshape([self.nLED, 1])
                LM_inv_pt = np.linalg.pinv(Light_M_).astype(np.float32)
                LM_INV_allPts.append(LM_inv_pt)
            print('Building the sparse matrix ...')
            print('Size of L-matrix per-pixel: {}'.format(LM_inv_pt.shape))
            self.LM_INV_allZ.append(LM_INV_allPts)

        self.d_min_LM = d_min 
        self.d_max_LM = d_max 
        self.z_candi_LM = z_candi

    def initialize_z_Idt(self, if_fix_rho_se=False, fix_rho_se=0):
        '''
        Initialize the depth z using the Idt term 
        We will combine two constraints here:
        1) The raw image formation model 
        2) The differential image formation model 
        For each hypothized depth z value, we will validate both constraint.
        If the hypothesis is correct, then the errors for the two constraints
        should be small
        '''
        import multiprocessing as mp
        import functools 

        # For debugging #
        if if_fix_rho_se is True:
            self.if_fix_rho_se = True 
            self.fix_rho_se = fix_rho_se
        else:
            self.if_fix_rho_se = False
        # End #

        pool = mp.Pool(processes = 10)
        # Building the cost cube #

        nz_candi = len(self.z_candi)
        Err_zcandi = np.zeros((nz_candi, self.nPts))
        self.Err_zcandi = Err_zcandi 
        # do this on multiple cores #

        if self.fem_d < 2:
            Errs_zcandis = pool.map(self.get_Idt_err_zcandi_, range(nz_candi))
        else:
            Errs_zcandis = pool.map(self.get_Idt_err_zcandi_fem_d_, range(nz_candi))

        self.Err_zcandi = np.vstack(Errs_zcandis)

#        for iz_candi in range(nz_candi):
#            per_z_candi = self.z_candi[iz_candi]
#            print('initialize_z_Idt(): iz_candi = {} of {}'.format(iz_candi, 
#                nz_candi))
#
#            getIdtErr = functools.partial(self.get_Idt_err_pix_,z_candi = per_z_candi,
#                    Err_zcandi = Err_zcandi, iz_candi = iz_candi ) 
#
#            pool.map(getIdtErr, range(self.nPts))
#
#
#            for ipt in range(self.nPts):
#                pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
#                indx_var = self.varIndx_Map[pt_irow, pt_icol]
#                P_var_candi =  self.KUV[:, indx_var] * per_z_candi 
#                Light_M_ = self.S - P_var_candi.reshape([1,3])
#                sp_candi_norm = np.linalg.norm(Light_M_, axis=1)
#                Light_M_ = Light_M_ / sp_candi_norm.reshape([self.nLED, 1])
#                pix_LM_inv = np.linalg.pinv(Light_M_)
##                pix_LM_inv = self.LM_INV_allZ[iz_candi][ipt] 
#                pix_It = self.observations[:, indx_var]
#                rho_se_n = pix_LM_inv.dot(pix_It * (sp_candi_norm **2))
#                rho_se = np.linalg.norm(rho_se_n)
#                pix_snorm_candi = rho_se_n / rho_se
#                
#                pix_Idt_model = self.St.dot(pix_snorm_candi) * rho_se / (sp_candi_norm**3) + \
#                        3 * self.St.dot(P_var_candi) * pix_It / (sp_candi_norm **2)
#
##                Idt_modeled = St.dot(snorm_naive_ps) * rho_se / (SP_norm**3) + 3* St.dot(P_obs_candi) * pix_intensity_obsev / (SP_norm**2)
#                pix_Idt = self.observations_dt[:, indx_var]
#                Err_zcandi[iz_candi, indx_var] = np.sum( (pix_Idt - pix_Idt_model)**2 )

        # Get the initialization for depth #
        indx_depth_init = np.argmin(self.Err_zcandi, axis=0)
        depth_init = [ self.z_candi[indx_] for indx_ in indx_depth_init ]
        depth_init = np.asarray(depth_init)
        self.depth_init_Idt = depth_init

    def initialize_z_spix_Idt(self):
        '''
        Initialize the depth z using the Idt term 
        We will combine two constraints here:
        1) The raw image formation model 
        2) The differential image formation model 
        For each hypothized depth z value, we will validate both constraint.
        If the hypothesis is correct, then the errors for the two constraints
        should be small
        '''
        import multiprocessing as mp
        import functools 

        pool = mp.Pool(processes = 10)
        # Building the cost cube #
        nz_candi = len(self.z_candi)
        # Do this on multiple cores #
        if self.fem_d < 2:
#            Errs_zcandis = pool.map(self.get_Idt_err_zcandi_spix_, range(nz_candi))
            Errs_zcandis = pool.map(self.get_Idt_err_zcandi_spix_DET_, range(nz_candi))
        else:
            print('FEM_D = {} larger than 1'.format(self.fem_d))
#            Errs_zcandis = pool.map(self.get_Idt_err_zcandi_spix_fem_d_, range(nz_candi))
            Errs_zcandis = pool.map(self.get_Idt_err_zcandi_spix_DET_fem_d_, range(nz_candi))
            
        self.Err_SPix_zcandi = np.vstack(Errs_zcandis)
        pool.close()
        # Get the initialization for depth #
        indx_depth_init = np.argmin(self.Err_SPix_zcandi, axis=0)
        depth_init = [ self.z_candi[indx_] for indx_ in indx_depth_init ]
        depth_init = np.asarray(depth_init)
        self.spix_depth_init_Idt = depth_init

    def initialize_z_spix_Idt_spixEnergy(self):
        '''
        Initialize the depth z using the Idt term 
        The cost function here is the err in Idt for every single pixel inside the super-pixel
        '''
        import multiprocessing as mp

        nz_candi = len(self.z_candi)
        Err_zcandi = np.zeros((nz_candi, self.nSlic_Seg))
        self.Err_zcandi = Err_zcandi 
        pool = mp.Pool(processes = 10)
        # Building the cost cube #
        nz_candi = len(self.z_candi)
        # Do this on multiple cores #
        Errs_zcandis = pool.map(self.get_Idt_err_zcandi_spix_segEnergy_, range(nz_candi))
        self.Err_SPix_zcandi = np.vstack(Errs_zcandis)
        pool.close()

        # Get the initialization for depth #
        indx_depth_init = np.argmin(self.Err_SPix_zcandi, axis=0)
        depth_init = [ self.z_candi[indx_] for indx_ in indx_depth_init ]
        depth_init = np.asarray(depth_init)
        self.spix_depth_init_Idt_spixEnergy = depth_init

    def get_Idt_err_zcandi_spix_segEnergy_(self,  iz_candi):
        '''
        Used by  initialize_z_spix_Idt_spixEnergy()
        '''
        z_candi = self.z_candi[iz_candi]
        nz_candi = len(self.z_candi)
        err_zcandi_spixs = np.zeros(self.nSlic_Seg)
        rho_nom = np.zeros(self.nSlic_Seg)
        rho_denom = np.zeros(self.nSlic_Seg)

        print('initialize_z_spix_Idt_segEnergy(): iz_candi = {} of {}'.format(iz_candi+1, nz_candi))
        SP_ = []
        SP_Norm_ = []
        PIX_Snorm_candi = np.zeros((3, self.nPts))
        for ipt in range(self.nPts):
            pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
            indx_var = self.varIndx_Map[pt_irow, pt_icol]
            P_var_candi = self.KUV[:, indx_var] * z_candi 
            pix_SP_ = self.S - P_var_candi.reshape([1,3])
            sp_candi_norm = np.linalg.norm(pix_SP_, axis=1)
            SP_.append(pix_SP_)
            SP_Norm_.append(sp_candi_norm)
#            pix_It = self.seg_observations[:, indx_var]
            # What if the raw measurements for It ? #
            pix_It = self.observations[:, indx_var]

            Light_M_ = pix_SP_ / sp_candi_norm.reshape([self.nLED, 1])
            pix_LM_inv = np.linalg.pinv(Light_M_)
            rho_se_n = pix_LM_inv.dot(pix_It * (sp_candi_norm **2))
            rho_se = np.linalg.norm(rho_se_n)
            pix_snorm_candi = rho_se_n / rho_se
            PIX_Snorm_candi[:, indx_var] = pix_snorm_candi

            iseg_pix_in_mask = self.ISEG_PIX_inmask[indx_var]
            geom_term_= pix_SP_.dot(pix_snorm_candi) / (sp_candi_norm**3)
            rho_nom_iseg_pix_ = np.sum(self.observations[:, indx_var] * geom_term_)
            rho_denom_iseg_pix_ = np.sum(geom_term_ **2)
            rho_nom[np.int(iseg_pix_in_mask)] += rho_nom_iseg_pix_
            rho_denom[np.int(iseg_pix_in_mask)] += rho_denom_iseg_pix_

        for ipt in range(self.nPts):
            pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
            indx_var = self.varIndx_Map[pt_irow, pt_icol]
            iseg_pix = np.int(self.ISEG_PIX_inmask[indx_var])

            P_var_candi = self.KUV[:, indx_var] * z_candi 
            pix_SP_ = SP_[ipt]
            sp_candi_norm = SP_Norm_[ipt]
            pix_snorm_candi = PIX_Snorm_candi[:, indx_var]
            rho_se = rho_nom[iseg_pix] / rho_denom[iseg_pix]

#            pix_It = self.seg_observations[:, indx_var]
            # What if the raw measurements for It ? #
            pix_It = self.observations[:, indx_var]
            pix_Idt_model = self.St.dot(pix_snorm_candi) * rho_se / (sp_candi_norm**3) + \
                    3 * self.St.dot(P_var_candi) * pix_It / (sp_candi_norm **2)
            pix_Idt = self.seg_observations_dt[:, indx_var]
            err_zcandi_spixs[iseg_pix] += np.sum((pix_Idt - pix_Idt_model)**2)

        return err_zcandi_spixs 


    def get_Idt_err_zcandi_spix_(self,  iz_candi):
        z_candi = self.z_candi[iz_candi]
        nz_candi = len(self.z_candi)
        err_zcandi_spixs = np.zeros(self.nPts)
        print('initialize_z_spix_Idt(): iz_candi = {} of {}'.format(iz_candi+1, nz_candi))
        for ipt in range(self.nPts):
            pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
            indx_var = self.varIndx_Map[pt_irow, pt_icol]
            P_var_candi = self.KUV[:, indx_var] * z_candi 
            pix_SP = self.S - P_var_candi.reshape([1,3])
            sp_candi_norm = np.linalg.norm(pix_SP, axis=1)
            Light_M_ = pix_SP / sp_candi_norm.reshape([self.nLED, 1])
            pix_LM_inv = np.linalg.pinv(Light_M_)

#            pix_It = self.seg_observations[:, indx_var]
            # What if the raw measurements for It ? #
            pix_It = self.observations[:, indx_var]

            rho_se_n = pix_LM_inv.dot(pix_It * (sp_candi_norm **2))
#            rho_se = np.linalg.norm(rho_se_n)
#            pix_snorm_candi = rho_se_n / rho_se

#            pix_Idt_model = self.St.dot(pix_snorm_candi) * rho_se / (sp_candi_norm**3) + \
#                    3 * self.St.dot(P_var_candi) * pix_It / (sp_candi_norm **2)
#            pix_Idt_model = pix_It * self.St.dot(pix_snorm_candi) / (pix_SP.dot(pix_snorm_candi)) + \
#                    3 * self.St.dot(P_var_candi) * pix_It / (sp_candi_norm **2)
            pix_Idt_model = pix_It * self.St.dot(rho_se_n) / (pix_SP.dot(rho_se_n)) + \
                    3 * self.St.dot(P_var_candi) * pix_It / (sp_candi_norm **2)
            pix_Idt = self.seg_observations_dt[:, indx_var]
            err_zcandi_spixs[indx_var] = np.sum( (pix_Idt - pix_Idt_model)**2 )

        return err_zcandi_spixs 

    def get_Idt_err_zcandi_spix_fem_d_(self,  iz_candi):
        z_candi = self.z_candi[iz_candi]
        nz_candi = len(self.z_candi)
        err_zcandi_spixs = np.zeros(self.nPts)
        print('initialize_z_spix_Idt(): iz_candi = {} of {}'.format(iz_candi+1, nz_candi))
        for ipt in range(self.nPts):
            pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
            indx_var = self.varIndx_Map[pt_irow, pt_icol]
            P_var_candi = self.KUV[:, indx_var] * z_candi 
            pix_SP = self.S - P_var_candi.reshape([1,3])
            pix_ShP = self.Sh - P_var_candi.reshape([1,3])
            sp_candi_norm = np.linalg.norm(pix_SP, axis=1)
            sph_candi_norm = np.linalg.norm(pix_ShP, axis=1)
            Light_M_ = pix_SP / sp_candi_norm.reshape([self.nLED, 1])
            pix_LM_inv = np.linalg.pinv(Light_M_)

#            pix_It = self.seg_observations[:, indx_var]
            # What if the raw measurements for It ? #
            pix_It = self.observations[:, indx_var]

            rho_se_n = pix_LM_inv.dot(pix_It * (sp_candi_norm **2))
#            rho_se = np.linalg.norm(rho_se_n)
#            pix_snorm_candi = rho_se_n / rho_se

            pix_It_c = pix_It * (sp_candi_norm**3) / (sph_candi_norm**3) * \
                    pix_ShP.dot(rho_se_n) / pix_SP.dot(rho_se_n)

            pix_Idt_model = pix_It_c * self.St.dot(rho_se_n) / (pix_SP.dot(rho_se_n)) + \
                    3 * self.St.dot(P_var_candi) * pix_It_c / (sp_candi_norm **2)

#            pix_Idt = self.seg_observations_dt[:, indx_var]
            pix_Idt = self.observations_dt[:, indx_var]
            err_zcandi_spixs[indx_var] = np.sum( (pix_Idt - pix_Idt_model)**2 )

        return err_zcandi_spixs 

    def get_Idt_err_zcandi_spix_DET_(self,  iz_candi):
        '''
        Getting the energy function for depth using determinant 
        '''
        z_candi = self.z_candi[iz_candi]
        nz_candi = len(self.z_candi)
        err_zcandi_spixs = np.zeros(self.nPts)
        print('initialize_z_spix_Idt(): iz_candi = {} of {}'.format(iz_candi+1, nz_candi))
        for ipt in range(self.nPts):
            pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
            indx_var = self.varIndx_Map[pt_irow, pt_icol]
            P_var_candi = self.KUV[:, indx_var] * z_candi 
            pix_SP = self.S - P_var_candi.reshape([1,3])
            sp_candi_norm = np.linalg.norm(pix_SP, axis=1)

            # Get pixel value measurements #
            pix_It = self.observations[:, indx_var]
            pix_Idt = self.seg_observations_dt[:, indx_var]
#            pix_Idt = self.observations_dt[:, indx_var]

            # Get the singular values of A#
            pix_ratio = pix_Idt / (pix_It+1e-9)
            A = self.St + 3* pix_SP * (self.St.dot(P_var_candi) / (sp_candi_norm **2)).reshape((self.nLED, 1)) - \
                    pix_SP * pix_ratio.reshape((self.nLED, 1))
#            A = self.St  - \
#                    pix_SP * pix_ratio.reshape((self.nLED, 1))
            singlA = np.abs(np.linalg.svd(A, compute_uv = False))
            err_zcandi_spixs[indx_var] = np.sort(singlA)[0]**2

        return err_zcandi_spixs 

    def get_Idt_err_zcandi_spix_DET_fem_d_(self,  iz_candi):
        '''
        Getting the energy function for depth using determinant 
        '''
        z_candi = self.z_candi[iz_candi]
        nz_candi = len(self.z_candi)
        err_zcandi_spixs = np.zeros(self.nPts)
        print('initialize_z_spix_Idt(): iz_candi = {} of {}'.format(iz_candi+1, nz_candi))
        for ipt in range(self.nPts):
            pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
            indx_var = self.varIndx_Map[pt_irow, pt_icol]
            P_var_candi = self.KUV[:, indx_var] * z_candi 
            pix_SP = self.S - P_var_candi.reshape([1,3])
            pix_ShP = self.Sh - P_var_candi.reshape([1,3])
            sp_candi_norm = np.linalg.norm(pix_SP, axis=1)
            sph_candi_norm = np.linalg.norm(pix_ShP, axis=1)
            Light_M_ = pix_SP / sp_candi_norm.reshape([self.nLED, 1])
            pix_LM_inv = np.linalg.pinv(Light_M_)

            # Get pixel value measurements #
#            pix_It = self.seg_observations[:, indx_var]
            pix_It = self.observations[:, indx_var]
            pix_Idt = self.seg_observations_dt[:, indx_var]
#            pix_Idt = self.observations_dt[:, indx_var]
            rho_se_n = pix_LM_inv.dot(pix_It * (sp_candi_norm **2))
            pix_sn = rho_se_n / np.linalg.norm(rho_se_n)

            pix_It_c = pix_It * (sp_candi_norm**3) / (sph_candi_norm**3) * \
                    pix_ShP.dot(rho_se_n) / pix_SP.dot(rho_se_n)

            pix_ratio = pix_Idt / pix_It_c 
#            pix_ratio = pix_Idt / pix_It
            A = self.St + 3* pix_SP * (self.St.dot(P_var_candi) / (sp_candi_norm **2)).reshape((self.nLED, 1)) - \
                    pix_SP * pix_ratio.reshape((self.nLED, 1))
            # A2 = A.transpose().dot(A)
            pix_sn = rho_se_n / np.linalg.norm(rho_se_n)
            An = A.dot(pix_sn)

            pix_Idt_model = pix_It_c * self.St.dot(rho_se_n) / (pix_SP.dot(rho_se_n)) + \
                    3 * self.St.dot(P_var_candi) * pix_It_c / (sp_candi_norm **2)

            # err_zcandi_spixs[indx_var] = np.linalg.det(A2) + 5000.0 * np.sum( (pix_Idt - pix_Idt_model)**2 )
            err_zcandi_spixs[indx_var] = 50 * np.linalg.norm(An) + np.sum((pix_Idt - pix_Idt_model)**2)

        return err_zcandi_spixs 
    

    def get_Idt_err_zcandi_(self,  iz_candi):
        z_candi = self.z_candi[iz_candi]
        nz_candi = len(self.z_candi)
        err_zcandi_pixs = np.zeros(self.nPts)
        print('initialize_z_Idt(): iz_candi = {} of {}'.format(iz_candi+1, nz_candi))
        for ipt in range(self.nPts):
            pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
            indx_var = self.varIndx_Map[pt_irow, pt_icol]
            P_var_candi = self.KUV[:, indx_var] * z_candi 
            pix_SP = self.S - P_var_candi.reshape([1,3])
            sp_candi_norm = np.linalg.norm(pix_SP, axis=1)
            Light_M_ = pix_SP / sp_candi_norm.reshape([self.nLED, 1])
            pix_LM_inv = np.linalg.pinv(Light_M_)
            pix_It = self.observations[:, indx_var]
            rho_se_n = pix_LM_inv.dot(pix_It * (sp_candi_norm **2))
#            rho_se = np.linalg.norm(rho_se_n)
#            pix_snorm_candi = rho_se_n / rho_se
#            pix_Idt_model = self.St.dot(pix_snorm_candi) * rho_se / (sp_candi_norm**3) + \
#                    3 * self.St.dot(P_var_candi) * pix_It / (sp_candi_norm **2)
            pix_Idt_model = pix_It * self.St.dot(rho_se_n) / (pix_SP.dot(rho_se_n)) + \
                    3 * self.St.dot(P_var_candi) * pix_It / (sp_candi_norm **2)
            pix_Idt = self.observations_dt[:, indx_var]
            err_zcandi_pixs[indx_var] = np.sum( (pix_Idt - pix_Idt_model)**2 )

        return err_zcandi_pixs

    def get_Idt_err_zcandi_fem_d_(self,  iz_candi):
        '''
        Compensate for fem_d > 1
        '''
        z_candi = self.z_candi[iz_candi]
        nz_candi = len(self.z_candi)
        err_zcandi_pixs = np.zeros(self.nPts)
        print('initialize_z_Idt(): iz_candi = {} of {}'.format(iz_candi+1, nz_candi))
        for ipt in range(self.nPts):
            pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
            indx_var = self.varIndx_Map[pt_irow, pt_icol]
            P_var_candi = self.KUV[:, indx_var] * z_candi 
            pix_SP = self.S - P_var_candi.reshape([1,3])
            pix_ShP = self.Sh - P_var_candi.reshape([1,3])
            sp_candi_norm = np.linalg.norm(pix_SP, axis=1)
            sph_candi_norm = np.linalg.norm(pix_ShP, axis=1)
            Light_M_ = pix_SP / sp_candi_norm.reshape([self.nLED, 1])
            pix_LM_inv = np.linalg.pinv(Light_M_)

            pix_It = self.observations[:, indx_var]
            # Compsate for the offset due to fem_d >1 #
            rho_se_n = pix_LM_inv.dot(pix_It * (sp_candi_norm **2))
            pix_It_c = pix_It * (sp_candi_norm**3) / (sph_candi_norm**3) * \
                    pix_ShP.dot(rho_se_n) / pix_SP.dot(rho_se_n)

#            rho_se = np.linalg.norm(rho_se_n)
#            pix_snorm_candi = rho_se_n / rho_se
#            pix_Idt_model = self.St.dot(pix_snorm_candi) * rho_se / (sp_candi_norm**3) + \
#                    3 * self.St.dot(P_var_candi) * pix_It / (sp_candi_norm **2)
            pix_Idt_model = pix_It_c * self.St.dot(rho_se_n) / (pix_SP.dot(rho_se_n)) + \
                    3 * self.St.dot(P_var_candi) * pix_It_c / (sp_candi_norm **2)
            pix_Idt = self.observations_dt[:, indx_var]
            err_zcandi_pixs[indx_var] = np.sum( (pix_Idt - pix_Idt_model)**2 )

        return err_zcandi_pixs


    def get_Idt_err_pix_(self,ipt,  z_candi, Err_zcandi, iz_candi):
        pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
        indx_var = self.varIndx_Map[pt_irow, pt_icol]
        P_var_candi =  self.KUV[:, indx_var] * z_candi 
        Light_M_ = self.S - P_var_candi.reshape([1,3])
        sp_candi_norm = np.linalg.norm(Light_M_, axis=1)
        Light_M_ = Light_M_ / sp_candi_norm.reshape([self.nLED, 1])
        pix_LM_inv = np.linalg.pinv(Light_M_)
        pix_It = self.observations[:, indx_var]
        rho_se_n = pix_LM_inv.dot(pix_It * (sp_candi_norm **2))
        rho_se = np.linalg.norm(rho_se_n)
        pix_snorm_candi = rho_se_n / rho_se
        
        pix_Idt_model = self.St.dot(pix_snorm_candi) * rho_se / (sp_candi_norm**3) + \
                3 * self.St.dot(P_var_candi) * pix_It / (sp_candi_norm **2)

        pix_Idt = self.observations_dt[:, indx_var]
        Err_zcandi[iz_candi, indx_var] = np.sum( (pix_Idt - pix_Idt_model)**2 )

    def export_snorm(self):
        '''
        Get snorm using Naive PS, given init_z
        '''
        self.snorm_init = np.zeros((self.nPts, 3))
        self.rhose = np.zeros(self.nPts)
        self.img_snorm_init = np.zeros((self.I_mask.shape[0], self.I_mask.shape[1], 3))
        self.img_rhose_init = np.zeros(self.I_mask.shape)
        for ipt in range(self.nPts):
            pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
            indx_var = self.varIndx_Map[pt_irow,pt_icol] 
            pix_depth_ = self.depth_init_Idt[indx_var]
            pix_loc3d = self.KUV[:, indx_var] * pix_depth_
            pix_SP_ = self.S - pix_loc3d.reshape((1,3))
            pix_SP_norm_ = np.linalg.norm(pix_SP_, axis=1)

            Light_M_ = pix_SP_ / pix_SP_norm_.reshape([self.nLED, 1])
            pix_LM_inv = np.linalg.pinv(Light_M_)
            pix_It = self.observations[:, indx_var]
            rho_se_n = pix_LM_inv.dot(pix_It * (pix_SP_norm_**2))
            rho_se = np.linalg.norm(rho_se_n)
            pix_snorm = rho_se_n / rho_se

            self.snorm_init[indx_var, :] = pix_snorm 
            self.img_snorm_init[pt_irow, pt_icol] = pix_snorm
            self.img_rhose_init[pt_irow, pt_icol] = rho_se
            self.rhose[indx_var] = rho_se

    def export_snorm_spix(self):
        '''
        Get snorm using Naive PS, given init_z
        '''
        self.snorm_init = np.zeros((self.nPts, 3))
        self.rhose = np.zeros(self.nPts)
        self.img_snorm_init = np.zeros((self.I_mask.shape[0], self.I_mask.shape[1], 3))
        self.img_rhose_init = np.zeros(self.I_mask.shape)
        for ipt in range(self.nPts):
            pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
            indx_var = self.varIndx_Map[pt_irow,pt_icol] 
            pix_depth_ = self.spix_depth_init_Idt[indx_var] 
            pix_loc3d = self.KUV[:, indx_var] * pix_depth_
            pix_SP_ = self.S - pix_loc3d.reshape((1,3))
            pix_SP_norm_ = np.linalg.norm(pix_SP_, axis=1)

            Light_M_ = pix_SP_ / pix_SP_norm_.reshape([self.nLED, 1])
            pix_LM_inv = np.linalg.pinv(Light_M_)
            pix_It = self.observations[:, indx_var]
            rho_se_n = pix_LM_inv.dot(pix_It * (pix_SP_norm_**2))
            rho_se = np.linalg.norm(rho_se_n)
            pix_snorm = rho_se_n / rho_se

            self.snorm_init[indx_var, :] = pix_snorm 
            self.img_snorm_init[pt_irow, pt_icol,:] = pix_snorm
            self.img_rhose_init[pt_irow, pt_icol] = rho_se
            self.rhose[indx_var] = rho_se


    def export_init_z_Idt(self):
        img_dmap_init_Idt = np.zeros(self.I_mask.shape)
        for ipt in range(self.nPts):
            pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
            indx_var = self.varIndx_Map[pt_irow,pt_icol] 
            img_dmap_init_Idt[pt_irow, pt_icol] = self.depth_init_Idt[indx_var]

        self.img_dmap_init_Idt = img_dmap_init_Idt

    def export_init_spix_z_Idt(self):
        '''
        Export the intializatino of depth on super-pixels 
        '''
        print('Exportring init depth using super pixel ...')
        img_dmap_spix_init_Idt = np.zeros(self.I_mask.shape)
        imgs_values_spix = np.zeros(self.IMGs.shape)
        for ipt in range(self.nPts):
            pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
            indx_var = self.varIndx_Map[pt_irow,pt_icol] 
            img_dmap_spix_init_Idt[pt_irow, pt_icol] = self.spix_depth_init_Idt[indx_var]
            imgs_values_spix[pt_irow, pt_icol, :] = self.seg_observations[:, indx_var]


        self.img_dmap_spix_init_Idt = img_dmap_spix_init_Idt
        self.imgs_values_spix = imgs_values_spix
        print('Done')

    def export_init_spix_z_Idt_spixEnergy(self):
        '''
        Export the intializatino of depth on super-pixels using spix energy 
        '''
        print('Exportring init depth using super pixel ...')
        img_dmap_spix_init_Idt_spixEnergy = np.zeros(self.I_mask.shape)
        imgs_values_spix = np.zeros(self.IMGs.shape)
        self.snorm_init = np.zeros((self.nPts, 3))
        self.rhose = np.zeros(self.nPts)
        self.img_snorm_init = np.zeros((self.I_mask.shape[0], self.I_mask.shape[1], 3))
        self.img_rhose_init = np.zeros(self.I_mask.shape)

        for ipt in range(self.nPts):
            pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
            indx_var = self.varIndx_Map[pt_irow,pt_icol] 
            iseg_inmask = np.int(self.ISEG_PIX_inmask[indx_var])
            img_dmap_spix_init_Idt_spixEnergy[pt_irow, pt_icol] \
                    = self.spix_depth_init_Idt_spixEnergy[iseg_inmask]
            imgs_values_spix[pt_irow, pt_icol, :] = self.seg_observations[:, indx_var]

            pix_depth_ = self.spix_depth_init_Idt_spixEnergy[iseg_inmask]
            pix_loc3d = self.KUV[:, indx_var] * pix_depth_
            pix_SP_ = self.S - pix_loc3d.reshape((1,3))
            pix_SP_norm_ = np.linalg.norm(pix_SP_, axis=1)
            Light_M_ = pix_SP_ / pix_SP_norm_.reshape([self.nLED, 1])
            pix_LM_inv = np.linalg.pinv(Light_M_)
            pix_It = self.observations[:, indx_var]
            rho_se_n = pix_LM_inv.dot(pix_It * (pix_SP_norm_**2))
            rho_se = np.linalg.norm(rho_se_n)
            pix_snorm = rho_se_n / rho_se

            self.snorm_init[indx_var, :] = pix_snorm 
            self.img_snorm_init[pt_irow, pt_icol, :] = pix_snorm
            self.img_rhose_init[pt_irow, pt_icol] = rho_se
            self.rhose[indx_var] = rho_se

        self.img_dmap_spix_init_Idt_spixEnergy = img_dmap_spix_init_Idt_spixEnergy
        self.imgs_values_spix = imgs_values_spix
        print('Done')




    def initialize_depth_bs(self, d_max, d_min = 0.0):
        '''
        Initialize the depth map using per-pix brutal force search 
        '''
        M_IT0 = []
        M_IT1 = []
        M_IS = []
        DIST_CLINE = []
        S = self.S
        St = self.St 
        observations_t = self.observations 
        observations_dt = self.observations_dt
        N_LED = self.nLED
        npts_valid = self.nPts
        K = self.K 
        I_mask = self.I_mask
        IndxM_mask = self.IndxM_mask 
        UV_mask = self.UV_mask

        print('Building the cost cube for depth initialization...')
        # n_observ = IndxM_mask.shape[1]
        # UV_obs = UV_mask[:, IndxM_mask[0, :]]
        n_observ = np.int(np.sum(self.I_mask))
        # indx_obsv = np.ravel_multi_index(np.nonzero(self.I_mask) , dims = I_mask.shape,order = 'F')
        UV_obs = np.zeros((3, self.nPts))
        for ipt in range(self.nPts):
            pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
            indx_var = self.varIndx_Map[pt_irow, pt_icol]
            indx_ = np.ravel_multi_index((pt_irow, pt_icol), dims = I_mask.shape, order = 'F')
            UV_obs[:, indx_var] = self.UV_mask[:, indx_]

        # UV_obs = UV_mask[:, indx_obsv]
        KUV_obs_ = K.dot(UV_obs)

        # distance to cline #
        for dt_led_st in range(np.int(N_LED/2)):
            print('{}-th LED pair in {} LED pairs'.format(\
                    dt_led_st + 1, np.int(N_LED/2)))
            dt_led_ed = dt_led_st + np.int(N_LED/2) 
        
            Idt1_ = observations_dt[dt_led_st, :] 
            Idt2_ = observations_dt[dt_led_ed, :]
            LHS = Idt1_ + Idt2_ 

            if self.use_ratio_img is True:
                LHS /= observations_t[dt_led_st, :]

            M_IS.append(LHS)
            I_t0 = observations_t[dt_led_st, :]
            I_t1 = observations_t[dt_led_ed, :]
            M_IT0.append(I_t0)
            M_IT1.append(I_t1)
        
            S0 = S[dt_led_st, :]
        
            # First get the 2d central line #
            cent_line_pts_ = np.zeros((2, 2))
            cent_line_pts_[0,:] = np.asarray([0.0, 0.0])
            cent_line_pts_[1,:] = np.asarray([S0[1], -S0[0]])
            line_2d_paras = fit_line_2d(cent_line_pts_[1,:])

            # Then get the distances to the 2d central line #
            dist2_cline_ = dist2_line2d(line_2d_paras, KUV_obs_[:2,:].transpose())
            img_dist2_cline = np.zeros(I_mask.shape)

            for ipt in range(self.nPts): 
                pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
                indx_var = self.varIndx_Map[pt_irow, pt_icol]
                img_dist2_cline[pt_irow, pt_icol] = dist2_cline_[indx_var]

            # for ipt in range(npts_valid):
            #     indx_ipt = IndxM_mask[:, ipt] 
            #     neighbor_loc = np.unravel_index(indx_ipt, I_mask.shape, order='F')
            #     img_dist2_cline[neighbor_loc[0][0], neighbor_loc[1][0]] = \
            #             dist2_cline_[ipt]
        
            DIST_CLINE.append(img_dist2_cline)

        DIST_CLINE = np.dstack(tuple(DIST_CLINE))
        dist_cline_min = np.min(DIST_CLINE, axis=2)
        #END distance to cline #

        
        M_IS = np.vstack(tuple(M_IS))
        self.observations_Is = M_IS
        M_IT0 = np.vstack(tuple(M_IT0))
        M_IT1 = np.vstack(tuple(M_IT1))
        print('DONE')
        
        
        n_inits_weightsum = np.int( N_LED / 2) 
        wexp_scalar_cline_dist = 5.0#10.0
        w_scalar_Is = .8 
        img_kuv0 = np.zeros(I_mask.shape)
        img_kuv1 = np.zeros(I_mask.shape)
        M_weights = np.zeros((n_inits_weightsum, KUV_obs_.shape[1]))
        
        ind_var = 0
        # img_indx_map = np.zeros(I_mask.shape).astype(np.int64)
        print('Getting the Weight Matrix for initialization')
        for ipt in range(npts_valid):
            pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
            indx_var = self.varIndx_Map[pt_irow,pt_icol] 

        
            # Get the weights #
            # pix_dist_ =  DIST_CLINE[neighbor_loc[0][0], neighbor_loc[1][0], :].flatten()
            pix_dist_ = DIST_CLINE[pt_irow, pt_icol, :]
        
            id_sort_ = np.argsort(pix_dist_)
            id_sort_ = id_sort_[:2] # choose the closest 2 
            dist_candi_w = pix_dist_[id_sort_]
            weights_2assign = np.zeros(n_inits_weightsum)
            weights_d_candi_ = np.exp(-(dist_candi_w * wexp_scalar_cline_dist)**2)
            
            # Further weighted by the abs intensity of Is # 
            # weights_d_candi_ = weights_d_candi_ * np.power(np.abs(M_IS[id_sort_, ipt]), w_scalar_Is )
            weights_d_candi_ = weights_d_candi_ * np.power(np.abs(M_IS[id_sort_, indx_var]), w_scalar_Is )
        
            weights_2assign[id_sort_] = weights_d_candi_
            # M_weights[:, ipt] = weights_2assign
            M_weights[:, indx_var] = weights_2assign
            # img_kuv0[neighbor_loc[0][0], neighbor_loc[1][0]] = KUV_obs_[0, ipt] 
            # img_kuv1[neighbor_loc[0][0], neighbor_loc[1][0]] = KUV_obs_[1, ipt] 
            img_kuv0[pt_irow, pt_icol] = KUV_obs_[0, indx_var]
            img_kuv1[pt_irow, pt_icol] = KUV_obs_[1, indx_var]

            ind_var = ind_var + 1

        print('DONE')
        M_weights = M_weights / np.sum(M_weights, axis=0).reshape((1, M_weights.shape[1])) 
        
        #  Brutal force search for the optimal depth for each pixel #
        print('Initialize the depth ...')
        init_z_indx, init_depth_bs, conf_m = \
                initialize_z_Tterm(M_IS, [M_IT0, M_IT1],
                         S, St,  KUV_obs_.transpose(), 
                         np.linspace(d_min, d_max, 200), M_weights,
                         use_ratio_img = self.use_ratio_img)
        print('DONE')

        # Initialization map #
        img_dmap_init_BS = np.zeros(I_mask.shape) # brutal force search initialization 
        img_conf_map = np.zeros(I_mask.shape)
        for ipt in range(npts_valid):
            # indx_ipt = IndxM_mask[:, ipt] 
            # neighbor_loc = np.unravel_index(indx_ipt, I_mask.shape, order='F')
            # img_dmap_init_BS[neighbor_loc[0][0], neighbor_loc[1][0]] = init_depth_bs[ipt]
            # img_conf_map[neighbor_loc[0][0], neighbor_loc[1][0]] = conf_m[ipt]
            pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
            indx_var = self.varIndx_Map[pt_irow,pt_icol] 
            img_dmap_init_BS[pt_irow, pt_icol] = init_depth_bs[indx_var]
            img_conf_map[pt_irow, pt_icol] = conf_m[indx_var]

        conf_m[np.isnan(conf_m)] = 0.0 
        img_conf_map[np.isnan(img_conf_map)] = 0.0 

        self.init_depth_bs = init_depth_bs 
        self.conf_bs = conf_m
        self.img_dmap_init_BS = img_dmap_init_BS
        self.img_conf_map_BS = img_conf_map
        self.M_weights = M_weights


        self.img_dmap_init_Idt = img_dmap_init_BS
        self.depth_init_Idt = init_depth_bs

    def initialize_depth_bs_nxy(self, d_max, d_min = 0.0):
        '''
        Initialize the depth map using per-pix brutal force search 
        given the information about nxy (i.e. we know the surface normal is in
        which plane). With the knowledge of nxy, we can find out the s_t such that:
        s_t.dot(n) = 0, thus the differential image term 
        I_dt = s_t.dot(p) * Se * Rho * It / [norm(s-p)**2]
        since the first term of I_dt = 0 for s_t.dot(n)
        '''
        self.init_nxy_I_dt()

        COS_St_NXY = []

        S = self.S
        St = self.St 
        observations_t = self.observations 
        observations_dt = self.observations_dt
        N_LED = self.nLED
        npts_valid = self.nPts
        K = self.K 
        I_mask = self.I_mask
        IndxM_mask = self.IndxM_mask 
        UV_mask = self.UV_mask

        print('Building the cost cube for depth initialization...')
        n_observ = np.int(np.sum(self.I_mask))
        UV_obs = np.zeros((3, self.nPts))
        for ipt in range(self.nPts):
            pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
            indx_var = self.varIndx_Map[pt_irow, pt_icol]
            indx_ = np.ravel_multi_index((pt_irow, pt_icol), dims = I_mask.shape, order = 'F')
            UV_obs[:, indx_var] = self.UV_mask[:, indx_]
        KUV_obs_ = K.dot(UV_obs)

        # cos between St and the nxy plane #
        for iled in range(np.int(N_LED)):
            print('{}-th LED in {} LEDs'.format(iled + 1, np.int(N_LED)))
            st_ = St[iled, 0:2]

            # cos(st, nxy) #
            cos_st_nxy = np.sum(self.snorm_init_XY * st_.reshape([2,1]), axis=0)
            img_cos_st_nxy = np.zeros(I_mask.shape)
            for ipt in range(self.nPts): 
                pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
                indx_var = self.varIndx_Map[pt_irow, pt_icol]
                img_cos_st_nxy[pt_irow, pt_icol] = cos_st_nxy[indx_var]
            COS_St_NXY.append(img_cos_st_nxy)

        COS_St_NXY = np.dstack(tuple(COS_St_NXY))

        # for debugging #
        self.COS_St_NXY = COS_St_NXY

        #END cos #
        print('DONE')
        
        
        n_inits_weightsum = np.int( N_LED ) 
        wexp_scalar_cline_dist = 5.0#10.0
        w_scalar_Is = .8 
        M_weights = np.zeros((n_inits_weightsum, KUV_obs_.shape[1]))
        ind_var = 0

        print('Getting the Weight Matrix for initialization')
        for ipt in range(npts_valid):
            pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
            indx_var = self.varIndx_Map[pt_irow,pt_icol] 
        
            # Get the weights #
            pix_dist_ = np.abs(COS_St_NXY[pt_irow, pt_icol, :])
            id_sort_ = np.argsort(pix_dist_)
            id_sort_ = id_sort_[:4] # choose the closest 2 
            dist_candi_w = pix_dist_[id_sort_]
            weights_2assign = np.zeros(n_inits_weightsum)
            weights_d_candi_ = np.exp(-(dist_candi_w * wexp_scalar_cline_dist)**2)
            
            # Further weighted by the abs intensity of observation_dt # 
#            weights_d_candi_ = weights_d_candi_ * \
#                    np.power(np.abs(observations_dt[id_sort_, indx_var]), w_scalar_Is )
            weights_2assign[id_sort_] = weights_d_candi_
            M_weights[:, indx_var] = weights_2assign
            ind_var = ind_var + 1

        print('DONE')
        M_weights = M_weights / np.sum(M_weights, axis=0).reshape((1, M_weights.shape[1])) 
        
        #  Brutal force search for the optimal depth for each pixel #
        print('Initialize the depth ...')
        init_depth_bs, conf_m = initialize_z_Tterm_Nxy(observations_dt, observations_t,
                         S, St,  KUV_obs_.transpose(), np.linspace(d_min, d_max, 200), M_weights)
        print('DONE')

        # Initialization map #
        img_dmap_init_BS = np.zeros(I_mask.shape) # brutal force search initialization 
        img_conf_map = np.zeros(I_mask.shape)
        for ipt in range(npts_valid):
            pt_irow, pt_icol = self.varLoc_multiIndx[0][ipt], self.varLoc_multiIndx[1][ipt]
            indx_var = self.varIndx_Map[pt_irow,pt_icol] 
            img_dmap_init_BS[pt_irow, pt_icol] = init_depth_bs[indx_var]
            img_conf_map[pt_irow, pt_icol] = conf_m[indx_var]

        conf_m[np.isnan(conf_m)] = 0.0 
        img_conf_map[np.isnan(img_conf_map)] = 0.0 

        self.init_depth_bs = init_depth_bs 
        self.conf_bs = conf_m
        self.img_dmap_init_BS = img_dmap_init_BS
        self.img_conf_map_BS = img_conf_map
        self.M_weights = M_weights
