import numpy as np
import scipy.io as sio
import os
import shutil
# import matlab.engine
import ipdb
# from mio import io_EXR

from z2n import Zmap2Nmap as z2n
from autodiff import simpleNearPS 

# TODO:
# * Update z such that it is robust to the global illuminations 

def updateZ_Idt(nearPS, lam_smooth_Idt, cur_scale, max_Iter_PS_Idt=800, dt_pow_para = 2.0):
    '''
    update z using Idt, such that the estimation is robust to the global
    illumination effects
    '''
    nThreads = 10
    PS_nearL = NearLight(nearPS.IMGs, nearPS.I_mask, nearPS.S, nearPS.K, 
            fem_d = 1, scale = 4)
    PS_nearL.dt_recon = True 
    rho_se_init = nearPS.rho_se_cur
    PS_nearL.set_lambda_smooth(lam_smooth_Idt)
    PS_nearL.sPS.set_dt_pow_para(dt_pow_para)

    PS_nearL.max_iter = max_Iter_PS_Idt
    PS_nearL.set_nThreads(nThreads)
    # Set initials #
    PS_nearL.set_initial_z(nearPS.depth_cur)
    PS_nearL.set_Rhos(rho_se_init, if_input_rhos_obsev=True)
    # Solve using Idt and better initialization of z 
    PS_nearL.solve()
    PS_nearL.solve_albedo()

    return PS_nearL

def updateRho_Idt(nearPS, femd = 1):
    '''
    Update rho using Idt, given the current estimation of z using Idt
    Also return the direct-only image based on rho and z 
    '''
    import pyigl as igl
    import iglhelpers as iglhp
    
    nLED = nearPS.observations.shape[0]
    nPt_obs = nearPS.observations.shape[1]
    nr, nc = nearPS.I_mask.shape[0], nearPS.I_mask.shape[1]
    observations_t = nearPS.observations_t
    observations_dt = nearPS.observations_dt

    F_vars = nearPS.F_vars 
    depth_cur = nearPS.depth_cur
    V = nearPS.KUV_vars.transpose() * depth_cur.reshape(depth_cur.shape[0], 1);
    V_ = np.zeros(V.shape, order='C')
    V_[:] = V
    F_ = np.zeros(F_vars.shape, order='C', dtype=np.int32)
    F_[:] = F_vars.astype(np.int32)
    V_e = iglhp.p2e(V_)
    F_e = iglhp.p2e(F_)

    # Get the vertices normals for the variable vertices #
    N_vertices = igl.eigen.MatrixXd()
    igl.per_vertex_normals(V_e, F_e, igl.PER_VERTEX_NORMALS_WEIGHTING_TYPE_AREA, N_vertices)
    N_vert = iglhp.e2p(N_vertices).transpose() # Nvert for variable vertices each col of N_vert is a normal 

    # Get the updated Rho using analytical form of Idt #
    Rho_obs = np.zeros(nPt_obs)
    Id_obs = np.zeros((nLED, nPt_obs)) # Direct only image intensities
    N_vert_obs = np.zeros((3, nPt_obs))
    indxV_var = nearPS.IndxM_var[0, :]
    S = nearPS.sPS.get_S()
    St = nearPS.sPS.get_St()
    if femd > 1:
        St = np.zeros(St.shape)
        for iLED in range(nLED):
            jLED = ((iLED - femd) + nLED) % nLED 
            kLED = ((iLED + femd) + nLED) % nLED 
            St[:, iLED] = (S[:, kLED] - S[:, jLED]) / (2 * femd)
            observations_dt[iLED, :] = (observations_t[kLED] - observations_t[jLED]) / (2* femd)

    Rho_obs_raw = np.zeros(nPt_obs)

    IMGs_Tterm = np.zeros((nr, nc, nLED))
    for ipt in range(nPt_obs):
        N_vert_obs[:, ipt] = N_vert[:, indxV_var[ipt]]
        N_vert_pix_ = N_vert_obs[:, ipt]
        Kuv_obs_ = nearPS.KUV_vars[:, indxV_var[ipt]]
        depth_cur_obs_ = depth_cur[indxV_var[ipt]]
        P_obs_ = Kuv_obs_ * depth_cur_obs_
        StP = np.sum(St * P_obs_.reshape((3,1)), axis=0)
        StN = np.sum(St * N_vert_pix_.reshape((3,1)), axis=0)
        SP = S - P_obs_.reshape((3,1)) # size: 3 x nLED
        NSP = np.sum(SP * N_vert_pix_.reshape(3,1), axis=0)
        SP_norm = np.linalg.norm(SP, axis=0)
        T_terms_pix = observations_dt[:,ipt] - 3 * StP / (SP_norm**2) * observations_t[:, ipt]
        # rho_pix_ = np.sum(T_terms_pix**2 * (SP_norm**3)/StN ) / np.sum(T_terms_pix) 
        # rho_pix_ = np.sum(np.abs(observations_dt[:,ipt])**2 *  T_terms_pix * (SP_norm**3)/StN ) \
        #           / np.sum(np.abs(observations_dt[:,ipt])**2) 
#        rho_pix_ = np.mean(T_terms_pix * (SP_norm**3)/StN )
        indx = 12
        rho_pix_ = T_terms_pix[indx] * (SP_norm[indx]**3)/StN[indx] 
        rho_pix_raw = np.mean( observations_t[:, ipt]* (SP_norm**3) / (NSP))

        #deubg 
        neighbor_indx = nearPS.IndxM_mask[:, ipt] 
        neighbor_loc = np.unravel_index(neighbor_indx, nearPS.I_mask.shape, order='F')
        pt_irow, pt_icol = neighbor_loc[0][0], neighbor_loc[1][0]
        IMGs_Tterm[pt_irow, pt_icol, :] = T_terms_pix
        Rho_obs_raw[ipt] = rho_pix_raw
        
        Rho_obs[ipt] = rho_pix_
        # Also get the direct compoenent based on rho_pix_ and geometry #
        Id_pix_  = rho_pix_ * np.sum(N_vert_pix_.reshape((3,1)) * SP, axis=0) / \
                (SP_norm ** 3)
        # Id_pix_  = rho_pix_raw * np.sum(N_vert_pix_.reshape((3,1)) * SP, axis=0) / \
        #         (SP_norm ** 3)
        # Id_pix_ = observations_dt[:, ipt] / ( StN / (NSP) + 3 * StP / (SP_norm**2) ) 
        # Id_pix_ = observations_dt[:, ipt] / ( StN / (NSP)  ) 
        Id_obs[:, ipt] = Id_pix_

    IMGs_d = np.zeros((nr, nc, nLED))
    IMGs_g = np.zeros((nr, nc, nLED))
    IMGs_dt = np.zeros((nr, nc, nLED))
    N_vert_map = np.zeros((nr, nc, 3))
    Rho_map = np.zeros((nr, nc))
    Rhoraw_map = np.zeros((nr, nc))
    for ipt in range(nPt_obs):
         neighbor_indx = nearPS.IndxM_mask[:, ipt] 
         neighbor_loc = np.unravel_index(neighbor_indx, nearPS.I_mask.shape, order='F')
         pt_irow, pt_icol = neighbor_loc[0][0], neighbor_loc[1][0]
         IMGs_d[pt_irow, pt_icol, :] = Id_obs[:, ipt]
         IMGs_g[pt_irow, pt_icol, :] = observations_t[:,ipt] - Id_obs[:,ipt]
         IMGs_dt[pt_irow, pt_icol, :] = observations_dt[:, ipt]
         N_vert_map[pt_irow, pt_icol, :] = N_vert_obs[:, ipt]
         Rho_map[pt_irow, pt_icol] = Rho_obs[ipt]
         Rhoraw_map[pt_irow, pt_icol] = Rho_obs_raw[ipt]

    return Rho_obs, Id_obs, IMGs_dt, IMGs_d, IMGs_g, N_vert_map, IMGs_Tterm, Rho_map, Rhoraw_map


def get_sNorm_Idt(nearPS):
    '''
    Get the surface normal and albedo from Idt.  Since Idt can be assumed to
    be free of the global illumination, the surface normal and albedo
    estimation could be more robust to the global illumination components 
    '''

    # Get the point locations for the observed points #
    Indxv_var = nearPS.IndxM_var[0,:]
    depth_cur_obs = nearPS.depth_cur[Indxv_var]
    Kuv_obs = nearPS.KUV_vars[:, Indxv_var]
    P_cur_obs = Kuv_obs * depth_cur_obs.reshape([1, len(depth_cur_obs)])
    S = nearPS.sPS.get_S()
    St = nearPS.sPS.get_St()

    nLED = S.shape[1]
    nPts = P_cur_obs.shape[1] # number of observed points 
    LM = np.zeros((nLED, 3, nPts)) # the lighting matrix 
    LMraw = np.zeros((nLED, 3, nPts)) # the lighting matrix 
    for iLED in range(nLED):
        st = St[:, iLED]
        s = S[:, iLED]
        sp_ = s.reshape((3,1)) -  P_cur_obs 
        stp_ = np.sum(st.reshape((3,1)) * P_cur_obs, axis=0)
        sp_norm = np.linalg.norm(sp_, axis=0)
        Term1_ = np.tile(st.reshape((3,1)), [1, nPts])  / (sp_norm.reshape([1, nPts]) ** 3)
        Term2_ = 3 * sp_ * stp_.reshape((1, len(stp_))) / (sp_norm.reshape([1, nPts]) ** 5)
        LM_ = Term1_ + Term2_
        LM[iLED, :, :] = LM_  
        # For comparison, NOT needed for estimation #
        LMraw[iLED, :, :] = sp_ / (sp_norm**3).reshape([1, nPts])
        # ipdb.set_trace()

    RhoN = np.zeros((3, nPts))
    RhoN_raw = np.zeros((3, nPts))
    Rho = np.zeros(nPts)
    N = np.zeros(RhoN.shape)
    N_raw = np.zeros(RhoN.shape)
    for ipt in range(nPts):
        LM_pix = LM[:,:, ipt]
        pix_Int = nearPS.observations_dt[:, ipt].reshape((nLED, 1))
        rhoN_pix = ( np.linalg.pinv(LM_pix)).dot(pix_Int)
        RhoN[:, ipt] = rhoN_pix.flatten()
        Rho[ipt] = np.linalg.norm(rhoN_pix.flatten())
        N[:, ipt] = RhoN[:, ipt] / Rho[ipt] 
        pix_Int_it = nearPS.observations[:, ipt].reshape((nLED,1))

        RhoN_raw[:, ipt] = (np.linalg.pinv(LMraw[:,:,ipt]).dot(pix_Int_it)).flatten()
        N_raw[:, ipt] = RhoN_raw[:,ipt] / np.linalg.norm(RhoN_raw[:, ipt])

    rhoMap = np.zeros(nearPS.I_mask.shape)
    N_Map = np.zeros((nearPS.I_mask.shape[0], 
        nearPS.I_mask.shape[1], 3))
    N_Map_raw = np.zeros(N_Map.shape)
    for ipt in range(nPts):
         neighbor_indx = nearPS.IndxM_mask[:, ipt] 
         neighbor_loc = np.unravel_index(neighbor_indx, nearPS.I_mask.shape, order='F')
         pt_irow, pt_icol = neighbor_loc[0][0], neighbor_loc[1][0]
         rhoMap[pt_irow, pt_icol] = Rho[ipt]
         N_Map[pt_irow, pt_icol, :] = N[:, ipt]
         N_Map_raw[pt_irow, pt_icol, :] = N_raw[:, ipt]


    # Render the direct-only image #
    RDs = np.zeros((nLED, nPts)) 
    RDs_raw = np.zeros((nLED, nPts)) 
    for iLED in range(nLED):
        s = S[:, iLED]
        sp_ = s.reshape((3,1)) -  P_cur_obs 
        sp_norm = np.linalg.norm(sp_, axis=0)
        Rd = np.sum(RhoN * sp_, axis=0) / (sp_norm ** 3)
        Rd_raw = np.sum(RhoN_raw * sp_, axis=0) / (sp_norm ** 3)
        RDs[iLED, :] = Rd
        RDs_raw[iLED, :] = Rd_raw

    IMGs_d = np.zeros((N_Map.shape[0], N_Map.shape[1], nLED))
    IMGs_g = np.zeros((N_Map.shape[0], N_Map.shape[1], nLED))
    IMGs_d_raw = np.zeros((N_Map.shape[0], N_Map.shape[1], nLED))
    IMGs_Idt_obs = np.zeros((N_Map.shape[0], N_Map.shape[1], nLED))
    for ipt in range(nPts):
         neighbor_indx = nearPS.IndxM_mask[:, ipt] 
         neighbor_loc = np.unravel_index(neighbor_indx, nearPS.I_mask.shape, order='F')
         pt_irow, pt_icol = neighbor_loc[0][0], neighbor_loc[1][0]
         IMGs_d[pt_irow, pt_icol, :] = RDs[:, ipt]
         IMGs_d_raw[pt_irow, pt_icol, :] = RDs_raw[:, ipt]
         IMGs_g[pt_irow, pt_icol, :] = nearPS.observations[:, ipt] - RDs[:, ipt] 
         IMGs_Idt_obs[pt_irow, pt_icol, :] = nearPS.observations_dt[:, ipt] 

    return rhoMap, N_Map , N_Map_raw, IMGs_d, IMGs_g, IMGs_d_raw, IMGs_Idt_obs
    

class NearLight:
    def __init__(self, IMGs, I_mask, S, K_inv, fem_d = 1, scale = 1.0):
        self.IMGs = IMGs 
        self.I_mask = I_mask.astype(np.int)
        self.S = S 
        self.K = K_inv 
        self.nLED = IMGs.shape[2]
        self.dt_recon = False
        # scale #
        self.scale = scale
        # fem_d #
        self.fem_d = fem_d
        # Here we set to Se = 10.0 because Se = 10.0 in the simulation data,
        # we should have a function to set Se in Nearlight and sPs objects 
        self.Se =np.ones( (self.nLED, 1))
        self.lambda_smooth = 0.0 
        self.max_iter = 1000
        self._init_sPS_()
        self.depth_cur = 0
        self.formed_prob = False
        # Initial value for IMG_dt #
        self.IMG_dt = -1
        

    def _init_sPS_(self):
        K = self.K 
        S = self.S 
        I_mask = self.I_mask
        Se = self.Se

        # set Rho to be ones, we should set it to some plausible value before # 
        Rho = np.ones((np.sum(I_mask), 1))
        dMap_dummy = np.zeros(I_mask.shape)
        UV_mask, IndxM_mask, IndxM_var, F_vars, UV_vars, F_local, z_nz, varIndx_Map \
            = z2n.IndM_2d_grid(self.I_mask, dMap_dummy, if_clockwise = False)

        # scale #
        # ipdb.set_trace()
        UV_mask[:2, :] = UV_mask[:2, :] * self.scale
        UV_vars[:2, :] = UV_vars[:2, :] * self.scale

        varIndx_Map = varIndx_Map.astype(np.int64)
        self.varIndx_Map = varIndx_Map
        self.varLoc_multiIndx = np.nonzero(varIndx_Map+1) 
        self.IndxM_mask = IndxM_mask
        self.IndxM_var = IndxM_var
        # self.nPts = np.int(np.sum(self.I_mask)) # number of variable points 
        self.nPts = np.int(len(self.varLoc_multiIndx[0])) 
        self.UV_mask = UV_mask
        self.F_vars = F_vars

        KUV_mask = K.dot(UV_mask)
        I_mask_var = np.zeros(I_mask.shape)
        I_mask_var[varIndx_Map>=0] = 1

        KUV_vars = K.dot(UV_vars)
        V_vars = KUV_vars * z_nz.reshape([1, z_nz.shape[0]])
        V_vars = V_vars.transpose()
        self.KUV_vars = KUV_vars

#        self.nPts = np.sum(self.I_mask)
        # self.nPts = IndxM_mask.shape[1] 

        S_ = S.transpose()
        F_local = F_local.transpose()

        print("NearLight init(): fem_d = {}".format(self.fem_d))
        sPS = simpleNearPS.PS_Problem(S_.reshape(S_.shape, order='F'),
            KUV_mask.reshape(KUV_mask.shape, order='F'),
            Rho.flatten(),
            IndxM_mask.reshape(IndxM_mask.shape, order='F'),
            IndxM_var.reshape(IndxM_var.shape, order='F'),
            F_local.reshape(F_local.shape, order='F'),
            Se.flatten(), self.fem_d ) 

        observations = np.zeros((self.nLED, self.IndxM_mask.shape[1]))
        for ipt in range(observations.shape[1]): 
            neighbor_indx = IndxM_mask[:, ipt] 
            neighbor_loc = np.unravel_index(neighbor_indx, self.I_mask.shape, order='F')
            pt_irow, pt_icol = neighbor_loc[0][0], neighbor_loc[1][0]
            observations[:, ipt] = self.IMGs[pt_irow, pt_icol, :]

        self.observations = observations 
        sPS.set_observations(observations.reshape(observations.shape, order='F'))
        self.sPS = sPS 
        self.observations_dt = self.sPS.get_observations_dt()
        self.observations_t = self.sPS.get_observations()


    def set_Rhos(self, input_Rhos, if_input_rhos_obsev=True):

        if if_input_rhos_obsev is True:
            self.rho_se_cur = input_Rhos.flatten()
            # Set the rhos in sPS, note that in sPS, rhos only includes the albedo for observed points, rather than the variable albedos #
            self.sPS.set_Rhos(self.rho_se_cur)
        else:
            npts_valid = self.IndxM_mask.shape[1]         
            self.rho_se_cur = np.zeros(npts_valid) # rho_se_cur is the albedo for the observations 
            for ipt in range(npts_valid):
                neighbor_indx = self.IndxM_mask[:, ipt] 
                neighbor_loc = np.unravel_index(neighbor_indx, self.I_mask.shape, order='F')
                pt_irow, pt_icol = neighbor_loc[0][0], neighbor_loc[1][0]
                indx_var = self.varIndx_Map[pt_irow, pt_icol]
                self.rho_se_cur[ipt] = input_Rhos[indx_var]
        
    def set_initial_z(self, init_z):
        self.init_z = init_z 
        self.sPS.set_initial_vars(self.init_z.flatten())
        self.depth_cur = self.init_z
        print('Initialized the problem')

    def set_lambda_smooth(self, lam_sm):
        self.lambda_smooth = lam_sm 

    def set_nThreads(self, nThreads= 10):
        self.nThreads = nThreads

    def solve(self):
        self.sPS.set_initial_vars(self.depth_cur.flatten())
        # print('Initialized the problem')
        self.sPS.set_lambda_smooth(self.lambda_smooth)
        self.sPS.set_nThreads(self.nThreads)

        if self.dt_recon is False:
            if self.formed_prob is False:
                self.sPS.form_prob()
                self.formed_prob = True
        else:
            if self.formed_prob is False:
                self.sPS.form_prob_dt()
                self.formed_prob = True

        self.sPS.set_maxIter(self.max_iter)

        self.sPS.solve_prob()
        print('') 
        self.depth_cur = self.sPS.return_depths();

    def solve_albedo(self):
        PtsLocs = self.KUV_vars * self.depth_cur.reshape((1, self.nPts))
        
        # Here npts_valid is the number of observations used in optmization. 
        # Note that npts_valid < self.Npts, Npts is the # of variables.
        npts_valid = self.IndxM_mask.shape[1]         
        self.rho_se_cur = np.zeros(npts_valid) # rho_se_cur is the albedo for the observations 
        self.normal_cur = np.zeros((3, npts_valid))
        for ipt in range(npts_valid):
            neighbor_indx = self.IndxM_mask[:, ipt] 
            neighbor_loc = np.unravel_index(neighbor_indx, self.I_mask.shape, order='F')
            pt_irow, pt_icol = neighbor_loc[0][0], neighbor_loc[1][0]
            indx_var = self.varIndx_Map[pt_irow, pt_icol]
            pix_It = self.IMGs[pt_irow, pt_icol, :].astype(np.float64)
            pix_SP = self.S - PtsLocs[:, indx_var].reshape((1,3))
            norm_pix_SP = np.linalg.norm(pix_SP, axis=1)
            Light_M = pix_SP / norm_pix_SP.reshape((self.nLED, 1))
            pix_LM_inv = np.linalg.pinv(Light_M)
            rho_se_n = pix_LM_inv.dot(pix_It * (norm_pix_SP**2))
            pix_rho_se = np.linalg.norm(rho_se_n)
            self.normal_cur[:, ipt] = rho_se_n / (pix_rho_se + .0000001)
            # self.rho_se_cur[indx_var] = pix_rho_se
            self.rho_se_cur[ipt] = pix_rho_se




    def export_result(self, abs_vmax = 20.0):
        I_mask = self.I_mask 
        N_LED = self.nLED
        IndxM_mask = self.IndxM_mask

        if self.dt_recon is False:
            print('Writing done the current states image intensities')
            self.sPS.est_pixel_values_cur()
            img_int_cur = self.sPS.get_cur_img()
            PixelIntensities_Imgs_curGuess = \
                    np.zeros((I_mask.shape[0], I_mask.shape[1], N_LED))

            npts_valid = IndxM_mask.shape[1]
            for ipt in range(npts_valid):
                neighbor_indx = IndxM_mask[:, ipt] 
                neighbor_loc = np.unravel_index(neighbor_indx, self.I_mask.shape, order='F')
                for ich in range(self.nLED):
                    PixelIntensities_Imgs_curGuess[neighbor_loc[0][0],neighbor_loc[1][0], ich] = \
                            img_int_cur[ich, ipt]
            self.IMGs_cur = PixelIntensities_Imgs_curGuess

            import matplotlib.pyplot as plt
            plt.figure(figsize=(10,10))
            for ich in range(N_LED):
                im2write = PixelIntensities_Imgs_curGuess[:,:,ich]
                im2write[im2write > self.IMGs.max()] = self.IMGs.max() 
                im2write = np.hstack((im2write, self.IMGs[:,:,ich] * self.I_mask))
                plt.imshow(im2write, cmap='gray', vmax=4000) 
                plt.title('Left: re-synthesis; Right: input images')
                plt.savefig('tmp_res/ps_img_cur_guess_{}.png'.format(ich))
                plt.clf()
            plt.close()

        elif self.dt_recon is True:
            self.sPS.est_pixel_dt_values_cur()
            img_int_cur_dt = self.sPS.get_cur_img_dt()
            obs_dt = self.sPS.get_observations_dt()

            PixelIntensities_Imgs_curGuess = \
                    np.zeros((I_mask.shape[0], I_mask.shape[1], N_LED))
            PixelIntensities_obs = \
                    np.zeros((I_mask.shape[0], I_mask.shape[1], N_LED))

            npts_valid = IndxM_mask.shape[1]
            for ipt in range(npts_valid):
                neighbor_indx = IndxM_mask[:, ipt] 
                neighbor_loc = np.unravel_index(neighbor_indx, self.I_mask.shape, order='F')
                for ich in range(self.nLED):
                    PixelIntensities_Imgs_curGuess[neighbor_loc[0][0],neighbor_loc[1][0], ich] = \
                            img_int_cur_dt[ich, ipt]
                    PixelIntensities_obs[neighbor_loc[0][0],neighbor_loc[1][0], ich] = \
                            obs_dt[ich, ipt]

            self.IMG_cur_dt = PixelIntensities_Imgs_curGuess
            self.IMG_dt = PixelIntensities_obs

            # Plot #
            import matplotlib.pyplot as plt
            plt.figure()
            abs_vmax = abs_vmax 
            for ich in range(N_LED):
                im2write = PixelIntensities_Imgs_curGuess[:,:,ich]
                im2write[im2write > self.IMG_dt.max()] = self.IMG_dt.max() 
#                im2write = np.hstack((im2write, self.IMG_dt[:,:,ich] * self.I_mask))
                plt.imshow(im2write, cmap='jet', vmin=-abs_vmax, vmax=abs_vmax) 
                plt.colorbar()
                # plt.title('Left: re-synthesis; Right: input images')
                plt.savefig('tmp_res/ps_img_Idt_cur_guess_{}.png'.format(ich))
                plt.clf()
            plt.close()



    def export_observations(self, vmax_abs=100):
        # CHECK: OK
        observations = self.observations_t 
        IndxM_mask = self.IndxM_mask 
        IMGs_out = np.zeros(self.IMGs.shape)

        print('EXPORTING ...')
        if self.dt_recon is False:
            for ipt in range(observations.shape[1]): 
                neighbor_indx = IndxM_mask[:, ipt] 
                neighbor_loc = np.unravel_index(neighbor_indx, self.I_mask.shape, order='F')
                pt_irow, pt_icol = neighbor_loc[0][0], neighbor_loc[1][0]
                IMGs_out[pt_irow, pt_icol, :] = observations[:, ipt]
            import matplotlib.pyplot as plt
            for ich in range(self.nLED):
                plt.imshow(IMGs_out[:,:,ich], cmap='gray', vmax=4000) 
                plt.colorbar()
                plt.savefig('tmp_res/ps_img_observations_{}.png'.format(ich))
                plt.clf()
        else:
            import matplotlib.pyplot as plt
            for ich in range(self.nLED):
                plt.imshow(self.IMG_dt[:,:,ich], cmap='jet', vmin=-vmax_abs, vmax=vmax_abs) 
                plt.colorbar()
                plt.savefig('tmp_res/ps_img_Idt_observations_{}.png'.format(ich))
                plt.clf()




    def export_dmap(self, vrange = [100, 500]):
        '''
        Export the depth map
        '''
        dmap = np.zeros(self.I_mask.shape)
        img_rhose_map = np.zeros(self.I_mask.shape)
        normal_map = np.zeros((self.I_mask.shape[0], self.I_mask.shape[1], 3))

        nPix = dmap.shape[0] * dmap.shape[1]
        for ipt in range(nPix):
            loc_ = np.unravel_index(ipt, dmap.shape, order='C')
            if self.varIndx_Map[loc_] >= 0:
                dmap[loc_] = self.depth_cur[self.varIndx_Map[loc_]]

        IndxM_mask = self.IndxM_mask
        for ipt in range(IndxM_mask.shape[1]): 
            neighbor_indx = IndxM_mask[:, ipt] 
            neighbor_loc = np.unravel_index(neighbor_indx, self.I_mask.shape, order='F')
            pt_irow, pt_icol = neighbor_loc[0][0], neighbor_loc[1][0]
            img_rhose_map[pt_irow, pt_icol] = \
                self.rho_se_cur[ipt]
            normal_map[pt_irow, pt_icol,:] = self.normal_cur[:, ipt]

        self.dmap_opt = dmap
        self.img_rhose_map = img_rhose_map
        self.normal_map = normal_map 

        import matplotlib.pyplot as plt
        if len(vrange) == 0 :
            plt.imshow(dmap, cmap='jet', vmax= dmap.max(), vmin= dmap.min())
        else:
            plt.imshow(dmap, cmap='jet', vmax=vrange[0], vmin=vrange[1])

        plt.colorbar()
        plt.savefig('tmp_res/dmapC.png')
        plt.close()
        plt.imshow(self.img_rhose_map, cmap='gray')
        plt.colorbar()
        plt.savefig('tmp_res/img_rhose.png')
        plt.close()


    def export_mat(self, fname = 'tmp_res/cur_depth_estimation.mat',
        if_dgnd=False):
        '''
        if_dgnd: if we have gnd depth. If we have it, we export it 
        '''
        self.export_dmap()

        depth_cur = self.sPS.return_depths();
        mat_dic = {}
        print('near_PS(): Exporting mat ... ')
        # mat_dic = {'z_cur': depth_cur, 'I_mask': self.I_mask, 'K': self.K, 
        #            'lambda_smooth':self.lambda_smooth, 'N_LED':self.nLED, 'S': self.S} 
        if if_dgnd is False:
            mat_dic = {\
            'z_cur': self.depth_cur, 'I_mask': self.I_mask, 'varIndx_Map': self.varIndx_Map,
            'KUV_vars': self.KUV_vars, 
            'F_vars': self.F_vars, 'K': self.K, 
            'lambda_smooth':self.lambda_smooth, 
            'normal_map': self.normal_map, 
            'albedo_map': self.img_rhose_map,
            'depth_map': self.dmap_opt,
            'z_init': self.init_z}
        else:
            assert np.sum(self.z_gnd ) > 0, 'Failed to validte the z_gnd variable. Did you set it correctly ?'
            mat_dic = {\
            'z_cur': self.depth_cur, 'I_mask': self.I_mask, 'varIndx_Map': self.varIndx_Map,
            'KUV_vars': self.KUV_vars, 
            'F_vars': self.F_vars, 'K': self.K, 
            'lambda_smooth':self.lambda_smooth,
            'z_gnd': self.z_gnd, 'z_init': self.init_z}

        import scipy.io as sio
        sio.savemat(fname, mat_dic)
        print('Done exported results in {}'.format(fname))
        return(mat_dic)




########################
#METHODS FOR COMPARISON
########################

class NearLight_Mat:
    '''
    Variables:
      I : the input images WxHx nimg 3D tensor
      S : the light source positions, nimg x 3
      K : the intrinsic camera matrix 
    '''

    I = []
    S = []
    K = []
    mask = []
    params_nearPS = {'ratio':1, 'init_d': 200, 'Phi': 0} 

    def __init__(self, I,mask, S, K, init_d):
        '''
        Inputs:
            I, S, K - 
                I : the input images WxHx nimg 3D tensor
                S : the light source positions, nimg x 3
                K : the intrinsic camera matrix 
        '''
        self.I = I
        self.S = S
        self.K = K
        self.mask = mask
        nimg = self.I.shape[2]
        self.Phi = np.ones((nimg,1)) 
        self.init_d = init_d


    def set_params_nearPS(self, Phi, ratio=1, init_d=200, mu=1):
        self.params_nearPS['ratio'] = ratio
        self.params_nearPS['init_d'] = self.init_d
        self.params_nearPS['mu'] = mu

        self.Phi = Phi
    
    def solve(self, if_export=True):
        '''
        This function solves the photometric stereo under nearby point light source
        inputs:
         if_export - if export the 3D result as .obj format
        outputs:
         XYZ, N, rho, mask, Phi
        Reference:
        [1] LED-based Photometric Stereo: Modeling, Calibration and Numerical Solution. In arxiv 2017
        '''
    
        tmp_dir = 'matfile_tmp'
        if os.path.isdir(tmp_dir):
            shutil.rmtree(tmp_dir)
    
        os.mkdir(tmp_dir)
    
        # save the variables into .mat file 
        varpath = {}
        varpath['base'] = './{}'.format(tmp_dir)
        varpath['I'] = './{}/nearPS_I.mat'.format(tmp_dir)
        varpath['S'] = './{}/nearPS_S.mat'.format(tmp_dir)
        varpath['K'] = './{}/nearPS_K.mat'.format(tmp_dir)
        varpath['Phi'] = './{}/nearPS_Phi.mat'.format(tmp_dir)
        I_dic = {}; S_dic={}; K_dic = {};
        Phi_dic = {};
        I_dic['I'] = self.I
        I_dic['mask'] = self.mask
        S_dic['S'] = self.S
        K_dic['K'] = self.K
        Phi_dic['Phi'] = self.Phi
        sio.savemat(varpath['I'], I_dic)
        sio.savemat(varpath['S'], S_dic)
        sio.savemat(varpath['K'], K_dic)
        sio.savemat(varpath['Phi'], Phi_dic)
    
        # run the matlab interface
        print('starting Matlab ...')
        mat_eng = matlab.engine.start_matlab()
        mat_eng.addpath('./matlab', nargout=0)
        mat_eng.mat_install(nargout=0)
        print('done starting Matlab ') 

        var_path = mat_eng.nearPS(varpath, self.params_nearPS, if_export, nargout=1) 
    
        # get the outputs
        res_info = sio.loadmat(varpath['base'])
        XYZ = res_info['XYZ']
        N = res_info['N']
        rho = res_info['rho']
        mask = res_info['mask']
        Phi = res_info['Phi']
        shutil.rmtree(tmp_dir)
        mat_eng.quit()
        return (XYZ, N, rho, mask, Phi)

if __name__ == '__main__':

    dat_name = 'sim' # {'sim', 'Statuette'}
#    dat_name = 'Statuette'
    print(dat_name)

    #load data
    if dat_name is 'Statuette':
        dat_info = sio.loadmat(
                '/home/chaoliu1/PS_BSSRDF/ref/near_ps/Datasets/Statuette/inputImg.mat')
        I = dat_info['I']
        S = dat_info['calib']['S'][0,0]
        K = dat_info['calib']['K'][0,0]
        mask = dat_info['mask']
        #optmization prameters
        nimg = I.shape[2]
        init_depth = 700.0 
        ratio = 8
        Phi = np.ones((nimg,1)) * 100
        mu = 1
    elif dat_name is 'sim':
        I = io_EXR.readEXR_fldr('/home/chaoliu1/PS_BSSRDF/data/SmallRing', 
                prefix = 'FractalTerrain', index=np.arange(1,21), if_grayscale=True) 
        calib_info = sio.loadmat('/home/chaoliu1/PS_BSSRDF/data/SmallRing/calib_info.mat')
        S = calib_info['S']
        K = calib_info['K']
        K[0,0] = 41.0
        K[1,1] = 41.0
        mask = np.zeros((I.shape[1], I.shape[1]))
        mask = np.sum(I,axis=2) > 0 
        nimg = I.shape[2]
        #optmization prameters
        init_depth = 40.0 
        ratio = 1.0 
        Phi = np.ones((nimg,1)) * 1.0 
        mu = 0.0 


    from nearLight import near_ps

    NearL = near_ps.NearLight(I,mask,S,K )
    NearL.set_params_nearPS(Phi, mu=mu, ratio=ratio, init_d= init_depth)
    XYZ, N, rho,mask, Phi  = NearL.solve(if_export = True)
    XYZ_mask = np.zeros(XYZ.shape) 
    XYZ_mask[ np.stack([mask, mask, mask], axis=-1)>0 ]=XYZ[np.stack(
        [mask, mask, mask], axis=-1)>0]


def COMP_DISTL_img(IMGs, S, I_mask, d_init):  
    '''
    Distant-light PS
    '''
    nr, nc = IMGs.shape[0], IMGs.shape[1]
    NMap = np.zeros((nr, nc, 3))

    ref_vert_loc = np.asarray([0, 0, d_init])
    S_distant = S - ref_vert_loc.reshape([1,3])
    Sinv_dist = np.linalg.pinv(S_distant)

    for irow, icol in zip(np.nonzero(I_mask)[0],np.nonzero(I_mask)[1]):
        observations = IMGs[irow, icol, :]
        N_pix = Sinv_dist.dot(observations)
        NMap[irow, icol,:] = N_pix.flatten() / np.linalg.norm(N_pix.flatten())

    return NMap
    
