# Get the reconstruction using 
# [1] LED-based Photometric Stereo: Modeling, Calibration and Numerical Solution. In arxiv 2017
import numpy as np
import os
import shutil
import scipy.io as sio

class NearLight_Mat:
    '''
    Variables:
      I : the input images WxHx nimg 3D tensor
      S : the light source positions, nimg x 3
      K : the intrinsic camera matrix 
    '''

    I = []
    S = []
    K = []
    mask = []
    params_nearPS = {'ratio':1, 'init_d': 435, 'Phi': 0} 

    def __init__(self, I,mask, S, K, init_d):
        '''
        Inputs:
            I, S, K - 
                I : the input images WxHx nimg 3D tensor
                S : the light source positions, nimg x 3
                K : the intrinsic camera matrix 
        '''
        self.I = I
        self.S = S
        self.K = K
        self.mask = mask
        nimg = self.I.shape[2]
        self.Phi = np.ones((nimg,1)) 
        self.init_d = init_d

        self.params_nearPS['ratio'] = 1
        self.params_nearPS['init_d'] = self.init_d
        self.params_nearPS['mu'] = 1.0

    def set_params_nearPS(self, Phi, ratio=1, init_d=400, mu=1):
        self.params_nearPS['ratio'] = ratio
        self.params_nearPS['init_d'] = self.init_d
        self.params_nearPS['mu'] = mu

        self.Phi = Phi
    
    def solve(self, if_export=True):
        '''
        This function solves the photometric stereo under nearby point light source
        inputs:
         if_export - if export the 3D result as .obj format
        outputs:
         XYZ, N, rho, mask, Phi
        Reference:
        [1] LED-based Photometric Stereo: Modeling, Calibration and Numerical Solution. In arxiv 2017
        '''
    
        tmp_dir = 'matfile_tmp'
        if os.path.isdir(tmp_dir):
            shutil.rmtree(tmp_dir)
    
        os.mkdir(tmp_dir)
    
        # save the variables into .mat file 
        varpath = {}
        varpath['base'] = './{}'.format(tmp_dir)
        varpath['I'] = './{}/nearPS_I.mat'.format(tmp_dir)
        varpath['S'] = './{}/nearPS_S.mat'.format(tmp_dir)
        varpath['K'] = './{}/nearPS_K.mat'.format(tmp_dir)
        varpath['Phi'] = './{}/nearPS_Phi.mat'.format(tmp_dir)
        I_dic = {}; S_dic={}; K_dic = {};
        Phi_dic = {};
        I_dic['I'] = self.I
        I_dic['mask'] = self.mask
        S_dic['S'] = self.S
        K_dic['K'] = self.K
        Phi_dic['Phi'] = self.Phi
        sio.savemat(varpath['I'], I_dic)
        sio.savemat(varpath['S'], S_dic)
        sio.savemat(varpath['K'], K_dic)
        sio.savemat(varpath['Phi'], Phi_dic)
    
        # run the matlab interface
        print('starting Matlab ...')
        import matlab.engine
        mat_eng = matlab.engine.start_matlab()
        mat_eng.addpath('./matlab', nargout=0)
        mat_eng.mat_install(nargout=0)
        print('done starting Matlab ') 

#        import ipdb
#        ipdb.set_trace()
        var_path = mat_eng.nearPS(varpath, self.params_nearPS, if_export, nargout=1) 
    
        # get the outputs
        res_info = sio.loadmat(varpath['base'])
        XYZ = res_info['XYZ']
        N = res_info['N']
        rho = res_info['rho']
        mask = res_info['mask']
        Phi = res_info['Phi']
        shutil.rmtree(tmp_dir)
        mat_eng.quit()
        return (XYZ, N, rho, mask, Phi)
