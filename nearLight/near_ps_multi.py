import numpy as np
import matplotlib.pyplot as plt
import scipy.io as sio 

# opencv #
import cv2
# Initializer #
from nearLight import initializer as dIniter 
# Refinement #
from nearLight import near_ps

import math
import ipdb

'''
Muti-scale photometric stereo
'''

# -------- HELPER FUNCTIONS ----------#
def _downSampleImg(img_in, nDown):
    img_per = img_in.copy()
    nDown = np.int(nDown)
    for idown in range(nDown):
        if idown == 0:
            img_per = cv2.pyrDown(img_in)
        else:
            img_per = cv2.pyrDown(img_per)
    return img_per

def _upSampleImg(img_in,nUp, if_fill_boundary = True, erode_boundar = 2 ):
    img_per = img_in.copy()
    nUp = np.int(nUp)
    for idown in range(nUp):
        if idown == 0:
            img_per = cv2.pyrUp(img_in)
        else:
            img_per = cv2.pyrUp(img_per)

        # fill the boundary values with the mean value of the non-zeros #
        if if_fill_boundary is True:
            import scipy.ndimage.morphology as sci_morph
            I_mask = img_per > 0
            I_maske = sci_morph.binary_erosion(I_mask, iterations= erode_boundar) 
            I_mask_bound = I_mask ^ I_maske
            vmean_I_mask = img_per[np.nonzero(I_mask)].mean()
            img_per[np.nonzero(I_mask_bound)] = vmean_I_mask
    return img_per

def _upSampleRes(nearPS, diniter, nUp, if_fill_boundary=True, erode_boundar = 2, dmap_range= [100, 500]):
    I_mask = diniter.varIndx_Map > -1
    nearPS.export_dmap(vrange = dmap_range)

    print('export dmap done !')
    rhose_map = nearPS.img_rhose_map
    dmap = nearPS.dmap_opt

    rhose_map_up = _upSampleImg(rhose_map,nUp,  if_fill_boundary, erode_boundar)
    dmap_up = _upSampleImg(dmap ,nUp,  if_fill_boundary, erode_boundar)
    I_mask_up = dmap_up > 0
    print('up-sampling done !')
    print('upsampled image size is: {}\n\n'.format(rhose_map_up.shape))

    varIndx_map = diniter.varIndx_Map
    rho_se_up = np.zeros(varIndx_map.max()+1)
    z_cur_up = np.zeros(varIndx_map.max()+1)

    for irow, icol in zip( np.nonzero(I_mask)[0], np.nonzero(I_mask)[1]):
        z_cur_up[ varIndx_map[irow, icol] ] = dmap_up[irow, icol]
        rho_se_up[ varIndx_map[irow, icol] ] = rhose_map_up[irow, icol]

    return z_cur_up, rho_se_up


# -------- CLASS: NearLightMulti --------- #
class NearLightMulti:
    '''
    We will do BOTH INITIALIZATION and REFINEMENT in MULTIPLE scales 
    input: Scales, for example: [4, 2, 1] , from lower-res to higher-res
         1 is the original scale; 2 is half size of the original scale #
    '''
    def __init__(self, IMGs_raw, S, K_inv, Scales, fem_d = 1 ):
        self.IMGs = IMGs_raw # The images in the raw scale 
        self.S = S 
        self.K_inv = K_inv 
        self.nLED = IMGs_raw.shape[2]
        self.dt_recon = False
        self.fem_d = fem_d
        # Scales, for example: [4, 2, 1] #
        # 1 is the original scale; 2 is half size of the original scale #
        self.Scales = Scales
        self.nScale = len(Scales)

        # PS result for different scales #
        self.PS_Idt_multiscale = [0] * self.nScale 
        self.PS_It_multiscale  = [0] * self.nScale

        # exported depth map range
        self.drange_export = []

    def set_paras(self, nThreads=12, dmin=100, dmax=500, ncandi=100, 
            lam_smooth_Idt=200.0, lam_smooth_It = 1000.0, fem_idt_refine=1, MAX_ITER_Idt=5,
            MAX_ITER_It=20,  max_Iter_PS_Idt=500, max_Iter_PS_It=800, drange_export = [],
            if_sm_dimnish=True, if_Idt_sm_diminish=True):
        '''
        Set the parameters for optimizations for ALL SCALES
        '''
        self.nThreads = nThreads

        # initialization #
        self.dmin = dmin 
        self.dmax = dmax  # dmin and dmax for the first initialization using Initializer
        self.ncandi = ncandi 
        self.lam_smooth_Idt = lam_smooth_Idt 

        # refine #
        self.lam_smooth_It = lam_smooth_It 
        self.fem_idt_refine = fem_idt_refine 
        self.MAX_ITER_Idt = MAX_ITER_Idt 
        self.MAX_ITER_It = MAX_ITER_It 

        self.max_Iter_PS_Idt = max_Iter_PS_Idt 
        self.max_Iter_PS_It = max_Iter_PS_It 
        nScales = len(self.Scales)

        if type(self.max_Iter_PS_Idt) is not np.ndarray:
            self.max_Iter_PS_Idt = np.ones( nScales)
            self.max_Iter_PS_Idt[:] = max_Iter_PS_Idt
        if type(self.max_Iter_PS_It) is not np.ndarray:
            self.max_Iter_PS_It = np.ones( nScales)
            self.max_Iter_PS_It[:] = max_Iter_PS_It
            

        self.if_sm_dimnish = if_sm_dimnish
        self.if_Idt_sm_diminish = if_Idt_sm_diminish

        # parameters #
        Paras = {}
        Paras['nThreads'] = nThreads 
        Paras['drange'] = [dmin, dmax]
        Paras['ncandi'] = ncandi 
        Paras['lam_smooth_Idt'] = lam_smooth_Idt 
        Paras['lam_smooth_It'] = lam_smooth_It 
        Paras['MAX_ITER_Idt'] = MAX_ITER_Idt 
        Paras['MAX_ITER_It'] = MAX_ITER_It
        Paras['max_Iter_PS_Idt'] = max_Iter_PS_Idt 
        Paras['max_Iter_PS_It'] = max_Iter_PS_It 
        Paras['if_It_sm_dimnish'] = if_sm_dimnish
        Paras['if_Idt_sm_dimnish'] = if_Idt_sm_diminish
        self.Paras = Paras

        # export #
        self.drange_export = drange_export
        self.fname_It_res = None

    def save_paras(self, fname = './tmp_res/multi_opt_paras.txt'):
        # import scipy.io as sio
        # sio.savemat(fname, self.Paras)
        txtFile = open(fname, 'w')
        for para in self.Paras.keys():    
            txtFile.write('{}:  {}\n'.format(para, self.Paras[para]))
        txtFile.close()


    def solve_scale(self, iscale, step_fem = 3, mean_init_d=True):
        '''
        Initialize & Refine for a specific scale
        '''
        
        assert iscale < self.nScale,  "solve_scale(): input iscale should < nScale." 
        cur_scale = self.Scales[iscale] # The current scale 

        # Set the parameters for initialization and optimization #
        nThreads = self.nThreads
        dmin = self.dmin 
        dmax = self.dmax 
        ncandi = self.ncandi 
        lam_smooth_Idt = self.lam_smooth_Idt
        fem_idt_refine = self.fem_idt_refine
        MAX_ITER_Idt = self.MAX_ITER_Idt
        max_Iter_PS_Idt = self.max_Iter_PS_Idt[iscale] 
        lam_smooth_It =  self.lam_smooth_It
        MAX_ITER_It = self.MAX_ITER_It 
        max_Iter_PS_It = self.max_Iter_PS_It[iscale]
        if_sm_dimnish = self.if_sm_dimnish 

        # Prepare the variables #
        IMGs_LED_normalized = self.IMGPyramid[iscale]
        I_mask_var = self.ImaskPyramid[iscale]
        S = self.S
        K_inv = self.K_inv

        # Initialize #
        if iscale == 0: # only perofrm initialization for the lowest resolution
            print('Initialize ...')
            diniter = dIniter.Initer(IMGs_LED_normalized, I_mask_var, K_inv, S, fem_d = step_fem, scale = cur_scale, perform_knn_pix=False )
            diniter.set_zcandi(dmin=dmin, dmax=dmax, n_candi = ncandi)
            diniter.initialize_z_Idt()
            diniter.export_init_z_Idt()
            diniter.export_snorm()

            # debug: 
            #what if use constant value for initialization ? # 
            if mean_init_d is True:
                diniter.depth_init_Idt[:] =  diniter.depth_init_Idt[:].mean()

            # write the initial depth map 
            plt.imsave(arr = diniter.img_dmap_init_Idt, fname='tmp_res/dmap_init_Idt_navie.png', 
                    cmap='jet', vmin=0, vmax=self.dmax)
            rho_se_init = diniter.rhose
            spix_depth_init_Idt = diniter.depth_init_Idt
            spix_depth_init_Idt[:] = spix_depth_init_Idt

        else: # if we already did something in the lower scale ..
            nUp = np.int(np.log2( self.Scales[iscale - 1] / self.Scales[iscale]))
            diniter = dIniter.Initer(IMGs_LED_normalized, I_mask_var, K_inv, S,
                    fem_d = step_fem, scale = cur_scale, perform_knn_pix=False)
            z_cur_up, rho_se_up = _upSampleRes(self.PS_It_multiscale[0][-1], diniter, nUp, dmap_range= (self.dmin, self.dmax) )
            spix_depth_init_Idt = z_cur_up 
            rho_se_init = rho_se_up 
            lam_smooth_Idt /= nUp
            lam_smooth_It  /= nUp
        
        print('Optimization using Idt')
        # OPTIMIZATION USING Idt#
        if_dt_recon = True # Use It or use Idt
        # Get prepared #
        PS_nearL = near_ps.NearLight(IMGs_LED_normalized, I_mask_var, S, K_inv, 
                fem_d = fem_idt_refine, scale = cur_scale)
        PS_nearL.dt_recon = if_dt_recon
        PS_nearL.set_Rhos(rho_se_init, if_input_rhos_obsev=False)

        PS_nearL.set_lambda_smooth(lam_smooth_Idt)
        # More iterations for the first loop in Idt #
        if iscale==0:
            PS_nearL.max_iter = max_Iter_PS_Idt * 2
            # PS_nearL.max_iter = max_Iter_PS_Idt * 3
        else:
            PS_nearL.max_iter = max_Iter_PS_Idt

        PS_nearL.set_nThreads(nThreads)
        PS_nearL.set_initial_z(spix_depth_init_Idt)
        PS_NEARL = []
        PS_NEARL.append(PS_nearL)
        # After each z-iteration, we also add a rho-iteration where rho_se is
        # updated based on the current guess #
        lam_smooth_Idt_cur = lam_smooth_Idt
        for Iter in range(MAX_ITER_Idt):
            PS_NEARL[Iter].solve()
            print('Solving for albedo...')
            PS_NEARL[Iter].solve_albedo()
            print('Done')
            PS_NEARL[Iter].export_dmap(vrange = self.drange_export)

            PS_NEARL[Iter].export_mat(fname='tmp_res/cur_EXP_Idt_depth_est_scale_{}_iter{}.mat'.format(cur_scale,Iter), 
                    if_dgnd = False);
            # Prepare for the next iteration #
            if Iter+1 < MAX_ITER_Idt:
                PS_nearL_ = near_ps.NearLight(IMGs_LED_normalized, I_mask_var, S, K_inv, 
                        fem_d = fem_idt_refine, scale = cur_scale)
                PS_NEARL.append(PS_nearL_)
                PS_NEARL[Iter+1].dt_recon = PS_NEARL[Iter].dt_recon
#                PS_NEARL[Iter+1].set_Rhos(PS_NEARL[Iter].rho_se_cur, if_input_rhos_obsev=True)
                PS_NEARL[Iter+1].set_nThreads(nThreads)

                # reduce the smoothness constraint over iterations #
                if self.if_Idt_sm_diminish is True:
                    lam_smooth_Idt_cur /= 2.0

                PS_NEARL[Iter+1].set_lambda_smooth(lam_smooth_Idt_cur)
                PS_NEARL[Iter+1].set_initial_z(PS_NEARL[Iter].depth_cur)
                PS_NEARL[Iter+1].max_iter = max_Iter_PS_Idt

        print('Idt Optimization done!\n')
        print('*********************************')
        print('\n\n\n')

        self.PS_Idt_multiscale[iscale] = PS_NEARL

        # Refinement #
         #OPTIMIZATION USING It# 
        # Smoothness for It
        if_dt_recon = False # Use It or use Idt
        # Get prepared #
        PS_nearL = near_ps.NearLight(IMGs_LED_normalized, I_mask_var, S, K_inv, scale = cur_scale )
        PS_nearL.dt_recon = if_dt_recon
        PS_nearL.set_Rhos(PS_NEARL[-1].rho_se_cur, if_input_rhos_obsev=True)
        PS_nearL.set_lambda_smooth(lam_smooth_It)
#        PS_nearL.max_iter = max_Iter_PS_It * 5 # We will do more iterations for the first loop. The first loop is with large lambda_smooth
        PS_nearL.max_iter = max_Iter_PS_It  
        PS_nearL.set_nThreads(nThreads)
        PS_nearL.set_initial_z(PS_NEARL[-1].depth_cur)
        PS_NEARL_It = []
        PS_NEARL_It.append(PS_nearL)
        # After each z-iteration, we also add a rho-iteration where rho_se is
        # updated based on the current guess #
        lam_smooth_It_cur = lam_smooth_It * 1.0
        for Iter in range(MAX_ITER_It):
            PS_NEARL_It[Iter].solve()
            print('Solving albedo...')
            PS_NEARL_It[Iter].solve_albedo()
            print('Done')
            PS_NEARL_It[Iter].export_dmap(vrange = self.drange_export)

            if self.fname_It_res is not None:
                PS_NEARL_It[Iter].export_mat(fname=self.fname_It_res, if_dgnd = False)
            else:
                PS_NEARL_It[Iter].export_mat(fname= 'tmp_res/cur_EXP_It_depth_est_scale_{}_iter{}.mat'.format(cur_scale,Iter), if_dgnd = False)

            # Prepare for the next iteration #
            if Iter+1 < MAX_ITER_It:
                PS_nearL_ = near_ps.NearLight(IMGs_LED_normalized, I_mask_var, S, K_inv, scale = cur_scale)
                PS_NEARL_It.append(PS_nearL_)
                PS_NEARL_It[Iter+1].dt_recon = PS_NEARL_It[Iter].dt_recon
                PS_NEARL_It[Iter+1].set_Rhos(PS_NEARL_It[Iter].rho_se_cur, if_input_rhos_obsev=True)
                PS_NEARL_It[Iter+1].set_nThreads(nThreads)
                
                # reduce the smoothness constraint over iterations #
                if if_sm_dimnish is True:
                    lam_smooth_It_cur /= 2.0
        
                PS_NEARL_It[Iter+1].set_lambda_smooth(lam_smooth_It_cur)
                PS_NEARL_It[Iter+1].set_initial_z(PS_NEARL_It[Iter].depth_cur)
                PS_NEARL_It[Iter+1].max_iter = max_Iter_PS_It

        print('It Optimization done!')
        self.PS_It_multiscale[iscale] = PS_NEARL_It


    def build_ImgPyramid(self):
        '''
        Build the image pyramids 
        IMGPyramid would be a list of image stacks in the same order as the list : self.Scales
        Same applies to ImaskPyramid
        '''
        nscale = len(self.Scales)
        assert self.Scales[-1] == 1, "Scales should include the original scale"
        self.IMGPyramid   = []
        self.ImaskPyramid = []
        for iscale in range(nscale):
            scale_per = self.Scales[iscale]
            nDown = np.int(math.log2(scale_per))
            IMGs_d_ = []
            for iLED in range(self.nLED):
#                ipdb.set_trace()
                IMGs_d_.append(_downSampleImg(self.IMGs[:,:,iLED], nDown))

            self.IMGPyramid.append(np.dstack(tuple(IMGs_d_)))
            self.ImaskPyramid.append(_downSampleImg(self.I_mask_var, nDown)>0)

    def est_Imask(self, X_in, thresh = 3000.0, if_sel = False, if_roipoly = False, 
            remove_spcular = False, thresh_specular = 3600, if_remove_smallC = True):
        '''
        Get I_mask in the raw scale 
        '''
        from z2n import Zmap2Nmap as z2n

        if if_roipoly is False:
            if if_sel is True:
                plt.figure() 
                plt.imshow(np.max(self.IMGs, axis=2))
                plt.title('Select the mask')
                X = plt.ginput(2)
                plt.close()
            else:
                X = X_in

            if len(X) == 0:
                # Use the entire image #
                X = [[1, 1], [self.IMGs.shape[1]-1, self.IMGs.shape[0]-1]]
                X = np.asarray(X)

            # Assign I_mask #
            I_mask = np.zeros((self.IMGs.shape[0], self.IMGs.shape[1]))
            I_mask_sel = I_mask.copy()
            I_mask_sel[np.int(X[0][1]):np.int(X[1][1]), np.int(X[0][0]):np.int(X[1][0])] = True
        else:
            plt.figure()
            plt.imshow(np.log(np.max(self.IMGs, axis=2)))
            plt.title('Select the polygon ROI')
            from helper.roipoly import roipoly
            ROI = roipoly.roipoly(roicolor = 'r')
            I_mask_sel = ROI.getMask(self.IMGs[:,:,0])
            plt.close()

        I_mask = I_mask_sel * np.sum(self.IMGs, axis=2) > thresh 

        if remove_spcular is True: # remove the bright region.
            I_mask *= (np.max(self.IMGs, axis=2) < thresh_specular)

        _, _, _, _, _, _, _, varIndx_Map \
                = z2n.IndM_2d_grid(I_mask, np.zeros(I_mask.shape), if_clockwise = False)
        I_mask_var = np.zeros(I_mask.shape)
        I_mask_var[varIndx_Map>=0] = 1

        if if_remove_smallC is True:
            import skimage.morphology as smorph 
            I_mask_var_in = I_mask_var.astype(np.bool)
            I_mask_var = smorph.remove_small_objects(I_mask_var_in, min_size=1000)
            
        self.I_mask_var = I_mask_var.astype(np.float32)

    def getScales(self):
        '''
        Return the scales
        '''
        return self.Scales
