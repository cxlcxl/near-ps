import numpy as np
import scipy.io as sio
import matplotlib.pyplot as plt
import ipdb
import math
import time

from nearLight import initializer as dIniter 
# ==== The simulation module for testing different samples, imaging setups and so on ==== #


class Testor:
    def __init__(self, K_inv, gndPath):
        self.K_inv = K_inv 
        self.gndPath = gndPath

        # Optimization paramters: initializations #
        self.d_min = 2.0 
        self.d_max = 100.0

        # Optimization parameters: refinement #
        self.lambda_smooth_It =  10.0
        self.lambda_smooth_Idt = .1
        self.MAX_ITER = 10

    def setupCapture(self, LED_intensity, LED_R, N_LED):
        self.LED_intensity = LED_intensity
        self.LED_R = LED_R 
        self.N_LED = N_LED

    def genImages(self, morph_iter=1):
        # Scene info, we will use the surface normal from Zmap2Nmap()
        f_path = self.gndPath
        mat_info = sio.loadmat(f_path)
        dMap = mat_info['depthMap'].astype(np.float64)
        I_mask = dMap >0

        # Erode I_mask #
        import scipy.ndimage.morphology as sci_morph
        I_mask = sci_morph.binary_erosion(I_mask, iterations= morph_iter)
        npts = np.sum(I_mask)

        #Surface albedo info#
        Rho = np.ones((npts, 1))
        #Light source info#
        from simulation import Generator
        sg_near =  Generator.Generator('' , '')
        sg_near.set_LEDRing_NearL( LED_r = self.LED_R, LED_z = 0., nLED = self.N_LED, LED_int = self.LED_intensity);
        S = sg_near.Lpos
        nlight = S.shape[0]
        Se = np.ones( (nlight, 1)) * self.LED_intensity 

        from autodiff import simpleNearPS
        #Test The simple_nearPS module #
        from z2n import Zmap2Nmap as z2n
        UV_mask, IndxM_mask, IndxM_var, F_vars, UV_vars, F_local, z_nz, varIndx_Map \
                = z2n.IndM_2d_grid(I_mask, dMap, if_clockwise = False)
        KUV_mask = self.K_inv.dot(UV_mask)
        I_mask_var = np.zeros(I_mask.shape)
        I_mask_var[varIndx_Map>=0] = 1

        KUV_vars = self.K_inv.dot(UV_vars)
        V_vars = KUV_vars * z_nz.reshape([1, z_nz.shape[0]])
        V_vars = V_vars.transpose()

        npts_valid = IndxM_mask.shape[1]
        S_ = S.transpose()
        F_local = F_local.transpose()
        sPS = simpleNearPS.PS_Problem(S_.reshape(S_.shape, order='F'),
                KUV_mask.reshape(KUV_mask.shape, order='F'),
                Rho.flatten(),
                IndxM_mask.reshape(IndxM_mask.shape, order='F'),
                IndxM_var.reshape(IndxM_var.shape, order='F'),
                F_local.reshape(F_local.shape, order='F'),
                Se.flatten(), 1) 

        nmap = np.zeros((I_mask.shape[0], I_mask.shape[1], 3))

        # For each pixel, create a PixelReconstruct() object. Then estimate its vertice normal#  
        PixelIntensities = np.zeros((npts_valid, self.N_LED))
        PixelIntensities_Imgs = np.zeros((I_mask.shape[0], I_mask.shape[1], self.N_LED))
        observations = np.zeros((self.N_LED, npts_valid))

        print('Getting the observations...')
        N_vert_obs = np.zeros((3, npts_valid))
        St = sPS.get_St().transpose()
        for ipt in range(npts_valid): # for all observations
            kuv_pix =  sPS.return_pixelKuv(ipt) # kuv in the ring-1 neighbor of the current pixel
            spixel = simpleNearPS.PixelReconstruct(kuv_pix, S_.reshape(S_.shape, order='F'),
                    F_local.reshape(F_local.shape, order='F'), Se.flatten(), Rho[ipt] )
            dummy_inten_ = 0.0
            neighbor_indx = IndxM_mask[:, ipt] 
            neighbor_loc = np.unravel_index(neighbor_indx, dMap.shape, order='F')
            z_ = dMap[neighbor_loc]
            spixel.pixel_reconT(z_[0], z_[1], z_[2], z_[3], z_[4], z_[5], z_[6],  dummy_inten_)
           
            N_vert = spixel.get_N() 
            if N_vert.shape[0] < 2 or N_vert.shape[1] < 2:
                N_vert = N_vert.transpose()
            N_vert_obs[:, ipt] = N_vert.flatten()
            spixel_intensities = spixel.get_intensity().flatten()
            spixel_intensities[spixel_intensities<0] = 0
            PixelIntensities[ipt, :] = spixel_intensities
            for ich in range(3):
                nmap[neighbor_loc[0][0], neighbor_loc[1][0], ich] = N_vert[0, ich] 

            observations[:, ipt] = spixel.get_intensity().flatten()
            for ich in range(self.N_LED):
                PixelIntensities_Imgs[neighbor_loc[0][0], neighbor_loc[1][0], ich] =\
                        spixel.get_intensity().flatten()[ich]

        self.Imgs = PixelIntensities_Imgs
        self.nmap = nmap
        self.I_mask_var = I_mask_var
        self.S = S
        self.sPS_gen = sPS
        self.varIndx_Map = varIndx_Map
        self.dmap = dMap 
        print('Done')

    def initDepth(self):
        # Initialization #
        diniter = dIniter.Initer(self.Imgs, self.I_mask_var, self.K_inv, self.S, fem_d = 1)
        diniter.set_zcandi(dmin=self.d_min, dmax=self.d_max, n_candi = 50)
        diniter.initialize_z_Idt()
        diniter.export_init_z_Idt()
        diniter.export_snorm()
        self.rho_se_init = diniter.rhose
        self.spix_depth_init_Idt = diniter.depth_init_Idt
        self.diniter = diniter

    def solve_depth(self): 
        # Refinement #
        lam_smooth_It = self.lambda_smooth_It
        max_Iter_PS = 800 
        nThreads = 12 
        if_dt_recon = False # Use It or use Idt
        # Get prepared #
        from nearLight import near_ps
        PS_nearL = near_ps.NearLight(self.Imgs, self.I_mask_var, self.S, self.K_inv)
        PS_nearL.dt_recon = False 
        PS_nearL.set_Rhos(self.rho_se_init, if_input_rhos_obsev=True)
        PS_nearL.set_lambda_smooth(lam_smooth_It)
        PS_nearL.max_iter = max_Iter_PS
        PS_nearL.set_nThreads(nThreads)
        PS_nearL.set_initial_z(self.spix_depth_init_Idt)

        PS_NEARL_It = []
        PS_NEARL_It.append(PS_nearL)
        # After each z-iteration, we also add a rho-iteration where rho_se is updated based on the current guess #
        lam_smooth_It_cur = lam_smooth_It 
        MAX_ITER = self.MAX_ITER
        for Iter in range(MAX_ITER):
            PS_NEARL_It[Iter].solve()
            print('Solving albedo...')
            PS_NEARL_It[Iter].solve_albedo()
            print('Done')
            PS_NEARL_It[Iter].export_mat(\
                    fname='tmp_res/cur_simulation_It_depth_est_LEDR_{}_iter{}.mat'.format(self.LED_R, Iter),\
                    if_dgnd = False)
            # Prepare for the next iteration #
            if Iter+1 < MAX_ITER:
                PS_nearL_ = near_ps.NearLight(self.Imgs, self.I_mask_var, self.S, self.K_inv)
                PS_NEARL_It.append(PS_nearL_)
                PS_NEARL_It[Iter+1].dt_recon = PS_NEARL_It[Iter].dt_recon
                PS_NEARL_It[Iter+1].set_Rhos(PS_NEARL_It[Iter].rho_se_cur, 
                        if_input_rhos_obsev=True)
                PS_NEARL_It[Iter+1].set_nThreads(nThreads)
                
                lam_smooth_It_cur /= 2.0
        
                PS_NEARL_It[Iter+1].set_lambda_smooth(lam_smooth_It_cur)
                PS_NEARL_It[Iter+1].set_initial_z(PS_NEARL_It[Iter].depth_cur)
                PS_NEARL_It[Iter+1].max_iter = max_Iter_PS
        print('It Optimization done!')


def COMP_NEARV():
    '''
    Compare with variational near-PS 
    Implemented in a seperate file since it needs python2
    '''

def COMP_DISTL(data_filename, d_init, if_returnVarindx = False, if_return_NMap=False):
    '''
    Compare with distant lighting PS
    '''
    import scipy.io as sio
    mat_info = sio.loadmat(data_filename)
#    ipdb.set_trace()
    IMGs = mat_info['IMGs']
    S = mat_info['S']

    if not if_return_NMap:
        # Use the light source direction for the vertex in the image center
        # with depth d_init
        ref_vert_loc = np.asarray([0, 0, d_init])
        S_distant = S - ref_vert_loc.reshape([1,3])
        Sinv_dist = np.linalg.pinv(S_distant)


        # For each pixel, perform distant light source photometric stereo #
        varIndx_Map = mat_info['varIndx_Map']
        nVars = len(np.nonzero( varIndx_Map +1)[0])
        N = np.zeros((3, nVars))
        for irow, icol in zip(np.nonzero(varIndx_Map + 1)[0],np.nonzero(varIndx_Map + 1)[1]):
            idx_var = varIndx_Map[irow, icol]
            observations_var = IMGs[irow, icol, :]
            N_pix_ = Sinv_dist.dot(observations_var)
            if observations_var.max() > 0:
                N[:, np.int(idx_var)] = N_pix_.flatten() / np.linalg.norm(N_pix_)
            else:
                N[:, np.int(idx_var)] = N_pix_.flatten() 
                

        if if_returnVarindx is True:
            return N, varIndx_Map
        else:
            return N
    else: # reuturn normal map 
        I_mask = np.sum(IMGs, axis=2) > 0 
        return COMP_DISTL_img(IMGs, S, I_mask, d_init)

def COMP_DISTL_img(IMGs, S, I_mask, d_init):  
    nr, nc = IMGs.shape[0], IMGs.shape[1]
    NMap = np.zeros((nr, nc, 3))

    ref_vert_loc = np.asarray([0, 0, d_init])
    S_distant = S - ref_vert_loc.reshape([1,3])
    Sinv_dist = np.linalg.pinv(S_distant)

    for irow, icol in zip(np.nonzero(I_mask)[0],np.nonzero(I_mask)[1]):
        observations = IMGs[irow, icol, :]
        N_pix_ = Sinv_dist.dot(observations)
        NMap[irow, icol,:] = N_pix_.flatten() / np.linalg.norm(N_pix_.flatten())

    return NMap
    

        
def simpleTest(gnd_fpath, output_fpath, LED_R_in = 1.0, N_LED_in = 24, 
        lambda_smooth = .3, if_init=True, 
        d_init = 4.0, max_iter = 10000, if_export_imgs = False):
    
    ##############
    # simple Test, Simulate the observations numerically #
    #############

    z_guess = 5.0 # the guessed z value if NO initialization is performed 

    # Camera and LED setups #
    # For the camera, the FOV is 90 degrees #
    K = np.eye(3)
    K[0,0] = 1.0/300.5
    K[1,1] = 1.0/300.5
    K[0:2,2] = -1.0
    LED_intensity = 10.0
    N_LED =  N_LED_in
    LED_R = LED_R_in #1.0 #.5 ## small light source baseline 

    # Optimization parameters #
    d_min = 2.0
    d_max = 15.0
    # if d_t reconstruction
    dt_recon = False 
    
    # Scene info, we will use the surface normal from Zmap2Nmap()
    f_path = gnd_fpath 
    mat_info = sio.loadmat(f_path)
    ipdb.set_trace()
    dMap = mat_info['depthMap'].astype(np.float64)
#   dMap = dMap.reshape(dMap.shape, order='F')
    I_mask = dMap >0
    # erode I_mask #
    import scipy.ndimage.morphology as sci_morph
    I_mask = sci_morph.binary_erosion(I_mask, iterations= 10)
    npts = np.sum(I_mask)
    dMap = I_mask.astype(np.float32) * dMap

    #Surface albedo info#
    Rho = np.ones((npts, 1))
    #Light source info#
    from simulation import Generator
    sg_near =  Generator.Generator('' , '')
    sg_near.set_LEDRing_NearL( LED_r = LED_R, LED_z = 0., nLED = N_LED, LED_int = LED_intensity);
    S = sg_near.Lpos
    nlight = S.shape[0]
    Se = np.ones( (nlight, 1)) * LED_intensity 

    from autodiff import simpleNearPS
    #Test The simple_nearPS module #
    from z2n import Zmap2Nmap as z2n
    UV_mask, IndxM_mask, IndxM_var, F_vars, UV_vars, F_local, z_nz, varIndx_Map \
            = z2n.IndM_2d_grid(I_mask, dMap, if_clockwise = False)
    KUV_mask = K.dot(UV_mask)
    I_mask_var = np.zeros(I_mask.shape)
    I_mask_var[varIndx_Map>=0] = 1

    KUV_vars = K.dot(UV_vars)
    V_vars = KUV_vars * z_nz.reshape([1, z_nz.shape[0]])
    V_vars = V_vars.transpose()

    npts_valid = IndxM_mask.shape[1]
    S_ = S.transpose()
    F_local = F_local.transpose()
    sPS = simpleNearPS.PS_Problem(S_.reshape(S_.shape, order='F'),
            KUV_mask.reshape(KUV_mask.shape, order='F'),
            Rho.flatten(),
            IndxM_mask.reshape(IndxM_mask.shape, order='F'),
            IndxM_var.reshape(IndxM_var.shape, order='F'),
            F_local.reshape(F_local.shape, order='F'),
            Se.flatten(), 1) 

    nmap = np.zeros((I_mask.shape[0], I_mask.shape[1], 3))
    # For each pixel, create aPixelReconstruct() object. Then estimate its vertice normal#  
    PixelIntensities = np.zeros((npts_valid, N_LED))
    PixelIntensities_Imgs = np.zeros((I_mask.shape[0], I_mask.shape[1], N_LED))
    observations = np.zeros((N_LED, npts_valid))

    print('--------- Getting the observations -----------')
    N_vert_obs = np.zeros((3, npts_valid))
    St = sPS.get_St().transpose()
    for ipt in range(npts_valid): # for all observations
        kuv_pix =  sPS.return_pixelKuv(ipt) # kuv in the ring-1 neighbor of the current pixel
        spixel = simpleNearPS.PixelReconstruct(kuv_pix, S_.reshape(S_.shape, order='F'),
                F_local.reshape(F_local.shape, order='F'), Se.flatten(), Rho[ipt] )
        dummy_inten_ = 0.0
        neighbor_indx = IndxM_mask[:, ipt] 
        neighbor_loc = np.unravel_index(neighbor_indx, dMap.shape, order='F')
        z_ = dMap[neighbor_loc]
        spixel.pixel_reconT(z_[0], z_[1], z_[2], z_[3], z_[4], z_[5], z_[6],  dummy_inten_)
       
        N_vert = spixel.get_N() 
        if N_vert.shape[0] < 2 or N_vert.shape[1] < 2:
            N_vert = N_vert.transpose()
        N_vert_obs[:, ipt] = N_vert.flatten()
        spixel_intensities = spixel.get_intensity().flatten()
        spixel_intensities[spixel_intensities<0] = 0
        PixelIntensities[ipt, :] = spixel_intensities
        for ich in range(3):
            nmap[neighbor_loc[0][0], neighbor_loc[1][0], ich] = N_vert[0, ich] 

        observations[:, ipt] = spixel.get_intensity().flatten()
        for ich in range(N_LED):
            PixelIntensities_Imgs[neighbor_loc[0][0], neighbor_loc[1][0], ich] =\
                    spixel.get_intensity().flatten()[ich]

    print('--------- DONE Getting the observations -----------')

    # ipdb.set_trace()


# write down the observations 
    # plt.figure()
    # for ich in range(N_LED):
    #    plt.imshow(PixelIntensities_Imgs[:,:,ich], cmap='gray', vmin=0, vmax=.2) 
    #    plt.savefig('tmp_res/ps_img_{}.png'.format(ich))
    #    plt.clf()
    # plt.close()

    ##############
    # DONE Simulate the observations numerically #
    #############

    ##############
    # Given the observations, initialize #
    ##############
    if if_init is True:
        from nearLight import initializer as dIniter 
        diniter = dIniter.Initer(PixelIntensities_Imgs, I_mask_var, K, S)
        diniter.set_zcandi(dmin=2.0, dmax=15.0, n_candi = 50)
        # diniter.initialize_depth_bs(d_max=15.0, d_min = 2.0)
        diniter.initialize_z_Idt()
#        diniter.filter_init_depth_bs()

    if if_init is False:
        z0 = z_nz.copy()
        # z0[:] = z_guess
        z0[:] = d_init
    else:
#        z0 = diniter.init_depth_bs_filtered.copy()
        # z0 = diniter.init_depth_bs.copy()
        z0 = diniter.depth_init_Idt

    sPS.set_observations(observations.reshape(observations.shape, order='F'))
    observations_dt = sPS.get_observations_dt()
    observations_t = sPS.get_observations()
    print('Done Set the observations')


    sPS.set_initial_vars(z0.flatten())
    print('Initialized the problem')

    ##############
    # Given the initialization, optimize #
    ##############
    sPS.est_pixel_dt_values_cur()
    cur_img_dt = sPS.get_cur_img_dt()
    sPS.set_lambda_smooth(lambda_smooth)
    if dt_recon is False:
        sPS.form_prob()
    else:
        sPS.form_prob_dt()

    sPS.set_maxIter(max_iter)
    print('Done Formulating the problem !') 
    sPS.solve_prob()
    print('Done solving the problem !') 
    print('') 

    ##############
    # Saving the resutls 
    ##############
    print('Writing down the current states image intensities')
    sPS.est_pixel_values_cur()
    img_int_cur = sPS.get_cur_img()
    PixelIntensities_Imgs_curGuess = np.zeros((I_mask.shape[0], I_mask.shape[1], N_LED))

    for ipt in range(npts_valid):
        for ich in range(N_LED):
            neighbor_indx = IndxM_mask[:, ipt] 
            neighbor_loc = np.unravel_index(neighbor_indx, dMap.shape, order='F')
            PixelIntensities_Imgs_curGuess[neighbor_loc[0][0],
                    neighbor_loc[1][0], ich] =\
                    img_int_cur[ich, ipt]

    # Save to .mat file #
    depth_cur = sPS.return_depths();
    mat_dic = {}
    mat_dic = {'z_gnd': z_nz, 
            'z_cur': depth_cur, 'I_mask': I_mask, 'varIndx_Map': varIndx_Map,
            'V_gnd': V_vars, 'KUV_vars': KUV_vars, 'UV_vars': UV_vars, 
            'F_vars': F_vars, 'K': K, 
            'lambda_smooth':lambda_smooth, 'LED_R': LED_R, 'N_LED':N_LED, 'S':S, 
             'IMGs': PixelIntensities_Imgs}

#    sio.savemat('cur_depth_estimation.mat', mat_dic)
    sio.savemat(output_fpath, mat_dic)
    print('Done')
    #END: test The simple_nearPS module #
