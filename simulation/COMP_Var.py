import numpy as np
import scipy.io as sio 
# import calibration.calib_lights as calib_lights
from nearLight import COMP_Var
import ipdb

# NOTE: should run with py2


def COMP_VAR(data_filename, d_init):
	mat_info = sio.loadmat(data_filename)	
	IMGs = mat_info['IMGs'].astype('<f8')
	IMGs[IMGs < 0 ] = .0000001
	S = mat_info['S'].astype('<f8')
	# I_mask = mat_info['I_mask']
	K_inv = mat_info['K']
	K = np.linalg.pinv(K_inv)
	# K[0,0] = 301
	# K[1,1] = 298
	K[0, 1] = 0.0 
	K[1:3, 0] = 0.0
	K[2, 1] = 0.0
	K[2,2]=  1.0
	# K[0, 2] = 0 
	# K[1, 2] = 0 
	S[0,-1] = .000001

#        import ipdb
#        ipdb.set_trace()

	I_mask = np.min(IMGs,axis=2)>0
	nearPSMat = COMP_Var.NearLight_Mat(IMGs.astype('<f8') * 10000.0, I_mask.astype(np.bool), S.astype('<f8'), K.astype('<f8'), init_d = d_init)
	XYZ, N, rho, mask, Phi = nearPSMat.solve()

	npt = len( np.nonzero(mask)[0])
	[nr, nc] = I_mask.shape
	N_map = np.zeros((nr, nc,3))

	indx_nonz = np.nonzero(mask)
	indx_nonzI = np.nonzero(I_mask)
	for ipt in range(npt):
		irowI, icolI = indx_nonzI[0][ipt], indx_nonzI[1][ipt]
		irow, icol = indx_nonz[0][ipt], indx_nonz[1][ipt]
		N_map[irowI, icolI, : ] =  N[irow, icol, :]


	return XYZ, N_map, mask

