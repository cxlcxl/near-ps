import subprocess
import numpy as np
import math
import os.path
import ipdb



def P2R( radii, angles):
    compX = radii * np.exp(1j * angles)
    X = np.real(compX)
    Y = np.imag(compX)
    return X, Y

def _textReplace_( inputfile, outputfile, str2find, repstr):
    '''
    Replace the strings (str2find) with (repstr) in inputfile, 
    then write to (outputfile) 
    '''
    with open(inputfile,'r') as ff:
        filedata =  ff.read()

    filedata_r= filedata.replace(str2find, repstr)

    with open(outputfile, 'w') as ff:
        ff.write(filedata_r)





class Generator:
    '''
    Vars:
        * seed_LDir_pbrtFname
            The name of the pbrt file, without file format 
            (e.g. FractalTerrain for file FractalTerrain.pbrt).
            This pbrt file serves as the 'seed' file, from which the files for the
            object under different illuminations are generated.
        * seedFldr
            The folder where the seed pbrt scene file is located
            The full path of the pbrt seed file is:
            {seedFldr}/{seed_LDir_pbrtFname}

        * Ldir: nLight x 3 matrix
            Needs to be normalized for each row.
            The lighting directions, for generating distance lighted images

        * Lpos: nLight x 3 matrix
            The light source positions, for generating near-light lighted images

    Methods:
        * Generator(self, seed_LDir_pbrtFname, seedFldr)
            inputs: seed_LDir_pbrtFname, seedFldr - strings 
                The filename and folder path for the seed pbrt file
            outputs: None
            The initializer

        * set_LEDRing_NearL(self, LED_r, nLED)

        * __set_Lpos__(self, Lpos )

        * set_Ldir(self, Ldir )
            inputs: Lpos/Ldir - nimg x 3 matrix
                The 3D light source positions for near light
                or the 3D light source direction for distant light
            outputs: None 
            Set the light source positions and directions respectively

        * gen_Images(self)
            inputs: None
            outputs: None 
                First generate the pbrt files from the seed pbrt file
                Then use pbrt to render all images into exr files.
                At last use imgtools to convert the exr files into png image files. 

        * resetFolder(self)
            inputs: None
            outputs: None 
                Delete all the files except for the seed file in the current folder
            
    '''

    # Variables #
    seed_LDir_pbrtFname = None
    seedFldr = None
    seed_LDir_pbrtPath = None
    nImg = None 
    Ldir = None 
    Lpos = None 
    LED_int = None
    plyfile= None
    obj_tz = None
    obj_tx = None
    obj_ty = None




    # Methods #
    def __init__(self,seed_LDir_pbrtFname, seedFldr, toneMap_scale=1.0, ifpbrt = True):
        self.seed_LDir_pbrtFname = seed_LDir_pbrtFname
        self.seedFldr = seedFldr
        self.seed_LDir_pbrtPath = '{}/{}'.format(
                seedFldr, 
                seed_LDir_pbrtFname)
        self.toneMap_scale = toneMap_scale
        self.ifpbrt = ifpbrt

        if ifpbrt is True:
            if os.path.isfile('{}.pbrt'.format(self.seed_LDir_pbrtPath)):
                print('Seed file: {}.pbrt'.format(self.seed_LDir_pbrtPath))
            else:
                print('Warning: seed file {}.pbrt does not exist!\n'.format(self.seed_LDir_pbrtPath) )
        else: # Mitsuba scene file 
            if os.path.isfile('{}.xml'.format(self.seed_LDir_pbrtPath)):
                print('Seed file: {}.xml'.format(self.seed_LDir_pbrtPath))
            else:
                print('Warning: seed file {}.xml does not exist!\n'.format(self.seed_LDir_pbrtPath) )
               

                
        self.nImg = 0
        self.Ldir = 0
        self.Lpos = 0
        self.obj_tz = 0
        self.obj_pos = np.zeros(3)

        self.obj_rotate_angle_degree = 0.0
        self.obj_rotate_around = "x"

        self.obj_rotate_angle_degree2 = 0.0
        self.obj_rotate_around2 = "x"

    def __set_Lpos__(self, Lpos):
        self.Lpos = Lpos
        self.obj_tz = Lpos[0,2]

    def set_obj_pos(self, obj_pos):
        # only used for mitsuba render #
        self.obj_pos = obj_pos 

    def set_obj_rotate(self, obj_rotate_around, obj_rotate_angle,
            obj_rotate_around2='x', obj_rotate_angle2=0.0):
        '''
        Only used for Mitsuba render 
        Inputs: obj_rotate_around - "x", "y" or "z"
                obj_rotate_angle - -90.0 to +90.0
        '''
        self.obj_rotate_around = obj_rotate_around
        self.obj_rotate_angle_degree = obj_rotate_angle

        self.obj_rotate_around2 = obj_rotate_around2
        self.obj_rotate_angle_degree2 = obj_rotate_angle2


    def __gen_mitsuba_nearL__(self, FileName, lpos):
        """
        For near light lighting
        Generate the scene discription file for MITSUBA renderer
        Inputs:
                FileName - dictionary: {'inputName': string , 'outputName': string}
                lpos - vector: [x,y,z]
        """
        inputName = FileName["inputName"]
        outputName = FileName["outputName"]

        # Translate  #
        _textReplace_(inputName, outputName, '<!--#obj_translate-->',
                '<translate x="{}" y="{}" z="{}"/>'.format(self.obj_pos[0],
                    self.obj_pos[1], self.obj_pos[2]) )

        # Rotate #
        _textReplace_(outputName, outputName, '<!--#obj_rotate-->',
                '<rotate {}="1" angle="{}"/>'.format(self.obj_rotate_around,
                    self.obj_rotate_angle_degree) )

        _textReplace_(outputName, outputName, '<!--#obj_rotate2-->',
                '<rotate {}="1" angle="{}"/>'.format(self.obj_rotate_around2,
                    self.obj_rotate_angle_degree2) )

        # LED position #
        _textReplace_(outputName, outputName, '<!--#LEDPos-->',
                '<point name="position" x="{}" y="{}" z="{}"/>'.format(lpos[0],
                    lpos[1],
                    lpos[2]) ) # Note that for Mistuba scene, we assume the camera is at the
                         # orginal point. ie. the object translates 

        # LED intensity #
        _textReplace_(outputName, outputName, '<!--#LEDInt-->',
                '<spectrum name="intensity" value="{}"/>'.format(self.LED_int) )

        # The object ply file #
        _textReplace_(outputName, outputName, '<!--#PlyFile-->',
                '<string name="filename" value="{}"/>'.format(self.plyfile) )
    
#        ipdb.set_trace()

        return

    def __gen_pbrt_nearL__(self, FileName, lpos):
        """
        For near light lighting
        Generate the scene discription file for PBRT renderer
        Inputs:
                FileName - dictionary: {'inputName': string , 'outputName': string}
                lpos - vector: [x,y,z]
        """
    
        inputName = FileName["inputName"]
        outputName = FileName["outputName"]
    
        file_object = open(inputName, 'r')
        file_w = open(outputName, 'w')
    
        try:
            text = file_object.read()
            w_str = 'AttributeBegin \n LightSource "point" '
            w_str = w_str + '  "point from"   [{} {} {}]    '.format(
                            lpos[0], lpos[1], lpos[2])
            w_str = w_str + ' "rgb I" [{}]'.format(
                    " ".join( (str(e) for e in np.ones(3)*(self.LED_int) )))
            w_str = w_str + '\nAttributeEnd\n\nWorldEnd'
            # writing
            file_w.write(text + w_str)
    
        finally:
            file_object.close()
            file_w.close()

        return

    def __gen_pbrt_distL__(self, FileName, ldir):
        """
        For distant light lighting
        Generate the scene discription file for PBRT renderer
        Inputs:
                FileName - dictionary: {'inputName': string , 'outputName': string}
                ldir - vector: [x,y,z]
                ldir is light from (to 0 0 0 )
        """
        inputName = FileName["inputName"]
        outputName = FileName["outputName"]
        file_object = open(inputName, 'r')
        file_w = open(outputName, 'w')
    
        try:
            text = file_object.read()
            s_str = 'AttributeBegin \n LightSource "point" '
            w_str = w_str + '  "point from"   [{} {} {}]    '.format(
                            ldir[0], ldir[1], ldir[2])
            w_str = w_str +\
                    ' "rgb I" [{}]'.format(" ".join( (str(e) for e in np.ones(3)*(self.LED_int) )))
            w_str = w_str + '\nAttributeEnd\n\nWorldEnd'
            # writing
            file_w.write(text + w_str)
        finally:
            file_object.close()
            file_w.close()
        return

    def set_Ldir(self, Ldir):
        self.Ldir = Ldir
        self.nImg = Ldir.shape[0]

    def set_ply_file(self, plyfile):
        self.plyfile = plyfile

    def set_LEDRing_NearL(self, LED_r, LED_z,  nLED , LED_int ):

        r = np.ones((nLED)) * LED_r 
        Thetas = np.zeros((nLED))
        for i in range(0,nLED):
            theta = 360.0/nLED * i / 180.0 * math.pi 
            Thetas[i] = theta

        # The position of the point light source 
        X,Y = P2R(LED_r, Thetas)
        Z = np.ones(nLED) * LED_z 

        # S is the location of the light sources
        # The coordinate center is the camera projction center #
        S = np.stack([X,Y,Z], axis=1) 
        self.__set_Lpos__(S)
        self.nImg = S.shape[0]

        # LED intensity 
        self.LED_int = LED_int


    def resetFolder(self):
        from os import listdir 
        from os import remove 
        import os.path
        flist = listdir(self.seedFldr)
        
        #for filename in flist:
        if filename == '{}.pbrt'.format(self.seed_LDir_pbrtFname):
            pass # Do not remove the seed scene file
        else:
            filepath = '{}/{}'.format(self.seedFldr,\
                    filename)
            if os.path.isfile(filepath):
                remove(filepath)


    def gen_Images(self, nearLight=False):
        nLED = self.nImg
        toneMap_scale = self.toneMap_scale
        for i in range(0, nLED):
            print('\n{}/{} point light'.format(i+1, nLED ))

            # input and output files for generating pbrt files #
            fname = {}
            if self.ifpbrt is True:
                fname['inputName'] = '{}.pbrt'.format(self.seed_LDir_pbrtPath)
                fname['outputName']  = '{}_{}.pbrt'.format(
                       self.seed_LDir_pbrtPath, i+1)
            else:
                fname['inputName'] = '{}.xml'.format(self.seed_LDir_pbrtPath)
                fname['outputName']  = '{}_{}.xml'.format(
                       self.seed_LDir_pbrtPath, i+1)

            print(fname['outputName'])

            # Get the pbrt scene file and render, then convert the rendered image
            # to png format
            if nearLight:
                if self.Lpos is None:
                    print('Error: Lpos is not defined\n')

                l_input =  self.Lpos[i,:]

                if self.ifpbrt is True:
                    self.__gen_pbrt_nearL__(fname, l_input)
                else:
                    self.__gen_mitsuba_nearL__(fname, l_input)
            else:
                if self.Ldir is None:
                    print('Error: Ldir is not defined\n')
                if self.ifpbrt is False:
                    print('Error: distant lighting for MITSUBA scene generation is not implemented\n')
                l_input =  self.Ldir[i,:]
                self.__gen_pbrt_distL__(fname, l_input)

            if self.ifpbrt is True:
                cmd = 'pbrt {} --quiet --outfile  {}.exr'.format(fname['outputName'],\
                        fname['outputName'][:-5:])
                cmd2 = 'imgtool convert --tonemap --scale {} {}.exr {}.png'.format(
                        toneMap_scale, fname['outputName'][:-5], fname['outputName'][:-5])
            else: # render using Mitsuba
                cmd = 'mitsuba {} -q -o {}.exr'.format(fname['outputName'],
                        fname['outputName'][:-4:])

            subprocess.call( cmd, shell=True)
            if self.ifpbrt is True:
                print(cmd2)
                subprocess.call( cmd2, shell=True)

    def gen_Images_checker_board(self, img_indx, obj_pos,rot_axis='x', rot_angle='0.0',
            rot_axis2 = 'y', rot_angle2 = '0.0'):

            # input and output files for generating pbrt files #
            fname = {}
            if self.ifpbrt is True:
                print('PBRT for checkerboard is not supported !')
                return
            else:
                fname['inputName'] = '{}.xml'.format(self.seed_LDir_pbrtPath)
                fname['outputName']  = '{}_{}.xml'.format(
                       self.seed_LDir_pbrtPath, img_indx)

            print(fname['outputName'])

            l_input = np.zeros(3)
            self.set_obj_pos(obj_pos)
            self.set_obj_rotate(obj_rotate_around = rot_axis, obj_rotate_angle = rot_angle, 
                    obj_rotate_around2 = rot_axis2, obj_rotate_angle2 = rot_angle2)
            self.__gen_mitsuba_nearL__(fname, l_input)

            cmd = 'mitsuba {} -q -o {}.exr'.format(fname['outputName'], fname['outputName'][:-4:])
            subprocess.call( cmd, shell=True)

'''
 Example to use the calss  Generator()
 first make sure there is a seed file (e.g. FractalTerrain.pbrt) in
 the seed folder  (e.g./home/chaoliu1/PS_BSSRDF/data/smallring_t)
if __name__=='__main__':

    # assuming using PBRt to get the images #
    sg = Generator('FractalTerrain',
            '/home/chaoliu1/PS_BSSRDF/data/smallring_t')
    sg.resetFolder()
    sg.set_LEDRing_NearL(LED_r=10, LED_z=8, nLED=20, LED_int = 100.0)
    sg.gen_Images(nearLight=True)

    # Assumimg using Mitsuba to get the images:
    # For generating images for virtual camera calibration:
    sg = Generator('checkerboard', $path_to_checkerboard_xml_foldr, ifpbrt=False)

    # Translate: [0, 0, -10]; rotation first around y in -20 degreee, then rotate around y in 30 degree
    sg.gen_Images_checker_board(img_indx, [0, 0, -10], rot_axis='y', rot_angle = -20, rot_axis2='x', rot_angle = 30)
'''
