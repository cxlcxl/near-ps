#!/bin/bash

echo "Building auto_diff_NearPS Module ... " 
clang++  -O3 -fPIC -fopenmp -Wall -shared -std=c++11 simpleNearPS.cc -o simpleNearPS`python3-config --extension-suffix`  `python3-config --includes`  -I/usr/local/include/eigen3 -I/home/chaoliu1/tools/libigl/libigl/include -I/usr/local/include -L/home/chaoliu1/tools/ceres-solver/ceres-solver/build/lib -lceres -lgtest -ltest_util -lglog -lgflags -lcholmod -lcxsparse -lSegFault -lgomp 
#clang++ -O3   -Wall -shared -std=c++11 -fPIC simpleNearPS.cc -o simpleNearPS`python3-config --extension-suffix`  `python3-config --includes` -I/usr/local/include/eigen3 -I/home/chaoliu1/tools/libigl/libigl/include -I/usr/local/include -L/home/chaoliu1/tools/ceres-solver/ceres-solver/build/lib -lceres -lgtest -ltest_util -lglog -lgflags -lcholmod -lcxsparse 
echo "Done ! "


