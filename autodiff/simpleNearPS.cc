#include "simpleNearPS.h"


#define IF_IGN_TERM 0 

 /* Helpers functions */
template <typename T>
T abs_tin(T T_in){
	return (T_in < (T)0? T_in * (T)(-1.0) : T_in);
}

template <typename T>
void normalize_vector(T* vector_in){
// assuming the vectror is of dim 3
	T normal_ = (T) 0.0;
	for(int i =0; i<3; i++){
		normal_ += vector_in[i] * vector_in[i];
	}	
	normal_ = sqrt(normal_);

	if(normal_ != T(0)){
		for(int i =0; i<3; i++){
			vector_in[i] /= normal_;
		}	
	}
}

template <typename T>
void normalize_vector(T* vector_in, T& norm){
// assuming the vectror is of dim 3
	T normal_ = (T) 0.0;
	for(int i =0; i<3; i++){
		normal_ += vector_in[i] * vector_in[i];
	}	
	normal_ = sqrt(normal_);

	if(normal_ != T(0)){
		for(int i =0; i<3; i++){
			vector_in[i] /= normal_;
		}	
	}
	norm = normal_;
}

template <typename T>
void vector_norm(T* vector_in, T& norm){
// assuming the vectror is of dim 3
	T normal_ = (T) 0.0;
	for(int i =0; i<3; i++){
		normal_ += vector_in[i] * vector_in[i];
	}	
	normal_ = sqrt(normal_);
	norm = normal_;
}

template <typename T>
void vector_sub(const T* const a, const T* const b, T* c){
	for(int i=0; i<3; i++){
		c[i] = a[i] - b[i];
	}
}

template <typename T>
void vector_dot(const T* const a, const T* const b, T& c){
	c = (T) 0.0;
	for(int i=0; i<3; i++){
		c += a[i]*b[i];
	}
}

template <typename T>
void vector_diff(const T* const a, const T* const b, T& c){
		// Calculate the angle between two vectors //
	T dot_ ;
	vector_dot(a, b, dot_);
//	dot_ = (dot_ > 0.) ? dot_ : -dot; 
	c = (T)1.0 - dot_;
}

 /* --- end of Helpers functions ----*/
struct pixel_dt_recon_err{
	pixel_dt_recon_err(
					VectorXd observations_in, // observed I_t
					VectorXd I_meas_in,
					MatrixXd S_in,
					MatrixXd S_t_in,
					MatrixXd S_h_in, 
					VectorXd Se_in,
					MatrixXd kuv_in,
					double rho_in,
					double lambda_smooth_in,
					double pow_para_in){
				observations = observations_in;
				I_meas = I_meas_in;
				S = S_in;
				S_t = S_t_in; 
				S_h = S_h_in;
				Se = Se_in;
				kuv = kuv_in;
				rho = rho_in;
				nlight = S_t.cols();	
				lambda_smooth = lambda_smooth_in;
				pow_para = pow_para_in;
	}

	template <typename T_in>
	bool operator()(
					const T_in* const z0,
					const T_in* const z1,
					const T_in* const z2,  
					const T_in* const z3,  
					const T_in* const z4,  
					const T_in* const z5,  
					const T_in* const z6,
				  	T_in* residuals) const {
	
	// Given z0 ~ z6 and kuvs, get the 3D locations for the vertices //
	T_in p0[3] = {(T_in)kuv(0, 0) * z0[0], (T_in)kuv(1,0) *z0[0] , (T_in)kuv(2,0) *z0[0]};
	T_in p1[3] = {(T_in)kuv(0, 1) * z1[0], (T_in)kuv(1,1) *z1[0] , (T_in)kuv(2,1) *z1[0]};
    T_in p2[3] = {(T_in)kuv(0, 2) * z2[0], (T_in)kuv(1,2) *z2[0] , (T_in)kuv(2,2) *z2[0]};
	T_in p3[3] = {(T_in)kuv(0, 3) * z3[0], (T_in)kuv(1,3) *z3[0] , (T_in)kuv(2,3) *z3[0]};
	T_in p4[3] = {(T_in)kuv(0, 4) * z4[0], (T_in)kuv(1,4) *z4[0] , (T_in)kuv(2,4) *z4[0]};
	T_in p5[3] = {(T_in)kuv(0, 5) * z5[0], (T_in)kuv(1,5) *z5[0] , (T_in)kuv(2,5) *z5[0]};
	T_in p6[3] = {(T_in)kuv(0, 6) * z6[0], (T_in)kuv(1,6) *z6[0] , (T_in)kuv(2,6) *z6[0]};

	// get the vectors //
	T_in v1[3] = { p1[0]-p0[0], p1[1]-p0[1], p1[2]-p0[2]}; 
	T_in v2[3] = { p2[0]-p0[0], p2[1]-p0[1], p2[2]-p0[2]}; 
	T_in v3[3] = { p3[0]-p0[0], p3[1]-p0[1], p3[2]-p0[2]};
	T_in v4[3] = { p4[0]-p0[0], p4[1]-p0[1], p4[2]-p0[2]};
	T_in v5[3] = { p5[0]-p0[0], p5[1]-p0[1], p5[2]-p0[2]};
	T_in v6[3] = { p6[0]-p0[0], p6[1]-p0[1], p6[2]-p0[2]};
	
	// First, get the N_faces by cross product //
	T_in N_face12[3] = { v1[1]*v2[2]-v1[2]*v2[1], v1[2]*v2[0]-v1[0]*v2[2], v1[0]*v2[1]-v1[1]*v2[0] }; 
	T_in N_face23[3] = { v2[1]*v3[2]-v2[2]*v3[1], v2[2]*v3[0]-v2[0]*v3[2], v2[0]*v3[1]-v2[1]*v3[0] }; 
	T_in N_face34[3] = { v3[1]*v4[2]-v3[2]*v4[1], v3[2]*v4[0]-v3[0]*v4[2], v3[0]*v4[1]-v3[1]*v4[0] }; 
	T_in N_face45[3] = { v4[1]*v5[2]-v4[2]*v5[1], v4[2]*v5[0]-v4[0]*v5[2], v4[0]*v5[1]-v4[1]*v5[0] }; 
	T_in N_face56[3] = { v5[1]*v6[2]-v5[2]*v6[1], v5[2]*v6[0]-v5[0]*v6[2], v5[0]*v6[1]-v5[1]*v6[0] }; 
	T_in N_face61[3] = { v6[1]*v1[2]-v6[2]*v1[1], v6[2]*v1[0]-v6[0]*v1[2], v6[0]*v1[1]-v6[1]*v1[0] }; 

	// We will also use the variance of the surface normal around a vetrice //
	T_in diff_N_face12 = (T_in) 0.0 ; 
	T_in diff_N_face23 = (T_in) 0.0 ; 
	T_in diff_N_face34 = (T_in) 0.0 ; 
	T_in diff_N_face45 = (T_in) 0.0 ; 
	T_in diff_N_face56 = (T_in) 0.0 ; 
	T_in diff_N_face61 = (T_in) 0.0 ; 

	// normalize //
	if(1- IF_IGN_TERM){
		normalize_vector(N_face12);
		normalize_vector(N_face23);
		normalize_vector(N_face34);
		normalize_vector(N_face45);
		normalize_vector(N_face56);
		normalize_vector(N_face61);
	}
	
	// Then, get N_vertices from N_faces //	
	T_in N_vert[3] = { 
		N_face12[0] + N_face23[0] + N_face34[0]+ N_face45[0]+ N_face56[0] + N_face61[0], 
		N_face12[1] + N_face23[1] + N_face34[1]+ N_face45[1]+ N_face56[1] + N_face61[1], 
		N_face12[2] + N_face23[2] + N_face34[2]+ N_face45[2]+ N_face56[2] + N_face61[2] 
	};
	
	if(1- IF_IGN_TERM){ // include the second term 
		normalize_vector(N_vert);
		// Get the variance of the surface normal around a vertice //
		vector_diff(N_vert, N_face12, diff_N_face12);
		vector_diff(N_vert, N_face23, diff_N_face23);
		vector_diff(N_vert, N_face34, diff_N_face34);
		vector_diff(N_vert, N_face45, diff_N_face45);
		vector_diff(N_vert, N_face56, diff_N_face56);
		vector_diff(N_vert, N_face61, diff_N_face61);
	}

	// Then, get the pixel dt value assumeing lambertian surface //
	T_in sp_dist, stp, spn;
	T_in stn_f12, stn_f23, stn_f34, stn_f45, stn_f56, stn_f61;
	T_in spn_f12, spn_f23, spn_f34, spn_f45, spn_f56, spn_f61;
	T_in sphn_f12, sphn_f23, sphn_f34, sphn_f45, sphn_f56, sphn_f61;

	for(int ilight = 0; ilight < nlight; ilight++){
		T_in s[3] = { (T_in)S(0, ilight), (T_in)S(1, ilight), (T_in)S(2, ilight) };	
		T_in s_t[3] = { (T_in)S_t(0, ilight), (T_in)S_t(1, ilight), (T_in)S_t(2, ilight)};
		T_in sp[3];

		vector_sub(s, p0, sp);	
		vector_dot(s_t, p0, stp);
		vector_dot(sp, N_vert, spn);

		vector_dot(s_t, N_face12, stn_f12);
		vector_dot(s_t, N_face23, stn_f23);
		vector_dot(s_t, N_face34, stn_f34);
		vector_dot(s_t, N_face45, stn_f45);
		vector_dot(s_t, N_face56, stn_f56);
		vector_dot(s_t, N_face61, stn_f61);

//		stn_f12 = std::max( (T_in)0.0, stn_f12);
//		stn_f23 = std::max( (T_in)0.0, stn_f23);
//		stn_f34 = std::max( (T_in)0.0, stn_f34);
//		stn_f45 = std::max( (T_in)0.0, stn_f45);
//		stn_f56 = std::max( (T_in)0.0, stn_f56);
//		stn_f61 = std::max( (T_in)0.0, stn_f61);
//		normalize_vector(sp, sp_dist);
		
		// Get spn //
		vector_sub(s, p0, sp);	
		vector_dot(sp, N_face12, spn_f12);
		vector_dot(sp, N_face23, spn_f23);
		vector_dot(sp, N_face34, spn_f34);
		vector_dot(sp, N_face45, spn_f45);
		vector_dot(sp, N_face56, spn_f56);
		vector_dot(sp, N_face61, spn_f61);

		spn_f12 = std::max( (T_in)0.0, spn_f12);
		spn_f23 = std::max( (T_in)0.0, spn_f23);
		spn_f34 = std::max( (T_in)0.0, spn_f34);
		spn_f45 = std::max( (T_in)0.0, spn_f45);
		spn_f56 = std::max( (T_in)0.0, spn_f56);
		spn_f61 = std::max( (T_in)0.0, spn_f61);

		normalize_vector(sp, sp_dist);
		T_in spn_ = spn_f12 ;
		spn_ += spn_f23 ;
		spn_ += spn_f34 ;
		spn_ += spn_f45 ;
		spn_ += spn_f56 ;
		spn_ += spn_f61 ;
		spn_  = spn_ / 6.0;

		if( spn < 0.0 || I_meas(ilight) == 0.0){ 
			//deal with self-shadow: we will ignore the pixel dt values @ self-shadow //
			residuals[ilight] = (T_in) 0.0;
		}
		else{
			T_in stn_  = stn_f12 ;
			stn_ 	   += stn_f23 ;
			stn_ 	   += stn_f34 ;
			stn_ 	   += stn_f45 ;
			stn_ 	   += stn_f56 ;
			stn_ 	   += stn_f61 ;
			stn_ 		= stn_ / 6.0;

			// what if we ignore the second term ... //
			if(IF_IGN_TERM){
			residuals[ilight] = ((T_in) I_meas(ilight) * (stn_ ) - (T_in)observations[ilight] * spn_) * 100.;
			}
			else{
			// what if we change the exponential parameter ... //
			residuals[ilight] = (T_in) I_meas(ilight) * (stn_ + (T_in)3.0 * stp
								*spn_ / (T_in)pow(sp_dist, pow_para)) -(T_in) observations[ilight] * spn_; 
				}
		 	}
		}

	// The smoothness constraint //
	T_in z_mean 		= (z0[0] + z1[0]+ z2[0]+ z3[0]+ z4[0]+ z5[0]+ z6[0]) / 7.0;
	residuals[nlight]   = (z0[0] - z_mean) *  lambda_smooth;
	residuals[nlight+1] = (z1[0] - z_mean) *  lambda_smooth;
	residuals[nlight+2] = (z2[0] - z_mean) *  lambda_smooth;
	residuals[nlight+3] = (z3[0] - z_mean) *  lambda_smooth;
	residuals[nlight+4] = (z4[0] - z_mean) *  lambda_smooth;
	residuals[nlight+5] = (z5[0] - z_mean) *  lambda_smooth;
	residuals[nlight+6] = (z6[0] - z_mean) *  lambda_smooth;
	if(1-IF_IGN_TERM){ // not ignore the second term
		residuals[nlight]   += diff_N_face12 * lambda_smooth * (T_in) 50.0;
		residuals[nlight+2] += diff_N_face34 * lambda_smooth * (T_in) 50.0;
		residuals[nlight+3] += diff_N_face45 * lambda_smooth * (T_in) 50.0;
		residuals[nlight+1] += diff_N_face23 * lambda_smooth * (T_in) 50.0;
		residuals[nlight+4] += diff_N_face56 * lambda_smooth * (T_in) 50.0;
		residuals[nlight+5] += diff_N_face61 * lambda_smooth * (T_in) 50.0;
	}
	else{ // ignore the second term 
		residuals[nlight]   += (T_in) 0.;
		residuals[nlight+2] += (T_in) 0.;
		residuals[nlight+3] += (T_in) 0.;
		residuals[nlight+1] += (T_in) 0.;
		residuals[nlight+4] += (T_in) 0.;
		residuals[nlight+5] += (T_in) 0.;
	}
	return true;

	} // END OF est_pixel_dt_values_cur_


	static ceres::CostFunction* Create(
					const VectorXd observations, // I_t measured
					const VectorXd I_meas, 
					const MatrixXd S,
					const MatrixXd S_t,
					const MatrixXd S_h,
					const VectorXd Se,
					const MatrixXd kuv,
					const MatrixXi F,
					const double rho,
					const int nlight_in, 
					const double lambda_smooth, 
					const double pow_para){
		return (new ceres::AutoDiffCostFunction<pixel_dt_recon_err, ceres::DYNAMIC,
								1, 1, 1, 1, 1, 1, 1>(new pixel_dt_recon_err(
												observations, I_meas, S, S_t, S_h, Se, kuv, rho,
												lambda_smooth, pow_para	
												), 
										nlight_in + 7 )); 
		// dimension of residual is # of lights + 7 (smoothness)
	}


	template <typename T_in>
	bool est_pixel_dt_values_cur_(
					const T_in* const z0,
					const T_in* const z1,
					const T_in* const z2,  
					const T_in* const z3,  
					const T_in* const z4,  
					const T_in* const z5,  
					const T_in* const z6,
				  	T_in* residuals) const {
	
	// Given z0 ~ z6 and kuvs, get the 3D locations for the vertices //
	T_in p0[3] = {(T_in)kuv(0, 0) * z0[0], (T_in)kuv(1,0) *z0[0] , (T_in)kuv(2,0) *z0[0]};
	T_in p1[3] = {(T_in)kuv(0, 1) * z1[0], (T_in)kuv(1,1) *z1[0] , (T_in)kuv(2,1) *z1[0]};
    T_in p2[3] = {(T_in)kuv(0, 2) * z2[0], (T_in)kuv(1,2) *z2[0] , (T_in)kuv(2,2) *z2[0]};
	T_in p3[3] = {(T_in)kuv(0, 3) * z3[0], (T_in)kuv(1,3) *z3[0] , (T_in)kuv(2,3) *z3[0]};
	T_in p4[3] = {(T_in)kuv(0, 4) * z4[0], (T_in)kuv(1,4) *z4[0] , (T_in)kuv(2,4) *z4[0]};
	T_in p5[3] = {(T_in)kuv(0, 5) * z5[0], (T_in)kuv(1,5) *z5[0] , (T_in)kuv(2,5) *z5[0]};
	T_in p6[3] = {(T_in)kuv(0, 6) * z6[0], (T_in)kuv(1,6) *z6[0] , (T_in)kuv(2,6) *z6[0]};

	// get the vectors //
	T_in v1[3] = { p1[0]-p0[0], p1[1]-p0[1], p1[2]-p0[2]}; 
	T_in v2[3] = { p2[0]-p0[0], p2[1]-p0[1], p2[2]-p0[2]}; 
	T_in v3[3] = { p3[0]-p0[0], p3[1]-p0[1], p3[2]-p0[2]};
	T_in v4[3] = { p4[0]-p0[0], p4[1]-p0[1], p4[2]-p0[2]};
	T_in v5[3] = { p5[0]-p0[0], p5[1]-p0[1], p5[2]-p0[2]};
	T_in v6[3] = { p6[0]-p0[0], p6[1]-p0[1], p6[2]-p0[2]};
	
	// First, get the N_faces by cross product //
	T_in N_face12[3] = {v1[1]*v2[2]-v1[2]*v2[1], v1[2]*v2[0]-v1[0]*v2[2], v1[0]*v2[1]-v1[1]*v2[0] }; 
	T_in N_face23[3] = {v2[1]*v3[2]-v2[2]*v3[1], v2[2]*v3[0]-v2[0]*v3[2], v2[0]*v3[1]-v2[1]*v3[0] }; 
	T_in N_face34[3] = {v3[1]*v4[2]-v3[2]*v4[1], v3[2]*v4[0]-v3[0]*v4[2], v3[0]*v4[1]-v3[1]*v4[0] }; 
	T_in N_face45[3] = {v4[1]*v5[2]-v4[2]*v5[1], v4[2]*v5[0]-v4[0]*v5[2], v4[0]*v5[1]-v4[1]*v5[0] }; 
	T_in N_face56[3] = {v5[1]*v6[2]-v5[2]*v6[1], v5[2]*v6[0]-v5[0]*v6[2], v5[0]*v6[1]-v5[1]*v6[0] }; 
	T_in N_face61[3] = {v6[1]*v1[2]-v6[2]*v1[1], v6[2]*v1[0]-v6[0]*v1[2], v6[0]*v1[1]-v6[1]*v1[0] }; 

	// normalize //
	normalize_vector(N_face12);
	normalize_vector(N_face23);
	normalize_vector(N_face34);
	normalize_vector(N_face45);
	normalize_vector(N_face56);
	normalize_vector(N_face61);
	
	// Then, get N_vertices from N_faces //	
	// Uniform weights TODO: use area weight //
	T_in N_vert[3] = { 
		N_face12[0] + N_face23[0] + N_face34[0]+ N_face45[0]+ N_face56[0] + N_face61[0], 
		N_face12[1] + N_face23[1] + N_face34[1]+ N_face45[1]+ N_face56[1] + N_face61[1], 
		N_face12[2] + N_face23[2] + N_face34[2]+ N_face45[2]+ N_face56[2] + N_face61[2] 
	};
	normalize_vector(N_vert);

	// Then, get the pixel dt value assumeing lambertian surface //
	T_in spn, sp_dist, stp, stn;
	T_in sphn_f12, sphn_f23, sphn_f34, sphn_f45, sphn_f56, sphn_f61;
	for(int ilight = 0; ilight < nlight; ilight++){
		T_in s[3] = { (T_in)S(0, ilight), (T_in)S(1, ilight), (T_in)S(2, ilight) };	
		T_in s_t[3] = { (T_in)S_t(0, ilight), (T_in)S_t(1, ilight), (T_in)S_t(2, ilight)};
		T_in sp[3];
		vector_sub(s, p0, sp);	
		vector_dot(sp, N_vert, spn);
		normalize_vector(sp, sp_dist);
		vector_dot(s_t, p0, stp);
		vector_dot(s_t,N_vert, stn);

		if( spn < 0.0){ 
			//deal with self-shadow: we will ignore the pixel dt values @ self-shadow //
			residuals[ilight] = (T_in) 0.0;
		}
		else{
			// residuals here saves the value of the change of pixel intensities //
			T_in s_h[3] = {(T_in)S_h(0, ilight), (T_in)S_h(1, ilight), (T_in)S_h(2, ilight)};
			T_in shp[3];
			vector_sub(s_h, p0, shp);	
			T_in shp_dist; 
			normalize_vector(shp, shp_dist);
			vector_dot(shp, N_face12, sphn_f12);
			vector_dot(shp, N_face23, sphn_f23);
			vector_dot(shp, N_face34, sphn_f34);
			vector_dot(shp, N_face45, sphn_f45);
			vector_dot(shp, N_face56, sphn_f56);
			vector_dot(shp, N_face61, sphn_f61);
			T_in sphn_ = sphn_f12 ;
			sphn_ += sphn_f23 ;
			sphn_ += sphn_f34 ;
			sphn_ += sphn_f45 ;
			sphn_ += sphn_f56 ;
			sphn_ += sphn_f61 ;
			sphn_ = sphn_ / 6.0;
			T_in scalar_I_meas_c = (T_in)pow(sp_dist, 3.0) / (T_in)pow(shp_dist , 3.0) * sphn_ / spn; 
			residuals[ilight] = (T_in) I_meas(ilight) * (stn / (spn) + (T_in)3.0 * stp / (T_in)pow(sp_dist, 2.0));// * scalar_I_meas_c;
			}

		}
	return true;
	
	} // END OF est_pixel_dt_values_cur_

 	/* Variables */
	VectorXd observations; // I_t
	VectorXd I_meas;
	// each col is an observation, vertice or face 
	MatrixXd S_t; // the changes of light source directions  
	MatrixXd S_h; 
	MatrixXd S;
	VectorXd Se;
	MatrixXd kuv; 
	int nlight;
	double rho;
	double lambda_smooth = 0.0;
	double pow_para = 2.0;
};

struct pixel_recon_err{

	pixel_recon_err(VectorXd observations_in,
					MatrixXd S_in,
					VectorXd Se_in,
					MatrixXd kuv_in,
					MatrixXi F_in,
					double rho_in,
					double lambda_smooth_in){
//				std::cout << "created pixel_recon_err structure" << std::endl;
				observations = observations_in;
				S = S_in; 
				Se = Se_in;
				kuv = kuv_in;
				F = F_in;
				rho = rho_in;
				nlight = S.cols();	
				lambda_smooth = lambda_smooth_in;
	}
	




	template <typename T_in>
	bool est_pixel_values_cur_(const T_in* const z0,
					const T_in* const z1,
					const T_in* const z2,  
					const T_in* const z3,  
					const T_in* const z4,  
					const T_in* const z5,  
					const T_in* const z6,
				  	T_in* residuals) const {

	// Given z0 ~ z6 and kuvs, get the 3D locations for the vertices //
	T_in p0[3] = {(T_in)kuv(0, 0) * z0[0], (T_in)kuv(1,0) *z0[0] , (T_in)kuv(2,0) *z0[0]};
	T_in p1[3] = {(T_in)kuv(0, 1) * z1[0], (T_in)kuv(1,1) *z1[0] , (T_in)kuv(2,1) *z1[0]};
    T_in p2[3] = {(T_in)kuv(0, 2) * z2[0], (T_in)kuv(1,2) *z2[0] , (T_in)kuv(2,2) *z2[0]};
	T_in p3[3] = {(T_in)kuv(0, 3) * z3[0], (T_in)kuv(1,3) *z3[0] , (T_in)kuv(2,3) *z3[0]};
	T_in p4[3] = {(T_in)kuv(0, 4) * z4[0], (T_in)kuv(1,4) *z4[0] , (T_in)kuv(2,4) *z4[0]};
	T_in p5[3] = {(T_in)kuv(0, 5) * z5[0], (T_in)kuv(1,5) *z5[0] , (T_in)kuv(2,5) *z5[0]};
	T_in p6[3] = {(T_in)kuv(0, 6) * z6[0], (T_in)kuv(1,6) *z6[0] , (T_in)kuv(2,6) *z6[0]};

	// get the vectors //
	T_in v1[3] = { p1[0]-p0[0], p1[1]-p0[1], p1[2]-p0[2]}; 
	T_in v2[3] = { p2[0]-p0[0], p2[1]-p0[1], p2[2]-p0[2]}; 
	T_in v3[3] = { p3[0]-p0[0], p3[1]-p0[1], p3[2]-p0[2]};
	T_in v4[3] = { p4[0]-p0[0], p4[1]-p0[1], p4[2]-p0[2]};
	T_in v5[3] = { p5[0]-p0[0], p5[1]-p0[1], p5[2]-p0[2]};
	T_in v6[3] = { p6[0]-p0[0], p6[1]-p0[1], p6[2]-p0[2]};
	
	// First, get the N_faces by cross product //
	T_in N_face12[3] = {v1[1]*v2[2]-v1[2]*v2[1], v1[2]*v2[0]-v1[0]*v2[2], v1[0]*v2[1]-v1[1]*v2[0] }; 
	T_in N_face23[3] = {v2[1]*v3[2]-v2[2]*v3[1], v2[2]*v3[0]-v2[0]*v3[2], v2[0]*v3[1]-v2[1]*v3[0] }; 
	T_in N_face34[3] = {v3[1]*v4[2]-v3[2]*v4[1], v3[2]*v4[0]-v3[0]*v4[2], v3[0]*v4[1]-v3[1]*v4[0] }; 
	T_in N_face45[3] = {v4[1]*v5[2]-v4[2]*v5[1], v4[2]*v5[0]-v4[0]*v5[2], v4[0]*v5[1]-v4[1]*v5[0] }; 
	T_in N_face56[3] = {v5[1]*v6[2]-v5[2]*v6[1], v5[2]*v6[0]-v5[0]*v6[2], v5[0]*v6[1]-v5[1]*v6[0] }; 
	T_in N_face61[3] = {v6[1]*v1[2]-v6[2]*v1[1], v6[2]*v1[0]-v6[0]*v1[2], v6[0]*v1[1]-v6[1]*v1[0] }; 

	// normalize //
	normalize_vector(N_face12);
	normalize_vector(N_face23);
	normalize_vector(N_face34);
	normalize_vector(N_face45);
	normalize_vector(N_face56);
	normalize_vector(N_face61);
	
	// Then, get N_vertices from N_faces //	
	// Uniform weights TODO: use area weight //
	T_in N_vert[3] = { 
		N_face12[0] + N_face23[0] + N_face34[0]+ N_face45[0]+ N_face56[0] + N_face61[0], 
		N_face12[1] + N_face23[1] + N_face34[1]+ N_face45[1]+ N_face56[1] + N_face61[1], 
		N_face12[2] + N_face23[2] + N_face34[2]+ N_face45[2]+ N_face56[2] + N_face61[2] 
	};
	normalize_vector(N_vert);
	

	// Then, get the pixel value assumeing lambertian surface //

	// Assuming Lambertian surface 
	T_in spn, sp_dist;
	for(int ilight = 0; ilight < nlight; ilight++){
		T_in s[3] = { (T_in)S(0, ilight), (T_in)S(1, ilight), (T_in)S(2, ilight) };	
		T_in sp[3];
		vector_sub(s, p0, sp);	
		vector_dot(sp, N_vert, spn);
		normalize_vector(sp, sp_dist);
		T_in denom_ = (T_in) pow(sp_dist,3.0); 
		T_in nome_ = spn * (T_in)Se(ilight) * (T_in)rho;

		// self-shadow //
		if( nome_ <  0.0){ 
			nome_ = (T_in) 0.0;
		}

		// residuals here saves the value of the pixel intensities //
		residuals[ilight] = nome_ / denom_ ;

		}

	return true;
	}		
	
	

	// DONE : validate the forward model by getting the surface normal image //
	template <typename T_in>
	bool operator()(const T_in* const z0,
					const T_in* const z1,
					const T_in* const z2,  
					const T_in* const z3,  
					const T_in* const z4,  
					const T_in* const z5,  
					const T_in* const z6,
				  	T_in* residuals) const {

	// Given z0 ~ z6 and kuvs, get the 3D locations for the vertices //
	T_in p0[3] = {(T_in)kuv(0, 0) * z0[0], (T_in)kuv(1,0) *z0[0] , (T_in)kuv(2,0) *z0[0]};
	T_in p1[3] = {(T_in)kuv(0, 1) * z1[0], (T_in)kuv(1,1) *z1[0] , (T_in)kuv(2,1) *z1[0]};
    T_in p2[3] = {(T_in)kuv(0, 2) * z2[0], (T_in)kuv(1,2) *z2[0] , (T_in)kuv(2,2) *z2[0]};
	T_in p3[3] = {(T_in)kuv(0, 3) * z3[0], (T_in)kuv(1,3) *z3[0] , (T_in)kuv(2,3) *z3[0]};
	T_in p4[3] = {(T_in)kuv(0, 4) * z4[0], (T_in)kuv(1,4) *z4[0] , (T_in)kuv(2,4) *z4[0]};
	T_in p5[3] = {(T_in)kuv(0, 5) * z5[0], (T_in)kuv(1,5) *z5[0] , (T_in)kuv(2,5) *z5[0]};
	T_in p6[3] = {(T_in)kuv(0, 6) * z6[0], (T_in)kuv(1,6) *z6[0] , (T_in)kuv(2,6) *z6[0]};

	// get the vectors //
	T_in v1[3] = { p1[0]-p0[0], p1[1]-p0[1], p1[2]-p0[2]}; 
	T_in v2[3] = { p2[0]-p0[0], p2[1]-p0[1], p2[2]-p0[2]}; 
	T_in v3[3] = { p3[0]-p0[0], p3[1]-p0[1], p3[2]-p0[2]};
	T_in v4[3] = { p4[0]-p0[0], p4[1]-p0[1], p4[2]-p0[2]};
	T_in v5[3] = { p5[0]-p0[0], p5[1]-p0[1], p5[2]-p0[2]};
	T_in v6[3] = { p6[0]-p0[0], p6[1]-p0[1], p6[2]-p0[2]};
	
	// First, get the N_faces by cross product //
	T_in N_face12[3] = {v1[1]*v2[2]-v1[2]*v2[1], v1[2]*v2[0]-v1[0]*v2[2], v1[0]*v2[1]-v1[1]*v2[0] }; 
	T_in N_face23[3] = {v2[1]*v3[2]-v2[2]*v3[1], v2[2]*v3[0]-v2[0]*v3[2], v2[0]*v3[1]-v2[1]*v3[0] }; 
	T_in N_face34[3] = {v3[1]*v4[2]-v3[2]*v4[1], v3[2]*v4[0]-v3[0]*v4[2], v3[0]*v4[1]-v3[1]*v4[0] }; 
	T_in N_face45[3] = {v4[1]*v5[2]-v4[2]*v5[1], v4[2]*v5[0]-v4[0]*v5[2], v4[0]*v5[1]-v4[1]*v5[0] }; 
	T_in N_face56[3] = {v5[1]*v6[2]-v5[2]*v6[1], v5[2]*v6[0]-v5[0]*v6[2], v5[0]*v6[1]-v5[1]*v6[0] }; 
	T_in N_face61[3] = {v6[1]*v1[2]-v6[2]*v1[1], v6[2]*v1[0]-v6[0]*v1[2], v6[0]*v1[1]-v6[1]*v1[0] }; 

	// normalize //
	normalize_vector(N_face12);
	normalize_vector(N_face23);
	normalize_vector(N_face34);
	normalize_vector(N_face45);
	normalize_vector(N_face56);
	normalize_vector(N_face61);
	
	// Then, get N_vertices from N_faces //	
	// Uniform weights TODO: use area weight //
	T_in N_vert[3] = { 
		N_face12[0] + N_face23[0] + N_face34[0]+ N_face45[0]+ N_face56[0] + N_face61[0], 
		N_face12[1] + N_face23[1] + N_face34[1]+ N_face45[1]+ N_face56[1] + N_face61[1], 
		N_face12[2] + N_face23[2] + N_face34[2]+ N_face45[2]+ N_face56[2] + N_face61[2] 
	};

	normalize_vector(N_vert);
	// Get the variance of the surface normal around a vertice //
	T_in diff_N_face12 = (T_in) 0.0 ; 
	T_in diff_N_face23 = (T_in) 0.0 ; 
	T_in diff_N_face34 = (T_in) 0.0 ; 
	T_in diff_N_face45 = (T_in) 0.0 ; 
	T_in diff_N_face56 = (T_in) 0.0 ; 
	T_in diff_N_face61 = (T_in) 0.0 ; 
	vector_diff(N_vert, N_face12, diff_N_face12);
	vector_diff(N_vert, N_face23, diff_N_face23);
	vector_diff(N_vert, N_face34, diff_N_face34);
	vector_diff(N_vert, N_face45, diff_N_face45);
	vector_diff(N_vert, N_face56, diff_N_face56);
	vector_diff(N_vert, N_face61, diff_N_face61);
	

	// Then, get the pixel value assumeing lambertian surface //

	// Assuming Lambertian surface, get the reconstruction error // 
	//T_in spn;
	T_in sp_dist;
	T_in spn_f12, spn_f23, spn_f34, spn_f45, spn_f56, spn_f61;
	for(int ilight = 0; ilight < nlight; ilight++){
		T_in s[3] = { (T_in)S(0, ilight), (T_in)S(1, ilight), (T_in)S(2, ilight) };	
		T_in sp[3];
		vector_sub(s, p0, sp);	
		
//		vector_dot(sp, N_vert, spn);
		vector_dot(sp, N_face12, spn_f12);
		vector_dot(sp, N_face23, spn_f23);
		vector_dot(sp, N_face34, spn_f34);
		vector_dot(sp, N_face45, spn_f45);
		vector_dot(sp, N_face56, spn_f56);
		vector_dot(sp, N_face61, spn_f61);

//		spn_f12 = std::max( (T_in)0.0, spn_f12);
//		spn_f23 = std::max( (T_in)0.0, spn_f23);
//		spn_f34 = std::max( (T_in)0.0, spn_f34);
//		spn_f45 = std::max( (T_in)0.0, spn_f45);
//		spn_f56 = std::max( (T_in)0.0, spn_f56);
//		spn_f61 = std::max( (T_in)0.0, spn_f61);
		normalize_vector(sp, sp_dist);
		T_in denom_ = (T_in) pow(sp_dist,3.0); 
//		T_in nome_ = spn * (T_in)Se(ilight) * (T_in)rho;
		T_in nome_ = spn_f12 ;
		nome_ += spn_f23 ;
		nome_ += spn_f34 ;
		nome_ += spn_f45 ;
		nome_ += spn_f56 ;
		nome_ += spn_f61 ;
		nome_ = nome_ * ( (T_in)Se(ilight) * (T_in)rho);
		nome_ = nome_ / 6.0;

		// self-shadow //
		if( nome_ < 0.0){ 
			nome_ = (T_in) 0.0;
		}

		// This residual is the pixel-wise reconstruction error //
		residuals[ilight] = nome_ / denom_ - (T_in)observations(ilight);
		//Normalize
//		residuals[ilight] = (nome_ / denom_ - (T_in)observations(ilight)) / (T_in)observations(ilight);
		}
    
	// The smoothness constraint //
	T_in z_mean = (z0[0] + z1[0]+ z2[0]+ z3[0]+ z4[0]+ z5[0]+ z6[0]) / 7.0;
//	T_in z_var =      pow( z0[0]-z_mean, 2.0 ) 
//				      + pow( z1[0]-z_mean, 2.0 )
//				      + pow( z2[0]-z_mean, 2.0 )
//				      + pow( z3[0]-z_mean, 2.0 )
//				      + pow( z4[0]-z_mean, 2.0 )
//				      + pow( z5[0]-z_mean, 2.0 )
//				      + pow( z6[0]-z_mean, 2.0 ) ;
//	z_var = sqrt(z_var);
	residuals[nlight]   = (z0[0] - z_mean)*  lambda_smooth;
	residuals[nlight+1] = (z1[0] - z_mean)*  lambda_smooth;
	residuals[nlight+2] = (z2[0] - z_mean)*  lambda_smooth;
	residuals[nlight+3] = (z3[0] - z_mean)*  lambda_smooth;
	residuals[nlight+4] = (z4[0] - z_mean)*  lambda_smooth;
	residuals[nlight+5] = (z5[0] - z_mean)*  lambda_smooth;
	residuals[nlight+6] = (z6[0] - z_mean)*  lambda_smooth;

	residuals[nlight]   += diff_N_face12 * lambda_smooth * (T_in) 50.0 *(T_in)5;
	residuals[nlight+3] += diff_N_face45 * lambda_smooth * (T_in) 50.0 *(T_in)5;
	residuals[nlight+1] += diff_N_face23 * lambda_smooth * (T_in) 50.0 *(T_in)5;
	residuals[nlight+2] += diff_N_face34 * lambda_smooth * (T_in) 50.0 *(T_in)5;
	residuals[nlight+4] += diff_N_face56 * lambda_smooth * (T_in) 50.0 *(T_in)5;
	residuals[nlight+5] += diff_N_face61 * lambda_smooth * (T_in) 50.0 *(T_in)5;
//
//	residuals[nlight]   = (T_in)sqrt(abs_tin(z0[0] -  z_mean))*  lambda_smooth;
//	residuals[nlight+1] = (T_in)sqrt(abs_tin(z1[0] -  z_mean))*  lambda_smooth;
//	residuals[nlight+2] = (T_in)sqrt(abs_tin(z2[0] -  z_mean))*  lambda_smooth;
//	residuals[nlight+3] = (T_in)sqrt(abs_tin(z3[0] -  z_mean))*  lambda_smooth;
//	residuals[nlight+4] = (T_in)sqrt(abs_tin(z4[0] -  z_mean))*  lambda_smooth;
//	residuals[nlight+5] = (T_in)sqrt(abs_tin(z5[0] -  z_mean))*  lambda_smooth;
//	residuals[nlight+6] = (T_in)sqrt(abs_tin(z6[0] -  z_mean))*  lambda_smooth;
	
	return true;

	}
	
	static ceres::CostFunction* Create(
					const VectorXd observations,
					const MatrixXd S,
					const VectorXd Se,
					const MatrixXd kuv,
					const MatrixXi F,
					const double rho,
					const int nlight_in, 
					const double lambda_smooth){
		return (new ceres::AutoDiffCostFunction<pixel_recon_err, ceres::DYNAMIC,
								1, 1, 1, 1, 1, 1, 1>(new pixel_recon_err(
												observations, S, Se, kuv, F, rho,
												lambda_smooth	
												), 
										nlight_in + 7 )); // dimension of residual is # of lights + 7 (smoothness)
	}
	
	VectorXd observations;
	// each col is an observation, vertice or face 
	MatrixXd S; 
	int nlight;
	VectorXd Se;
	MatrixXd kuv; 
	MatrixXi F;
	double rho;

	double lambda_smooth = 0.0;
};

// ## TESTED : OK, able to simulate the surface normal map correctly ## //
bool PixelReconstruct::pixel_recon(
			const	 double*   z0, 
			const	 double*   z1,
			const	 double*   z2,
			const	 double*   z3,
			const	 double*   z4,
			const	 double*   z5,
			const	 double*   z6,
			double* intensity 
				 ) {

	MatrixXd V(3, 7);
	// V = kuv * z 
	V.col(0) = kuv.col(0) * z0[0]; 		
	V.col(1) = kuv.col(1) * z1[0]; 		
	V.col(2) = kuv.col(2) * z2[0]; 		
	V.col(3) = kuv.col(3) * z3[0]; 		
	V.col(4) = kuv.col(4) * z4[0]; 		
	V.col(5) = kuv.col(5) * z5[0]; 		
	V.col(6) = kuv.col(6) * z6[0]; 		

	// Get the vertice normal 
	igl::per_vertex_normals(V.transpose(), this->F.transpose(), this->N_vertices);

//	std::cout << "Got the surface normal for the current pixel ! " << std::endl;

	// Assuming Lambertian surface 
//	double denom_ = 0.0; 
//	double nome_ = 1.0; 
	for(int ilight = 0; ilight < this->nlight; ilight++){
		double denom_ = pow((N_vertices.row(0).transpose() - S.col(0)).norm(), 3.0);
		double nome_ = 
				N_vertices.row(0).transpose().dot( S.col(ilight) - V.col(0) ) * Se(ilight) * rho; 
		this->intensity_(ilight) = nome_ / denom_;
//		intensity[0] = nome_ / denom_;
	}

	return true;
	}


// ## TESTED : OK, able to simulate the surface normal map correctly ## //
template <typename T_in> 
bool PixelReconstruct::pixel_reconT(
					const T_in* const z0,
					const T_in* const z1,
					const T_in* const z2,  
					const T_in* const z3,  
					const T_in* const z4,  
					const T_in* const z5,  
					const T_in* const z6,
				  	T_in* residuals){
	// Given z0 ~ z6 and kuvs, get the 3D locations for the vertices //
	T_in p0[3] = {(T_in)kuv(0, 0) * z0[0], (T_in)kuv(1,0) *z0[0] , (T_in)kuv(2,0) *z0[0]};
	T_in p1[3] = {(T_in)kuv(0, 1) * z1[0], (T_in)kuv(1,1) *z1[0] , (T_in)kuv(2,1) *z1[0]};
    T_in p2[3] = {(T_in)kuv(0, 2) * z2[0], (T_in)kuv(1,2) *z2[0] , (T_in)kuv(2,2) *z2[0]};
	T_in p3[3] = {(T_in)kuv(0, 3) * z3[0], (T_in)kuv(1,3) *z3[0] , (T_in)kuv(2,3) *z3[0]};
	T_in p4[3] = {(T_in)kuv(0, 4) * z4[0], (T_in)kuv(1,4) *z4[0] , (T_in)kuv(2,4) *z4[0]};
	T_in p5[3] = {(T_in)kuv(0, 5) * z5[0], (T_in)kuv(1,5) *z5[0] , (T_in)kuv(2,5) *z5[0]};
	T_in p6[3] = {(T_in)kuv(0, 6) * z6[0], (T_in)kuv(1,6) *z6[0] , (T_in)kuv(2,6) *z6[0]};

	// get the vectors //
	T_in v1[3] = { p1[0]-p0[0], p1[1]-p0[1], p1[2]-p0[2]}; 
	T_in v2[3] = { p2[0]-p0[0], p2[1]-p0[1], p2[2]-p0[2]}; 
	T_in v3[3] = { p3[0]-p0[0], p3[1]-p0[1], p3[2]-p0[2]};
	T_in v4[3] = { p4[0]-p0[0], p4[1]-p0[1], p4[2]-p0[2]};
	T_in v5[3] = { p5[0]-p0[0], p5[1]-p0[1], p5[2]-p0[2]};
	T_in v6[3] = { p6[0]-p0[0], p6[1]-p0[1], p6[2]-p0[2]};
	
	// First, get the N_faces by cross product //
	T_in N_face12[3] = {v1[1]*v2[2]-v1[2]*v2[1], v1[2]*v2[0]-v1[0]*v2[2], v1[0]*v2[1]-v1[1]*v2[0] }; 
	T_in N_face23[3] = {v2[1]*v3[2]-v2[2]*v3[1], v2[2]*v3[0]-v2[0]*v3[2], v2[0]*v3[1]-v2[1]*v3[0] }; 
	T_in N_face34[3] = {v3[1]*v4[2]-v3[2]*v4[1], v3[2]*v4[0]-v3[0]*v4[2], v3[0]*v4[1]-v3[1]*v4[0] }; 
	T_in N_face45[3] = {v4[1]*v5[2]-v4[2]*v5[1], v4[2]*v5[0]-v4[0]*v5[2], v4[0]*v5[1]-v4[1]*v5[0] }; 
	T_in N_face56[3] = {v5[1]*v6[2]-v5[2]*v6[1], v5[2]*v6[0]-v5[0]*v6[2], v5[0]*v6[1]-v5[1]*v6[0] }; 
	T_in N_face61[3] = {v6[1]*v1[2]-v6[2]*v1[1], v6[2]*v1[0]-v6[0]*v1[2], v6[0]*v1[1]-v6[1]*v1[0] }; 

	// normalize //
	normalize_vector(N_face12);
	normalize_vector(N_face23);
	normalize_vector(N_face34);
	normalize_vector(N_face45);
	normalize_vector(N_face56);
	normalize_vector(N_face61);
	
	// Then, get N_vertices from N_faces //	
	// Uniform weights TODO: use area weight //
	// weights TODO: use area weight //
	T_in N_vert[3] = { 
		N_face12[0] + N_face23[0] + N_face34[0]+ N_face45[0]+ N_face56[0] + N_face61[0], 
		N_face12[1] + N_face23[1] + N_face34[1]+ N_face45[1]+ N_face56[1] + N_face61[1], 
		N_face12[2] + N_face23[2] + N_face34[2]+ N_face45[2]+ N_face56[2] + N_face61[2] 
	};
	normalize_vector(N_vert);
//	VectorXd N_vert_ = Eigen::Map<VectorXd,0>(N_vert);
	Eigen::Map<VectorXd> N_vert_(N_vert,3);
	this->N_vertices = N_vert_;
	

	// Then, get the pixel value assuming lambertian surface //

	T_in spn, sp_dist;
	for(int ilight = 0; ilight < nlight; ilight++){
		T_in s[3] = { (T_in)S(0, ilight), (T_in)S(1, ilight), (T_in)S(2, ilight) };	
		T_in sp[3];
		vector_sub(s, p0, sp);	
		vector_dot(sp, N_vert, spn);
		normalize_vector(sp, sp_dist);
		T_in denom_ = (T_in) pow(sp_dist,3.0); 
		T_in nome_ = spn * (T_in)Se(ilight) * (T_in)rho;
		
		// self-shadow //
		if(nome_ < 0.0){
			nome_ = (T_in) 0.0;
		}

		this->intensity_(ilight) = nome_ / denom_;
		}

	return true;
}

bool PS_Problem::est_pixel_dt_values_cur(){
	/* estimate the pixel dt values absed on the current state of depths_ */
		
		double pixel_intensities_dt_[this->nlight];	
		for(int i=0; i < this->Npt; i++){
			MatrixXd kuv_(3,7);
			VectorXd observations_per = this->observations.col(i);
//			VectorXd observations_dt_per = this->observations.col(i);
			VectorXd observations_dt_per = this->observations_dt.col(i);

			this->getPixel_kuv(i, kuv_);

			pixel_dt_recon_err pixel_dt_recon_err(
					observations_dt_per, // observed I_t
					observations_per,
					this->S,
					this->S_t,
					this->S_h, 
					this->Se,
					kuv_,
					this->Rho(i),
					this->lambda_smooth,  
					this->dt_pow_para
					);

			pixel_dt_recon_err.est_pixel_dt_values_cur_(
						this->mutable_depth_for_observation(i, 0), 
						this->mutable_depth_for_observation(i, 1), 
						this->mutable_depth_for_observation(i, 2), 
						this->mutable_depth_for_observation(i, 3), 
						this->mutable_depth_for_observation(i, 4), 
						this->mutable_depth_for_observation(i, 5), 
						this->mutable_depth_for_observation(i, 6), 
						pixel_intensities_dt_	
							);

			for(int ilight=0; ilight< this->nlight; ilight++){
				this->cur_img_dt(ilight, i) = pixel_intensities_dt_[ilight];
			}

		}

		return true;
}

bool PS_Problem::est_pixel_values_cur(){

	double pixel_intensities_[this->nlight];

	for(int i=0; i< this->Npt; i++){
		MatrixXd kuv_(3,7) ;
		VectorXd observations_per = this->observations.col(i);

		this->getPixel_kuv(i, kuv_);

		pixel_recon_err pixel_recon_per( observations_per,
							this->S, 
							this->Se, 
							kuv_,
							this->F_pixel, 
							this->Rho(i),
							this->lambda_smooth);

		pixel_recon_per.est_pixel_values_cur_( 
						this->mutable_depth_for_observation(i, 0), 
						this->mutable_depth_for_observation(i, 1), 
						this->mutable_depth_for_observation(i, 2), 
						this->mutable_depth_for_observation(i, 3), 
						this->mutable_depth_for_observation(i, 4), 
						this->mutable_depth_for_observation(i, 5), 
						this->mutable_depth_for_observation(i, 6), 
					    pixel_intensities_	
						);

		for(int ilight=0; ilight< this->nlight; ilight++){
			this->cur_img(ilight, i) = pixel_intensities_[ilight];
		}
	}	
	return true;	
}

bool PS_Problem::form_prob(){
	assert(Npt > 0 );
	std::cout << "There are " << Npt << " variables in this problem" << std::endl; 	

	for(int i=0; i< this->Npt; i++){
		MatrixXd kuv_(3,7) ;
		VectorXd observations_per = this->observations.col(i);
//		std::cout << "observations size " << Npt << std::endl;
//		std::cout << "observation_per size " << observations_per.rows() << " "
//				<< observations_per.cols() << std::endl;
		this->getPixel_kuv(i, kuv_);

//		std::cout << "kuv_ size " << kuv_.rows() << " "
//				<< kuv_.cols() << std::endl;

		ceres::CostFunction* ps_cost_fun = 
				pixel_recon_err::Create(
					observations_per,
					this->S,
					this->Se,
					kuv_, 
					this->F_pixel, 
					this->Rho(i),
					this->nlight,
					this->lambda_smooth);

		this->ceres_PS_problem.AddResidualBlock(
						ps_cost_fun,
						NULL,
						this->mutable_depth_for_observation(i, 0), 
						this->mutable_depth_for_observation(i, 1), 
						this->mutable_depth_for_observation(i, 2), 
						this->mutable_depth_for_observation(i, 3), 
						this->mutable_depth_for_observation(i, 4), 
						this->mutable_depth_for_observation(i, 5), 
						this->mutable_depth_for_observation(i, 6) 
						);

	}	

	return true;
}

bool PS_Problem::form_prob_dt(){
	assert(Npt > 0 );

	std::cout << "There are " << Npt << " variables in this problem" << std::endl; 	
	std::cout << "nearPS(): " << "Use Idt" << std::endl;

	for(int i=0; i< this->Npt; i++){
		MatrixXd kuv_(3,7) ;
		VectorXd observations_per = this->observations.col(i);
		VectorXd observations_dt_per = this->observations_dt.col(i);
		this->getPixel_kuv(i, kuv_);

		ceres::CostFunction* ps_cost_fun_dt = 
				pixel_dt_recon_err::Create(
					observations_dt_per,
					observations_per,
					this->S,
					this->S_t,
					this->S_h, 
					this->Se,
					kuv_, 
					this->F_pixel, 
					this->Rho(i),
					this->nlight, 
					this->lambda_smooth, 
					this->dt_pow_para);



		this->ceres_PS_problem.AddResidualBlock(
						ps_cost_fun_dt,
						NULL,
						this->mutable_depth_for_observation(i, 0), 
						this->mutable_depth_for_observation(i, 1), 
						this->mutable_depth_for_observation(i, 2), 
						this->mutable_depth_for_observation(i, 3), 
						this->mutable_depth_for_observation(i, 4), 
						this->mutable_depth_for_observation(i, 5), 
						this->mutable_depth_for_observation(i, 6) 
						);

	}	

	return true;
}

bool PS_Problem::solve_prob(){
	// Set options //
	ceres_PS_problem_options.linear_solver_type = ceres::DENSE_QR;
	ceres_PS_problem_options.minimizer_progress_to_stdout = true;
	ceres_PS_problem_options.minimizer_type = ceres::LINE_SEARCH;
	ceres_PS_problem_options.line_search_direction_type = ceres::LBFGS;
	ceres_PS_problem_options.max_num_iterations = this->max_iter;
	ceres_PS_problem_options.num_threads = this->nThreads;


	// Being to solve //
	std::cout << "Begin to solve the problem !" << std::endl;
	signal(SIGSEGV, handler);
	ceres::Solve(this->ceres_PS_problem_options, &(this->ceres_PS_problem),
					&(this->summary));
	std::cout << "Done solving the ceres_PS_problem!" << std::endl;

	// print //
	std::cout << summary.FullReport() << std::endl;

	return true;	
}




/***********************************************/
// PYTHON BINDING for the simpleNearPS module //
namespace py=pybind11;
PYBIND11_MODULE(simpleNearPS, m){
	m.doc() = "simpleNearPS Module";

//	PS_Problem( const MatrixXd& S_light, 
//				const MatrixXd& Kuv, 
//				const VectorXd& RHo,
//				const MatrixXi& Indx_M, 
//				const MatrixXi& F_in, 
//				const VectorXd& S_e) 
	py::class_<PS_Problem>(m, "PS_Problem")
			.def(py::init<  const MatrixXd&, 
							const MatrixXd&, 
							const VectorXd&, 
							const MatrixXi&, 
							const MatrixXi&, 
							const MatrixXi&,
							const VectorXd&,  
							const int&
							>())
			.def("getPixel_kuv", &PS_Problem::getPixel_kuv)
			.def("return_depths", &PS_Problem::return_depths)
			.def("return_pixelKuv", &PS_Problem::return_pixelKuv)
			.def("set_observations", &PS_Problem::set_observations)
			.def("set_only_obs", &PS_Problem::set_only_obs)
			.def("set_maxIter", &PS_Problem::set_maxIter)
			.def("form_prob", &PS_Problem::form_prob)
			.def("form_prob_dt", &PS_Problem::form_prob_dt)
			.def("set_initial_vars", &PS_Problem::set_initial_vars)
			.def("set_lambda_smooth", &PS_Problem::set_lambda_smooth)
			.def("set_dt_pow_para", &PS_Problem::set_dt_pow_para)
			.def("set_Se", &PS_Problem::set_Se)
			.def("set_nThreads", &PS_Problem::set_nThreads)
			.def("set_Rhos", &PS_Problem::set_Rhos)
			.def("est_pixel_values_cur", &PS_Problem::est_pixel_values_cur)
			.def("est_pixel_dt_values_cur", &PS_Problem::est_pixel_dt_values_cur)
			.def("get_observations", &PS_Problem::get_observations)
			.def("get_observations_dt", &PS_Problem::get_observations_dt)
			.def("get_lambda_smooth", &PS_Problem::get_lambda_smooth)
			.def("get_Npt", &PS_Problem::get_Npt)
			.def("get_cur_img", &PS_Problem::get_cur_img)
			.def("get_cur_img_dt", &PS_Problem::get_cur_img_dt)
			.def("get_S", &PS_Problem::get_S)
			.def("get_St", &PS_Problem::get_St)
			.def("solve_prob", &PS_Problem::solve_prob)
			.def("destroy", &PS_Problem::destroy)
			.def("mutable_depth_for_observation", &PS_Problem::mutable_depth_for_observation);


//	PixelReconstruct(const MatrixXd& kuv_in, 
//					 const MatrixXd& S_in,
//					 const MatrixXi& F_in, 
//					 const VectorXd& Se_in, 
//					 const double& rho_in)
	py::class_<PixelReconstruct>(m, "PixelReconstruct")
			.def(py::init<const MatrixXd& , 
					 	  const MatrixXd&,
					 	  const MatrixXi&, 
					 	  const VectorXd&, 
					 	  const double&
					 		>())
			.def("set_kuv", &PixelReconstruct::set_kuv)
			.def("set_F", &PixelReconstruct::set_F)
			.def("pixel_recon", &PixelReconstruct::pixel_recon)
			.def("pixel_reconT", &PixelReconstruct::pixel_reconT<double>)
			.def("get_rho", &PixelReconstruct::get_rho)
			.def("get_N", &PixelReconstruct::get_N)
			.def("get_intensity", &PixelReconstruct::get_intensity)
			.def("get_F", &PixelReconstruct::get_F);
}

