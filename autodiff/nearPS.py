import numpy as np
import torch
from torch.autograd import Variable
from torch.autograd import Function
import ipdb

#class Shader(Function):
#    def forward(self, input):
#        np_input = input.numpy()
#        ref_ = np.ones(np_input.shape)
#        result = np.sum(np.abs(np_input - ref_))
#
#        return torch.FloatTensor([result])

def fun(input):
    #np_input = input.numpy()
    ref_ = torch.ones(input.size())
    ref_ = Variable(ref_)
    result = torch.sum(torch.abs(input - ref_))
    return result

def test_shader(input):
    return Shader()(input)
