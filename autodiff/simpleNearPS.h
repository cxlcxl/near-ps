#ifndef __SIMPLENEARPS__
#define __SIMPLENEARPS__

#include </usr/local/include/eigen3/Eigen/Core>
#include </usr/local/include/eigen3/Eigen/Dense>
#include <pybind11/eigen.h>
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

// igl functions //
#include <igl/slice.h>
#include <igl/per_vertex_normals.h>

// ceres-solver //
#include "ceres/ceres.h"


#include <iostream>
#include <cstdio>
#include <cmath>
#include <cassert>


// debugging //
#include <execinfo.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>

using MatrixXd = Eigen::MatrixXd;
using MatrixXi = Eigen::MatrixXi;
using VectorXd = Eigen::VectorXd;
using VectorXi = Eigen::VectorXi;



//for debugging// 
//
//
//

void handler(int sig){
	void *array[10];
	size_t size;
	size = backtrace(array,10);
	fprintf(stderr, "Error: signal: %d:\n", sig);
	backtrace_symbols_fd(array, size, STDERR_FILENO);
	exit(1);
}

// 

class PS_Problem{
public:
	PS_Problem( const MatrixXd& S_light, 
				const MatrixXd& Kuv, 
				const VectorXd& RHo,
				const MatrixXi& Indx_M, 
				const MatrixXi& Var_Indx_M, 
				const MatrixXi& F_in, 
				const VectorXd& S_e)
	 {
		S = S_light;
		KUV = Kuv;
		Rho = RHo;
		AdjIndx_M = Indx_M;
		Var_AdjIndx_M = Var_Indx_M;
		F_pixel = F_in;
		Se = S_e;
		this->nlight = S.cols();
		this->nThreads = 10;

		// # of variables 
		this->NVars = (int) Var_Indx_M.maxCoeff() + 1; 
		S_t = MatrixXd::Zero(S.rows(), S.cols());
		for(int ilight=0; ilight < this->nlight; ilight++){
			int jlight = ilight - 1;
			if(jlight < 0){
				jlight = this->nlight -1;
			}
			S_t.col(ilight) = (S.col( (ilight+1)%this->nlight ) - S.col(jlight)) * .5;
		}	
	
	}

	PS_Problem( const MatrixXd& S_light, 
				const MatrixXd& Kuv, 
				const VectorXd& RHo,
				const MatrixXi& Indx_M, 
				const MatrixXi& Var_Indx_M, 
				const MatrixXi& F_in, 
				const VectorXd& S_e, 
				const int& fem_d )
	 {
		S = S_light;
		KUV = Kuv;
		Rho = RHo;
		AdjIndx_M = Indx_M;
		Var_AdjIndx_M = Var_Indx_M;
		F_pixel = F_in;
		Se = S_e;
		this->nlight = S.cols();
		this->nThreads = 10;

		this->fem_d = fem_d;
		std::cout << "PS_Problem(): fem_d = " << this->fem_d << std::endl;

		this->NVars = (int) Var_Indx_M.maxCoeff() + 1; 
		this->S_t = MatrixXd::Zero(S.rows(), S.cols());
		this->S_h = MatrixXd::Zero(S.rows(), S.cols());
		for(int ilight=0; ilight < this->nlight; ilight++){
			int jlight = ilight - fem_d;
			if(jlight < 0){
				jlight = (jlight + this->nlight) % this->nlight;
			}
			this->S_t.col(ilight) = (S.col( (ilight + fem_d )%this->nlight ) - S.col(jlight)) * (1.0/(2.*(double)fem_d));
			this->S_h.col(ilight) = (S.col( (ilight + fem_d )%this->nlight ) + S.col(jlight)) * .5;
		}	
	
	}

	~PS_Problem(){
		delete[] depths_;
	}


	void destroy(){
		delete[] depths_;
	}

	///
	void getPixel_kuv(int ipixel, MatrixXd& KUV_pix){
	// Get the kuv of the pixel and its ring-1 neighbors 
		//size of KUV_pix: 3 x 7
		for(int i = 0; i<7; i++){
			KUV_pix.col(i) = KUV.col( AdjIndx_M(i, ipixel) );
		}	
	}

	VectorXd return_depths(){
		VectorXd depth_v(NVars, 1);	
		for(int i=0; i< NVars; i++){
			depth_v(i) = depths_[i];
		}
		return depth_v;
	}

	MatrixXd return_pixelKuv(int ipixel){
		MatrixXd kuv_pixel(3,7);
		for(int i = 0; i<7; i++){
			kuv_pixel.col(i) = KUV.col(AdjIndx_M(i, ipixel));
		}	
		return kuv_pixel; 
	}

	MatrixXd get_observations(){
		return this->observations;
	}

	double get_lambda_smooth(){
		return this->lambda_smooth;
	}

	MatrixXd get_observations_dt(){
		return this->observations_dt;
	}

	MatrixXd get_cur_img_dt(){
		return this->cur_img_dt;
	}

	MatrixXd get_cur_img(){
		return this->cur_img;
	}

	int get_Npt(){
		return this->Npt;	
	}

	MatrixXd get_S(){
		return this->S;
	}
	MatrixXd get_St(){
		return this->S_t;
	}


	void set_observations(const MatrixXd& observ){
		// set the measured values I and the differential term I_t //
		observations = observ;
		Npt = observations.cols();

		observations_dt = MatrixXd::Zero(observations.rows(), observations.cols());
		for(int irow=0; irow < this->nlight; irow++  ){
//			observations_dt.row(irow) = observations.row( (irow+1)%(this->nlight) ) - 
//					observations.row(irow);
			if(this->fem_d ==1){
				int jrow = irow - 1;
				if(jrow < 0){
					jrow = this->nlight - 1;
				}
				observations_dt.row(irow) = (observations.row( (irow+1)%(this->nlight) ) - 
						observations.row( jrow ) )  * .5;
			}
			else{
				int fem_d = this->fem_d;
				int jrow = irow - fem_d;
				if(jrow < 0){
					jrow = (jrow + this->nlight) % this->nlight;
				}
				observations_dt.row(irow) = (observations.row( (irow+1)%(this->nlight) ) - 
						observations.row( jrow ) )  * (1.0/(2.0* (double)fem_d));
			}
		}

		cur_img = MatrixXd::Zero(observations.rows(), observations.cols());
		cur_img_dt = MatrixXd::Zero(observations.rows(), observations.cols());
	}

	void set_only_obs(const MatrixXd& observ){
		observations = observ;
	}

	void set_maxIter(const int max_iter){
		this->max_iter = max_iter;	
	}

	void set_nThreads(const int nThreads){
		this-> nThreads = nThreads;
	}

	void set_initial_vars(const VectorXd& init_values){
		// This function should be called AFTER	set_observations()
		assert(Npt >0 );	
		assert(NVars > 0 );	
		assert(init_values.size() == NVars);

		std::cout << "# of observations : " << Npt << std::endl;
		std::cout << "# of vars: " << NVars << std::endl;

		//Done: The number of vars does not equal to Npt (the # of points for observations)
		depths_ = new double[NVars]; 
		for(int i=0; i<NVars; i++){
			depths_[i] = (double) init_values(i);
		}
	}

	void set_lambda_smooth(const double lambda_smooth_in){
		this->lambda_smooth = lambda_smooth_in;
	}

	void set_dt_pow_para(const double dt_pow_para_in){
		this->dt_pow_para = dt_pow_para_in;
	}

	void set_Rhos(const VectorXd& rho){
		this->Rho = rho; 	
	}

	void set_Se(const VectorXd& se){
		this->Se = se;
	}


	double* mutable_depth(){
	 	return depths_;
	}

	double* mutable_depth_for_observation(int ipixel, int indx_adj){
		// input: indx_adj - should be from 0 to 6: the 0~6 neighbor of pixel ipixel
//		std::cout << depths_[this->Var_AdjIndx_M(indx_adj, ipixel)] << std::endl;
		return mutable_depth() + this->Var_AdjIndx_M(indx_adj, ipixel);	
	}

	/* formulate the problem: add the residual block to the ceres problem variable */
	bool form_prob(); 

	/* formulate the problem for dt: add the residual block to the ceres problem variable */
	bool form_prob_dt(); 
		
	/* estimate the pixel values based on the current state of depths_ */
	bool est_pixel_values_cur();

	/* estimate the pixel dt values absed on the current state of depths_ */
	bool est_pixel_dt_values_cur();

	/* solve the ceres problem, this problem could either be the raw reconstruction,
	 * or the dt of reconstruction */
	bool solve_prob();
			
	

private:

	//AdjIndx_M: 7 x Npt, Npt is the number of pixels for observations 
	//KUV: 3xnPixel matices, nPixel is the # of pixels in the image.
	//The pixel index is in col-major order  
	//S: 3 x nLights  light source positions
	//AdjIndx_M 7 x Npt : for the ipixel-th col: the index of adjancent points in the column-major pixel index map
	//Var_AdjIndx_M 7 x Npt : for the ipixel-th col: the index of adjacent points in the column-major variable index map
	MatrixXd  KUV, S; 
	MatrixXd S_t; // The light source direction changes, as size as S
	MatrixXd S_h; // the midpoint light sources 
	MatrixXi AdjIndx_M; 
// For each observed pixel, what is its adjacent pixels in terms of the variable indx 
	MatrixXi Var_AdjIndx_M; 	
	MatrixXi F_pixel;

	VectorXd Rho; // Npt x 1
	VectorXd Se;  // nlights x 1
	
	MatrixXd observations; // the input image intensities, nlight x npt, each col is for a point
	MatrixXd observations_dt; // the temporal changes of the input image intensities, nlight x npt, each col is for one point

	MatrixXd cur_img; // the image intensities, nlight x npt for the current states of depths_
	MatrixXd cur_img_dt; // the image intensity changes for the current states of depth_, same dimension as cur_img

	double* depths_; // The number of vars does not necessarily be the same as the number of pixels
	int Npt = 0 ;   //Npt is the number of pixels for observations
	int NVars = 0; // # the number of vars
	int nlight ; 

	// shape prior //
	double lambda_smooth = 0.0;

	// pow para dt 
	double dt_pow_para = 2.0;

	// Ceres problem //
	ceres::Problem ceres_PS_problem;
	ceres::Solver::Options ceres_PS_problem_options;
	ceres::Solver::Summary summary;
	int max_iter = 50;
	int nThreads = 10;

	// fem_d //
	int fem_d = 1;
};

class PixelReconstruct{
	
public:
	PixelReconstruct(const MatrixXd& kuv_in, 
					 const MatrixXd& S_in,
					 const MatrixXi& F_in, 
					 const VectorXd& Se_in, 
					 const double& rho_in){
		kuv = kuv_in;
		S = S_in;
		F = F_in;	
		Se = Se_in;
		rho = rho_in;
		
		this->nlight = S_in.cols();
		VectorXd intensities(this->nlight);
		this->intensity_ = intensities;
	}
	
	bool pixel_recon(const double*   z0, 
					 const double*   z1,
					 const double*   z2,
					 const double*   z3,
					 const double*   z4,
					 const double*   z5,
					 const double*   z6,
					 double* intensity 
					 );

	template <typename T_in> 
			bool pixel_reconT(
					const T_in* const z0,
					const T_in* const z1,
					const T_in* const z2,  
					const T_in* const z3,  
					const T_in* const z4,  
					const T_in* const z5,  
					const T_in* const z6,
				  	T_in* residuals);

	void set_kuv(const MatrixXd& kuv_in){
		this->kuv = kuv_in;	
	}

	void set_F(const MatrixXi& F_in){
		this->F = F_in;	
	}

	double get_rho(){
		return this->rho;
	}

	VectorXd get_intensity(){
		return this->intensity_;
	}

	MatrixXd get_N(){
		return this->N_vertices;
	}

	MatrixXi get_F(){
		return this->F;
	}


private: 
	MatrixXd N_vertices	; // 3 x 7 normal vectors
	MatrixXd kuv, S; // kuv, 3 x 7 KUV matrix for one pixel, 
				 	 // S: 3 x nlight the light source positions
	MatrixXi F; // 3 x 6 matrix the face matrix for the pixel , each col is a face
	VectorXd Se; // the light source intensities  
	double rho; // the albedo
	VectorXd intensity_;

	int nlight;
	
};


#endif 
