# INSTALL #
Test with python3 and ipython3

Installing ipython in Anaconda is highly recommended 


# REQUIRED PACKAGES #
ceres: http://ceres-solver.org/

libigl (for visualization):  https://github.com/libigl/libigl

opencv: can be installed through anaconda 

pybind11: https://github.com/pybind/pybind11

autograd (for calibration): https://anaconda.org/omnia/autograd

skimage: https://anaconda.org/anaconda/scikit-image

ipdb  https://anaconda.org/conda-forge/ipdb

# Build & Run DEMO#
0. Download the data [here](https://drive.google.com/file/d/1OqsyyyPfRV9Wm5LFyO9n5M5wCsZBUeHA/view?usp=sharing)
1. In terminal, cd to ./autodiff
2. sh ./build.sh ( you will need to change the include file for the included packages)
3. cd back to the root folder. 
4. Change input_path in exp_simulations.py to the folder where you put the downloaded data.
5. Then in terminal:

	ipython3 exp_simulations.py -- --exp compare_our_near_ps_inits

Here we will run PS on simulated data saved in simRes folder

The results would be saved in 

cur_EXP_It_depth_est_scale_${scale}_iter_${iter}.mat

where scale is the index of scale: 1 is the finest scale
iter is the # of iteration

# Visualize the results #

python3 gs_show_zopt3.py cur_EXP_It_depth_est_scale_${scale}_iter_${iter}.mat

where scale is the index of scale: 1 is the finest scale iter is the # of iteration

