import numpy as np
import scipy.io as sio
import matplotlib.pyplot as plt
import ipdb
import math
import time
import argparse


# -- Generate images -- #
LED_RADIUS = .5 #.2 # LED radius to generate the image #
LED_INTENSITY = 10 
LED_NUMBER =24

input_path = './' # folder where the simulation data is located

# Simulation experiment module #
from simulation import experiments as sim_exps

def run_nearPS( IMGs, S, K_inv, drange_init, lambda_It, lambda_Idt, fname_mat, max_Iter_Idt = 600, max_Iter_It = 1000 ):
    from nearLight import near_ps_multi as nearPS_multi
    Scales = (4,1)
    max_Iters_It = np.ones( len(Scales) )
    max_Iters_Idt = np.ones( len(Scales) )

    max_Iters_Idt[0] = max_Iter_Idt
    max_Iters_Idt[1] = max_Iter_Idt
    max_Iters_It[0] = max_Iter_It
    max_Iters_It[1] = max_Iter_It

    nearPSMulti = nearPS_multi.NearLightMulti(IMGs, S, K_inv, Scales, fem_d = 1)
    nearPSMulti.I_mask_var = (np.sum(IMGs, axis=2) > 0 ).astype(np.float)
    nearPSMulti.build_ImgPyramid()
    nearPSMulti.set_paras(\
    	lam_smooth_Idt = lambda_Idt,
    	lam_smooth_It =  lambda_It,
    	MAX_ITER_Idt = 5, MAX_ITER_It = 3,
    	max_Iter_PS_Idt = max_Iters_Idt.astype(np.int), max_Iter_PS_It = max_Iters_It.astype(np.int),
    	dmin= drange_init[0], dmax = drange_init[1])
    nearPSMulti.fname_It_res = fname_mat
    nearPSMulti.save_paras()
    nearPSMulti.solve_scale(0, mean_init_d = False) # Lower res.
    nearPSMulti.solve_scale(1, mean_init_d = False) # Higher res.

def est_NormalDiff(nMap1, nMap2):
    '''
    Estimate the surface normal difference between two surface normal maps
    '''
    if np.ndim(nMap1) == 3 and np.ndim(nMap2) ==3:
        #make sure to normalize the surface normal maps first #
#        nMap1 /= np.sqrt(np.sum(nMap1**2, axis=2))[:,:,np.newaxis]
#        nMap2 /= np.sqrt(np.sum(nMap2**2, axis=2))[:,:,np.newaxis]
        # get nDiffMap #
        nDiffMap = np.arccos(np.sum(nMap1 * nMap2, axis=2) )
    elif np.ndim(nMap1) == 2 and np.ndim(nMap2) == 2:
        # each row is an observation #
#        nMap1 /= np.sqrt(np.sum(nMap1**2, axis=1))[:, np.newaxis]
#        nMap2 /= np.sqrt(np.sum(nMap2**2, axis=1))[:, np.newaxis]
        nDiffMap = np.arccos(np.sum(nMap1 * nMap2, axis=1) )
    return nDiffMap

def exp_FaceBody( exp_names ):
    ''' exp_names - a list of strings, indicating which simulation experiments to do 
    e.g.: exp_names = [ 'face_led_R', 'face_led_D', 'body_led_R', 'body_led_D']
    '''

    if 'numerical' == exp_names:
        # The measurements will be computed numerically #

        dat_fldr = input_path
        ply_name = 'nearPS_GND_objDist_7_bunny.ply'
        gndPath = dat_fldr + ply_name + '.mat'
        # Camera and LED setups #
        # For the camera, the FOV is 90 degrees #
        K = np.eye(3)
        K[0,0] = 1.0/300.5
        K[1,1] = 1.0/300.5
        K[0:2,2] = -1.0
        
        simTestor = sim_exps.Testor(K, gndPath)
        simTestor.setupCapture(LED_intensity = 10, LED_R = .5, N_LED = 24)
        simTestor.genImages()
        
        print('Initialize ...')
        simTestor.initDepth()
        print('Begin to solve ...')
        simTestor.solve_depth()

    if 'gen_images' == exp_names:
        print('Generating images ...')        
        dat_fldr = input_path
        ply_name = 'nearPS_GND_objDist_7_bunny.ply'
        gndPath = dat_fldr + ply_name + '.mat'

        # Camera and LED setups #
        # For the camera, the FOV is 90 degrees #
        K = np.eye(3)
        K[0,0] = 1.0/300.5
        K[1,1] = 1.0/300.5
        K[0:2,2] = -1.0
        # Genearte images #
        simTestor = sim_exps.Testor(K, gndPath)
        simTestor.setupCapture(LED_intensity = LED_INTENSITY, LED_R = LED_RADIUS , N_LED = LED_NUMBER )
        simTestor.genImages(morph_iter=5)
        # Save images #
        print('Saving images...')
        sio.savemat('simRes/input_imgs_dinits_LEDR_%1.2f.mat'%(LED_RADIUS), {'IMGs': simTestor.Imgs,
            'nMap': simTestor.nmap, 'varIndx_Map': simTestor.varIndx_Map, 'K':
            simTestor.K_inv, 'S': simTestor.S, 'dMap': simTestor.dmap })
        print('gen_Images(): Done saving the images !')

    # ------ DIFFERENT FACES ------# 
    if 'faces_sets' == exp_names:
        SAMPLE_NAMES = (\
                'face_00001_20061015_00418_neutral_face05',
                'face_00002_20061015_00448_neutral_face05',
                'face_00006_20080430_04384_neutral_face05',
                'face_00014_20080430_04338_neutral_face05',
                'face_00017_20061201_00812_neutral_face05',
                'face_00021_20061127_00715_neutral_face05',
                'face_00022_20070307_02486_neutral_face05',
                'face_00052_20061024_00526_neutral_face05',
                'face_00053_20061024_00542_neutral_face05',
                'face_00293_20080104_03413_neutral_face05')

        for sample_name in SAMPLE_NAMES:
            gndPath = '{}{}.ply.mat'.format('../data/mitsu_faces/faces/gndTruth/nearPS_GND_objDist_9_', sample_name)
            resPath = './Mt_res/{}.mat'.format(sample_name)
            sim_exps.simpleTest(gndPath, resPath)
    

    # ------ DIFFERENT LED R: Face ------- #
    if 'face_led_R' == exp_names:
        sample_name = 'face_00001_20061015_00418_neutral_face05'
        LED_Rs = np.arange(1., 1.2, .3)
        for led_r in LED_Rs:
            gndPath = '{}{}.ply.mat'.format('../data/mitsu_faces/faces/gndTruth/nearPS_GND_objDist_9_', sample_name)
            resPath = './Mt_res/%s_LEDR_%1.1f.mat'%(sample_name, led_r)
            # NO initializations
        #    sim_exps.simpleTest(gndPath, resPath, LED_R_in = led_r, if_init=False, d_init = 6.0) 
          # HAVE initializations
            sim_exps.simpleTest(gndPath, resPath, LED_R_in = led_r, if_init = True,
                    N_LED_in = 24, max_iter = 10000, lambda_smooth = .0005) 

    # ------ DIFFERENT LED R: Body----- #
    if 'body_LED_R' == exp_names:
        sample_name = 'body0'
        LED_Rs = np.arange(.9, 1, .1)
        for led_r in LED_Rs:
            gndPath = '{}{}.ply.mat'.format('../data/mitsu_bodies/gndTruth/nearPS_GND_objDist_6_', sample_name)
            resPath = './tmp_res/{}_LEDR_{}.mat'.format(sample_name, led_r)
            sim_exps.simpleTest(gndPath, resPath, LED_R_in = led_r, lambda_smooth = .01)

    # ------ DIFFERENT LED #: Face ------- #
    if 'face_nLED' == exp_names:
        sample_name = 'face_00001_20061015_00418_neutral_face05'
        N_LEDs = np.arange(4, 31, 4).astype(np.int)
        for n_led in N_LEDs:
            gndPath = '{}{}.ply.mat'.format('../data/mitsu_faces/faces/gndTruth/nearPS_GND_objDist_9_', sample_name)
            resPath = './Mt_res/NLEDs/%s_NLED_%d.mat'%(sample_name, n_led)
            # HAVE initializations
            sim_exps.simpleTest(gndPath, resPath, LED_R_in = .8, if_init = True,
                    N_LED_in = n_led , max_iter = 10000, lambda_smooth = .0005) 


    # ------- Compare with distant light source PS ---------#
    if 'compare_distLight' == exp_names:
        sample_name = 'face_00001_20061015_00418_neutral_face05'
        LED_Rs = np.arange(.2, 1.1, .2)
        for led_r in LED_Rs:
            data_filename= '/home/chaoliu1/PS_BSSRDF/res/comparison/simulation/methods/wo_init/dinit_4/%s_LEDR_%1.1f.mat'%(sample_name, led_r)
            resPath = ('Mt_res/%s_LEDR_%1.1f.mat')%(sample_name, led_r)
            # NO initializations: Distant light source #
            N_distantL = sim_exps.COMP_DISTL(data_filename, d_init=4.0)
            mat_info = {}
            mat_info['N_distantL'] = N_distantL
            sio.savemat(resPath, mat_info)

    # ------- Different depth init: Compare with distant light source PS ---------#
    if 'compare_distLight_inits' == exp_names:
        print('compare_distLight_inits')
        led_r = 0.8
        data_filename= '../data/sim_input/input_imgs_dinits.mat'

        err_array = []
        for dinit in np.linspace(0, 10, 20):
            print( dinit )
            resPath = ('simRes/DINITS_DistLight_LEDR_%1.1f_dinit_%1.1f.mat')%(led_r, dinit)
            N_distantL = sim_exps.COMP_DISTL(data_filename, d_init= dinit, if_return_NMap=True)
            mat_info = {}
            mat_info['N_distantL'] = N_distantL
            sio.savemat(resPath, mat_info)

            # compare with the ground truth surface normal map # 
            mat_info_gnd = sio.loadmat(data_filename)
            nMap_gnd = mat_info_gnd['nMap']
            nMap_diff= est_NormalDiff(nMap_gnd, N_distantL)
            nMap_diff = np.abs(nMap_diff)
            plt.imsave( 'simRes/DINITS_DistLight_LEDR_%1.1f_dinit_%1.1f.png'%(led_r, dinit), nMap_diff, cmap='jet', vmax=1)
            I_mask = np.sum(mat_info_gnd['IMGs'],axis=2) > 0
            I_mask[np.isnan(I_mask)] = 0
            err_array.append( nMap_diff[300,300])
            # compare with the ground truth dmap #
            dMap_gnd = mat_info_gnd['dMap']
            dMap_diff = (dMap_gnd - dinit) * I_mask.astype(np.float)
            plt.imsave( 'simRes/DmapDiff_DINITS_DistLight_LEDR_%1.1f_dinit_%1.1f.png'%(led_r, dinit), np.abs(dMap_diff), cmap='jet', vmax=10)

    # ------- Compare with var nearPS ---------#
    if 'compare_var_near_ps' == exp_names :
        sample_name = 'face_00001_20061015_00418_neutral_face05'
        LED_Rs = np.arange(.2, 1.1, .2)
        from simulation import COMP_Var
        for led_r in LED_Rs:
            data_filename= '/home/chaoliu1/PS_BSSRDF/res/comparison/simulation/methods/wo_init/dinit_4/%s_LEDR_%1.1f.mat'%(sample_name, led_r)
            resPath = ('Mt_res/%s_LEDR_%1.1f.mat')%(sample_name, led_r)
            # NO initializations: Distant light source #
            _, N_Vari,_ = COMP_Var.COMP_VAR(data_filename, d_init=4.0)
            mat_info = {}
            mat_info['N_Var'] = N_Vari
            sio.savemat(resPath, mat_info)

    # ------- Compare with var nearPS, with different depth inits ---------#
    if 'compare_var_near_ps_inits' == exp_names :
#        data_filename= '../data/sim_input/input_imgs_dinits.mat'
        data_filename= 'simRes/input_imgs_dinits_LEDR_%1.2f.mat'%(LED_RADIUS )
        led_r = LED_RADIUS 
        d_init_range = np.linspace(1, 10, 10).astype(np.float32)
        print('compare_var_near_ps_inits')
        from simulation import COMP_Var
        for dinit in d_init_range:
            resPath = ('simRes/DINITS_VAR_LEDR_%1.2f_dinit_%1.2f.mat')%(led_r, dinit)
            # NO initializations: Distant light source #
            _, N_Vari,_ = COMP_Var.COMP_VAR(data_filename, d_init= float(dinit) )
            mat_info = {}
            mat_info['N_Var'] = N_Vari
            sio.savemat(resPath, mat_info)

            mat_info_gnd = sio.loadmat(data_filename)
            nMap_gnd = mat_info_gnd['nMap']
            nMap_diff= est_NormalDiff(nMap_gnd, N_Vari)
            nMap_diff = np.abs(nMap_diff)
            plt.imsave( 'simRes/DINITS_DistLight_LEDR_%1.2f_dinit_%1.2f.png'%(led_r, dinit), nMap_diff, cmap='jet', vmax=1)

    if 'compare_our_near_ps_inits' == exp_names:
        data_filename= 'simRes/input_imgs_dinits_LEDR_%1.2f.mat'%(LED_RADIUS )
        print(data_filename)
        led_r = LED_RADIUS # 0.8
        d_init_radius = 1.5 
        d_init_range = np.linspace(1, 10, 10).astype(np.float32)
        lambda_It =  .1
        lambda_Idt = 0.2 
        mat_info_input = sio.loadmat( data_filename ) 
        IMGs = mat_info_input['IMGs']
        S = mat_info_input['S']
        K_inv = mat_info_input['K']

        for dinit in d_init_range:
            resPath = ('simRes/DINITS_OUR_LEDR_%1.2f_dinit_%1.2f.mat')%(led_r, dinit)
#            run_nearPS( IMGs, S, K_inv, [dinit, dinit + 10 ], lambda_It, lambda_Idt, resPath, max_Iter_It = 1000)
            run_nearPS( IMGs, S, K_inv, [ np.max([1, dinit- d_init_radius] ), np.min([10, dinit+ d_init_radius]) ], 
                    lambda_It, lambda_Idt, resPath, max_Iter_It = 600, max_Iter_Idt = 600)
            res_mat_info = sio.loadmat( resPath ) 
            nMap_diff = est_NormalDiff( mat_info_input['nMap'], res_mat_info['normal_map'])
            nMap_diff = np.abs(nMap_diff)
            plt.imsave( 'simRes/DINITS_OUR_LEDR_%1.2f_dinit_%1.2f.png'%(led_r, dinit), nMap_diff, cmap='jet', vmax=1)

def exp_Mitsuba_scenes():
    from nearLight import near_ps_multi as nearPS_multi
    # ------ Vgroove rendered by Mitsuba ------ #
    # Can we compensate for the global illumination such as the inter-reflections ? #
#    IMG_file = '../data/mitsu_vgroove/imgs/globalOn/IMGs.mat'
#    IMG_file = '../data/mitsu_vgroove/imgs/globalOff/IMGs.mat'
#    mat_info = sio.loadmat(IMG_file)
#    S = mat_info['S'].copy()
#    K_inv = mat_info['K']
#    print(K_inv)
#    print(np.linalg.inv(K_inv))
#    IMGs = mat_info['IMGs']
#    # # For images generated by Mitsuba, we need to invert the x,y coordinates of S #
#    S[:,0] = - S[:,0]
#    S[:,1] = - S[:,1]
#    print(S)
#    # Optimize in multiple scales #
#    Scales = [4, 1]
#    nearPSMulti = nearPS_multi.NearLightMulti(IMGs, S, K_inv, Scales, fem_d = 1)
#    print('Getting the multiscale images and mask images')
#    nearPSMulti.est_Imask([], if_sel=False, thresh = .01, if_remove_smallC = False)
#    nearPSMulti.build_ImgPyramid()
#    # Begin to optimize #
#    iscale = 0
#    nearPSMulti.set_paras(lam_smooth_Idt = 1, MAX_ITER_Idt = 1, max_Iter_PS_Idt = 300, 
#            lam_smooth_It = 0.0, MAX_ITER_It = 6, dmin=2, dmax=15 )
#    # nearPSMulti.save_paras()
#    nearPSMulti.solve_scale(iscale)

    
    # ---- Mitsuba: bunny, ----- #
    # for far away objects, can we deal with far away object ? #
    led_file = '../data/mitsu_bunny/imgs/S.mat'
    cam_file = '../data/mitsu_bunny/gndTruth/planes/fov38/Calib_Results.mat'
    img_file = '../data/mitsu_bunny/imgs/IMGs.mat'
    led_mat_info = sio.loadmat(led_file)
    cam_mat_info = sio.loadmat(cam_file)
    img_mat_info = sio.loadmat(img_file)
    
    K_inv = np.linalg.inv(cam_mat_info['KK'])
    IMGs = img_mat_info['IMGs']
    # For images generated by Mitsuba, we need to invert the x,y coordinates of S #
    S_bunny = led_mat_info['S']
    S_bunny[:,0] =  -S_bunny[:,0]
    S_bunny[:,1] =  -S_bunny[:,1]
    print('S_bunny = \n')
    print(S_bunny)

    # Build inputs in multiple scales #
    Scales = [4, 1]
    nearPSMulti = nearPS_multi.NearLightMulti(IMGs, S_bunny, K_inv, Scales, fem_d = 1)
    print('Getting the multiscale images and mask images')
    nearPSMulti.est_Imask([], if_sel=False, thresh = .01, if_remove_smallC = False)
    nearPSMulti.build_ImgPyramid()

    # Begin to optimize #
    iscale = 0
    nearPSMulti.set_paras(lam_smooth_It = 1., lam_smooth_Idt = 1., # smoothness
            max_Iter_PS_It = 10000, max_Iter_PS_Idt= 2000,        # gradient descent iteration
            MAX_ITER_It = 6, MAX_ITER_Idt = 2,                    # outer iteration
            dmin= 2, dmax=  15 , drange_export = [3, 10])         # export options

    nearPSMulti.save_paras()
    nearPSMulti.solve_scale(0)
    nearPSMulti.solve_scale(1)

def main():
#    exp_Mitsuba_scenes()
    exp_FaceBody('numerical')
#    exp_FaceBody('compare_distLight_inits')
#    exp_FaceBody('compare_var_near_ps_inits')
#    exp_FaceBody('compare_our_near_ps_inits')
#    parser = argparse.ArgumentParser() 
#    parser.add_argument('--exp', help='Name of experiment' )
#    args = parser.parse_args()
#    print(args.exp)
#
#    exp_FaceBody(args.exp)
    print('DONE EXP !')

if __name__ == '__main__':
    main()
