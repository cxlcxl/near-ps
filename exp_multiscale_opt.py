import numpy as np
import matplotlib.pyplot as plt
import scipy.io as sio
from nearLight import near_ps
from nearLight import initializer as dIniter
from nearLight import near_ps_multi as nearPS_multi
import calibration.calib_lights as calib_lights
from skimage.segmentation import mark_boundaries

import ipdb

# --- In this script,  we will perform the optimization in multiple scales  --- #
# NOTE:
# To deal wit specular regions, we can try to mask out the very bright regions #

# Set the parameters #
nLED = 24
indx_calib_img = 3
Scales = [4, 1]
LED_IMGs_file = '../../data/EXP_data/rw_imgs/checkerboard_ori{}.1117.mat'.format(indx_calib_img)

# Set the input image file #
# IMGs_file = '../data/EXP_data/rw_imgs/baseballman2.1117.mat'
# IMGs_file = '../data/EXP_data/rw_imgs/face_chao_1.1117.mat'
# IMGs_file = '../data/EXP_data/rw_imgs/vgroove.1130.mat'
# IMGs_file = '../data/EXP_data/rw_imgs/mannequin_far_noisy_LED24.1201.mat'
# IMGs_file = '../data/EXP_data/rw_imgs/mannequin_closer_noisy_LED24.1201.mat'
# IMGs_file = '../data/EXP_data/rw_imgs/plumystatue_noisy_LED24.1202.mat'
IMGs_file = '../../data/EXP_data/rw_imgs/shoe_noisy_LED24.1203.mat'
# IMGs_file = '../data/EXP_data/rw_imgs/buststatue_noisy_LED24.1202.mat'
# IMGs_file = '../data/EXP_data/rw_imgs/saladbowl_noisy_LED24.1215.mat'
#IMGs_file = '../../data/EXP_data/rw_imgs/hand_chao_LED24.1215.mat'
# IMGs_file = '../data/EXP_data/rw_imgs/tennisball_noisy_LED24.1215.mat'
# IMGs_file = '../data/EXP_data/rw_imgs/paperbag_noisy_LED24.1215.mat'
# IMGs_file = '../data/EXP_data/rw_imgs/cup_noisy_LED24.1215.mat'

# IMGs_file = '../data/EXP_data/rw_imgs/bowl2_noisy_LED24.1203.mat'
premasking = True

# Info for calibration files #
cam_calib_file = '../../data/EXP_data/calibrations/cameraCalib/setup1/Calib_Results.mat'
led_checker_calib_file = '../../data/EXP_data/calibrations/imgs/LEDs_Checker_setup1/YYs.mat'
Lpos_file = '../../data/EXP_data/calibrations/Lpos_set1.mat'

drange = [100, 1000]


# Load calibration info #
cam_calib_info = sio.loadmat(cam_calib_file)
K = cam_calib_info['KK']
K_inv = np.linalg.pinv(K)
S = sio.loadmat(Lpos_file)['S']
S[:, 1] = -S[:, 1]
S[:, 2] = -S[:, 2]

# Load data #
mat_info = sio.loadmat(IMGs_file)
IMGs = mat_info['IMGs'][:, ::2, 1::2, :]
IMGs = np.dstack((IMGs[0,:,:,:], IMGs[1,:,:,:]))
IMGs = IMGs.astype(np.float)

# --- Uniform the LED intensities --- #
# Load images for LED intensity calibration #
mat_led_info = sio.loadmat(LED_IMGs_file)
LED_IMGs_raw = mat_led_info['IMGs']
LED_IMGs = np.dstack((LED_IMGs_raw[0, ::2, 1::2, :], LED_IMGs_raw[1, ::2, 1::2, :]))
# Calibrate for the LED intensity variations #
Se, calib_X_grid, imgs_rect, pt_int, snorm_int =\
            calib_lights.LED_Uniform_getSEs( LED_IMGs, S, led_checker_calib_file,
                    img_indx = indx_calib_img, if_return_debug_info=True,
                    if_use_fixed_rect = True,
                    fixed_rect = [[550, 394], [570, 430]])
Sem = Se.transpose().reshape((nLED, -1))
Sem_mean = np.mean(Sem, axis = 1)
Sem_mean = Sem_mean / Sem_mean.max()
IMGs_LED_normalized = np.zeros(IMGs.shape)

# Normlizing the LED intensities #
print('Normlizing the LED intensities...')
for iLED in range(nLED):
    IMGs_LED_normalized[:, :, iLED] = IMGs[:,:, iLED] / Sem_mean[iLED]
print('Done')

ipdb.set_trace()

# --- Perform the optimization in multiple scales --- #
# Initialize multi-scale #
nearPSMulti = nearPS_multi.NearLightMulti(IMGs_LED_normalized, S, K_inv, Scales, fem_d = 1)
print('Getting the multiscale images and mask images')

if premasking is False:
	nearPSMulti.est_Imask([], if_sel = True, if_roipoly = True, thresh = 1000,
		remove_spcular  = False,
		thresh_specular = 1400) # thresh =10000 for bust, 2000 for shoe
else:
	# Load pre-defined mash for face #
	face_matinfo = sio.loadmat('tmp_res/premask/myface.mat')
	pre_I_mask = face_matinfo['I_mask']
	nearPSMulti.I_mask_var = pre_I_mask

nearPSMulti.build_ImgPyramid()
print('Done')

ipdb.set_trace()

# Optimize depth in multiple scales #
# nearPSMulti.set_paras(lam_smooth_Idt = 3000.0, MAX_ITER_It = 6, MAX_ITER_Idt = 1, lam_smooth_It = 100.0 )
nearPSMulti.set_paras(\
	lam_smooth_Idt = 1200.0, # 800
	lam_smooth_It = 100.0,
	MAX_ITER_Idt = 5, MAX_ITER_It = 5,
	max_Iter_PS_Idt = 600, max_Iter_PS_It = 800,
	dmin= drange[0], dmax = drange[1])
nearPSMulti.save_paras()
nearPSMulti.solve_scale(0) # Lower res.
nearPSMulti.solve_scale(1) # Higher res.
