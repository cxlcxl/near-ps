import sys, os
#import numpy as np
import numpy as np
import matplotlib.pyplot as plt
import scipy.io as sio
from scipy.spatial import Delaunay
import ipdb



# Add the igl library to the modules search path
sys.path.insert(0, '/usr0/home/chaoliu1/tools/libigl/python')
import pyigl as igl

def unique_rows(A, return_index=False, return_inverse=False):
    """
    Similar to MATLAB's unique(A, 'rows'), this returns B, I, J
    where B is the unique rows of A and I and J satisfy
    A = B[J,:] and B = A[I,:]

    Returns I if return_index is True
    Returns J if return_inverse is True
    """
    A = np.require(A, requirements='C')
    assert A.ndim == 2, "array must be 2-dim'l"

    B = np.unique(A.view([('', A.dtype)]*A.shape[1]),
               return_index=return_index,
               return_inverse=return_inverse)

    if return_index or return_inverse:
        return (B[0].view(A.dtype).reshape((-1, A.shape[1]), order='C'),) \
            + B[1:]
    else:
        return B.view(A.dtype).reshape((-1, A.shape[1]), order='C')




def getTriMap(zmap, I_mask, if_clockwise=False):
    '''
    Get the triangulation map for the input I_mask
    only for pixels with value !=0 
    Input:  zmap - the depth map
            I_mask - input binary mask image
    Output: V, F - the vetex and face matrices
            The first two cols of V are the image coordinates,
            which needs to be scaled: X = (x-x0) * Z / fx, Y = (y-y0) * Z / fy
            fx, fy corresponds to the focal length of the camera
            The last col of V is one, which needs to be scaled by the depth z
    '''
    pt_mask_loc = np.nonzero( I_mask )
    nz_rows = pt_mask_loc[0]
    nz_cols = pt_mask_loc[1]
    pt_mask = np.stack((nz_cols, nz_rows), axis=-1)
    tri_pt_mask = Delaunay(pt_mask)
    F = tri_pt_mask.simplices
    V = pt_mask
    z_v = zmap[nz_rows, nz_cols]
    V = np.hstack((V, np.ones((len(nz_rows),1))))

    if if_clockwise is True:
        F_ = F.copy()
        F_[:,0] = F[:,2]
        F_[:,2] = F[:,0]
        return V, F_
    else:
        return V, F

def get_boundList(I_mask):
    '''
    Get the list of points on the boundary of I_mask
    Inputs:
        I_mask (binary image) - input mask image
    Outputs:
        clist - the list of pixels on the boundary
    '''
    import skimage.measure
    clist = skimage.measure.find_contours(I_mask, .99999999)
    clist = np.round(clist)
    if clist.shape[0]>1:
        print('get_boundList(): WARNING: THERE ARE MORE THAN ONE CLOSED SHAPES')

    clist = clist[0,:,:]
    clist_ = np.zeros(clist.shape)
    clist_[:,0] = clist[:,1]
    clist_[:,1] = clist[:,0]
    # Get rid of duplicated points #
    # u_clist, u_ind = np.unique(clist_, axis=0, return_index=True)
    # u_ind_sort = np.sort(u_ind)
    # clist = clist_[u_ind_sort, :]
    u_list = []
    u_ind = []
    for r, irow in zip(clist_, range(clist_.shape[0])):
        if tuple(r) not in u_list:
            u_list.append(tuple(r))
            u_ind.append(irow)
    u_list = np.asarray(u_list)
    u_ind = np.asarray(u_ind)
    u_ind_sort = np.sort(u_ind)
    clist = clist_[u_ind_sort, :]


    return clist

def IndM_2d_grid(I_mask, dmap,  if_clockwise=False):
    '''
    Get the image coordinates of the pixels inside the mask boundary
    For each pixel, also return its ring-1 neighbors 
    Inputs:
    I_mask - binary mask 
    dmap - the depth map
    if_clockwise = True - if clockwise mesh 

    Outputs:
    UV 3 x npts - npts is the # of observations (used in Ceres), in column major 
    of the image.
    the homogeneous image coordinates for the pixels in the image

    Indx_M 7 x npts -npts is the # of observations (used in Ceres), 
    the pixel indexes (in column major) of the ring-1 neighbors for the observations, 
    including the observation itself: 
    each col of Indx_M: the pixel index of 
    [itself, neighbor_1, ..., neighbor_6]

    Indx_M_var 7 x npts -npts is the # of observations (used in Ceres), 
    the variable indexes (in column major) of the ring-1 neighbors for the observations, 
    including the pixel itself,
    each col of Indx_M: the variable index of 
    [itself, neighbor_1, ..., neighbor_6]

    F - the local face matrix for the ring-1 neighborhood

    F_vars - nFaces x 3 : the F matrix for all the variable vertices. Each row is one face
    
    UV_vars - 3 x nVars  : the UV matrix for all the variable vertices. Each row is one vertice

    z_nz - the depth values for the variables 


    NOTE that all the index is in F-order (col-major)
    '''
    
    indx_nz = np.nonzero(I_mask)
    indx_nz_ = np.ravel_multi_index(indx_nz, I_mask.shape, order='F')
    indx_nz_ = np.sort(indx_nz_)
    indx_nz = np.unravel_index(indx_nz_, I_mask.shape, order='F')
#    ipdb.set_trace()

    I_mask = I_mask.reshape(I_mask.shape, order='F')
    nPixel = I_mask.shape[0] * I_mask.shape[1]

    DeltaCoord = np.zeros((2,7), dtype=np.int32) # the delta coordiante 

    # clockwise face indexing is not supported yet .. 
    assert if_clockwise is False, 'if_clockwise should be False'

    if if_clockwise is False:
        DeltaCoord[0,:] = [0, 0, -1, -1, 0, 1, 1]
        DeltaCoord[1,:] = [0, -1, -1, 0, 1, 1, 0]
    

    npts = indx_nz[0].shape[0]

    COLS, ROWS = np.meshgrid(np.arange(I_mask.shape[1]), np.arange(I_mask.shape[0]))
    UV = np.ones((3, nPixel))
    UV[0,:] = COLS.flatten(order='F') 
    UV[1,:] = ROWS.flatten(order='F') 

#    # normalize the UV coordinates #
#    # TODO: the image center should be the calibrated image center #
#    img_center_ = np.asarray([I_mask.shape[1]*.5, I_mask.shape[0]*.5]).astype(np.float64)
#    img_w_ = np.max([I_mask.shape[0], I_mask.shape[1]]).astype(np.float64)
#    UV[0,:] = (UV[0,:] - img_center_[0]) / img_w_
#    UV[1,:] = (UV[1,:] - img_center_[1]) / img_w_


    Indx_M = np.zeros((7, npts))
    jpix = 0
    for ipix in range(npts):
        x_cord = indx_nz[1][ipix]
        y_cord = indx_nz[0][ipix]
        adj_x_cord = x_cord + DeltaCoord[0,:]
        adj_y_cord = y_cord + DeltaCoord[1,:]

        adj_mask_val = I_mask[(adj_y_cord, adj_x_cord)]

#        UV_pix = np.asarray([[x_cord],[y_cord],[1]])
#        UV[:, ipix] = UV_pix.flatten()

        if np.any(adj_mask_val==False):
            continue
        else: # all non-zero values in the ring-1 neighbor
            Indx_M_pix = np.ravel_multi_index([[adj_y_cord], [adj_x_cord]],
                    I_mask.shape, order='F')
            Indx_M[:, jpix] = Indx_M_pix.flatten() 
            jpix = jpix+1
            


    Indx_M = Indx_M[:, :jpix]
    Indx_M = Indx_M.astype(np.int32)

    # The variables are the depths #
    uni_indxM = np.unique(Indx_M)
    nVars = uni_indxM.shape[0]
    z_nz = np.zeros(nVars) # TODO: the # of vars ~= the # of observed points
    z_nz = dmap[np.unravel_index(uni_indxM, dmap.shape, order='F')]
    varIndx_Map = np.zeros(I_mask.shape, order='F')
    varIndx_Map_return = np.ones(I_mask.shape) * -1.0

    var_pix_loc = np.unravel_index(np.sort(uni_indxM), I_mask.shape, order='F') 
    varIndx_Map[var_pix_loc] = np.arange(nVars) 
    varIndx_Map_return[var_pix_loc] = np.arange(nVars) 

    Indx_M_var = varIndx_Map[ np.unravel_index(Indx_M.flatten(order='F'), I_mask.shape, order='F') ]
    Indx_M_var = Indx_M_var.reshape(Indx_M.shape, order='F')
    Indx_M_var = Indx_M_var.astype(np.int32)

    F = np.zeros((6,3))
    F[:,1] = np.arange(1,7)
    F[:-1,2] = np.arange(2,7)
    F[-1,2] = 1

    F = F.astype(np.int32)


    # Get the F matrix, F_entire,  for the entire scene #
    F_entire = np.zeros(( nVars *6 , 3), dtype=np.int32 )
    for iface in range(Indx_M_var.shape[1] ):
        ivar = iface
        F_entire[ivar*6,   :] = [Indx_M_var[0, ivar], Indx_M_var[1, ivar], Indx_M_var[2, ivar]]
        F_entire[ivar*6+1, :] = [Indx_M_var[0, ivar], Indx_M_var[2, ivar], Indx_M_var[3, ivar]]
        F_entire[ivar*6+2, :] = [Indx_M_var[0, ivar], Indx_M_var[3, ivar], Indx_M_var[4, ivar]]
        F_entire[ivar*6+3, :] = [Indx_M_var[0, ivar], Indx_M_var[4, ivar], Indx_M_var[5, ivar]]
        F_entire[ivar*6+4, :] = [Indx_M_var[0, ivar], Indx_M_var[5, ivar], Indx_M_var[6, ivar]]
        F_entire[ivar*6+5, :] = [Indx_M_var[0, ivar], Indx_M_var[6, ivar], Indx_M_var[1, ivar]]


    # F_entire = np.unique(F_entire, axis=0)
    # ipdb.set_trace()
    # F_unique = []
    # [F_unique.append(tuple(r)) for r in F_entire if tuple(r) not in F_entire];
    F_unique = unique_rows(F_entire)
    F_vars = np.asarray(F_unique)

    # Get the U_vars matrix corresponds to F_vars  #
    UV_vars  = np.ones((3, nVars))
    UV_vars[0,:] = UV[0, np.sort(uni_indxM)]
    UV_vars[1,:] = UV[1, np.sort(uni_indxM)]
    

    UV_obs = UV[:, indx_nz]

    return UV, Indx_M, Indx_M_var,F_vars, UV_vars,  F, z_nz, varIndx_Map_return
            
        
        
        

    
    

def mesh_triangle_2d(I_mask, s=100, if_clockwise=False, if_return_W=False):
    '''
    Inputs: 
    I_mask - binary mask 
    s - the scalar factor, larger -> larger meshes and vice versa
    if_clockwise = False - if clockwise mesh 
    if_return_W = False - if return the weight matrix, which weights the vertices at
    the boundary to be zero
    
    Outputs:
    V, F - (npts x 2), (nfaces x 3) 
    V is the image coordinates of the vertices ; F is the face index
    W (optitional if if_return_W = True) - the weight matrix
    '''
    #Get the list of points on the boundary#
    clist = get_boundList(I_mask) / s 
    npts = clist.shape[0]
    
    #Get the meshes from the the boundary#
    E = np.zeros((npts, 2))
    E[:,0] = np.arange(npts)
    E[:,1] = np.arange(npts)+1
    E[-1,1] = 0
    E = E.astype(np.int32)
    
    V = igl.eigen.MatrixXd(clist)
    E = igl.eigen.MatrixXi(E)
    F = igl.eigen.MatrixXi()
    H = igl.eigen.MatrixXd([])
    
    V2 = igl.eigen.MatrixXd()
    F2 = igl.eigen.MatrixXi()
    
    igl.triangle.triangulate(V, E, H, "a0.001q", V2, F2)
    
    if if_return_W is True:
        # TODO: DONE: 
        # For the vertices on the edge, we should find them out and assign 
        # weight zero to them.
        from scipy.spatial import distance
        import scipy.sparse as sparse
        CD = distance.cdist(np.asarray(V), np.asarray(V2) )
        min_indx = np.argmin(CD, axis=1)
        wv = np.ones(V2.rows())
        wv[min_indx] = 0
        W = sparse.diags( wv, 0)
        W = W.tocoo()

    if if_clockwise is True:
        F2 = np.asarray(F2)
        F2_ = F2.copy()
        F2_[:,0] = F2[:,2]
        F2_[:,2] = F2[:,0] 
        F2 = F2_

    if if_return_W is True:
        return np.asarray(V2) * s, np.asarray(F2), W
    else:
        return np.asarray(V2) * s, np.asarray(F2) 


def Zmap2Nmap(zmap, I_mask, K, if_clockwise=True, mesh_size=100, if_debug=False, if_return_W = False):
    '''
    inputs:
        zmap (w x h): depth map
        I_mask (w x h): mask map where the image intensity is not zero
        K (3 x 3): the inverse projection matrix 
             3D position of point and its image coordinate is related by
             xyz = K[u,v,1]^t
             xyz is 3x1, K is 3x3, [u,v,1]^t is 3x1
        if_clockwise=True: if use the clockwise face vertices indexing
        mesh_size (scalar): the mesh size (=100), note that it should NOT be too small
        otherwise the MCF blur of the mesh will make all values to be nan
        if_debug=False : if debug mode, 
        in the debug mode, the output would be:
        return N, V, F, L, N_vert, ind_nz, UV, KUV_ref
        if_return_W (False): if return the weight matrix
    outputs:
        N, V, F, L, N_vert, ind_nz
        N - (nvertices x 3): the surface normals for the vertices
        V - (nvertices x 3): the 3D locations for the vertices
        F - (nvertices x 3): the face index
        L - (nvertices x nvertices ): the Laplacian-Beltrami Opererator
    '''
    nrow, ncol = zmap.shape
    fx = K[0,0] # the name is confusing, actually 1/fx
    fy = K[1,1]
    x0 = K[0,2]
    y0 = K[1,2]
    
    # Get the vertex and face matrices 
    if if_return_W is True:
        V, F, W = mesh_triangle_2d(I_mask, s = mesh_size, if_clockwise = if_clockwise, if_return_W = True)
    else:
        V, F = mesh_triangle_2d(I_mask, s = mesh_size, if_clockwise = if_clockwise, if_return_W = False)
    
    # Take the rounding of UV coordinates#
    # TODO: is there better method to map the sampled points to the image space
    # rather than rounding ? 
    V = np.round(V)

    ind_V =tuple([np.round(V[:,1]).astype(np.int32), np.round(V[:,0]).astype(np.int32)])

    if if_debug:
        UV = np.round(V.copy())

    zv = zmap[ind_V]
    V = np.hstack((V, np.ones((V.shape[0],1))))

    # This is equivalent to estimate KUV #
    V[:,0] = (V[:,0]) * fx + x0
    V[:,1] = (V[:,1]) * fy + y0 

    if if_debug:
        KUV_ref = V.copy()

    V = V * zv.reshape([V.shape[0],1])

    V = igl.eigen.MatrixXd(V)
    F = igl.eigen.MatrixXi(F)
    
    # Laplace-Beltrami Operator
    L = igl.eigen.SparseMatrixd()
    M = igl.eigen.SparseMatrixd()
    Minv = igl.eigen.SparseMatrixd()
    igl.cotmatrix(V, F, L)
    igl.massmatrix(V, F, igl.MASSMATRIX_TYPE_VORONOI, M)
    igl.invert_diag(M, Minv)

    HN = -Minv * (L * V)
    N_ = HN.rowwiseNormalized()
    H_ = HN.rowwiseNorm()
    N = np.asarray(N_)
    H = np.asarray(H_) # mean curvature

    # assuming the z-coordinates are < 0
    N[np.isnan(N)] = 0.0
    inv_indx = N[:,2] > 0
    N[inv_indx, :] = -N[inv_indx, :]

    #  Get the vertex normal #
    N_vert = igl.eigen.MatrixXd()
    import time 
#    st_time = time.time()
    igl.per_vertex_normals(V, F, igl.PER_VERTEX_NORMALS_WEIGHTING_TYPE_AREA,N_vert)
#    ed_time = time.time()
#    print('{} sec'.format(ed_time - st_time))
#    ipdb.set_trace()
    N_vert = np.asarray( N_vert )

    # The row, col image coordinates for the vertices#
    ind_nz = ind_V # e.g. the depths for the vertices are zmap[ind_nz]
    if if_debug is False:
        if if_return_W is False:
            return N, V, F, L, N_vert, ind_nz
        else:
            return N, V, F, L, N_vert, ind_nz, W
    elif if_debug is True: # Return some debug variables
        if if_return_W is False:
            return N, V, F, L, N_vert, ind_nz, UV, KUV_ref, zv
        else: # Also return the weight matrix
            return N, V, F, L, N_vert, ind_nz, UV, KUV_ref, zv, W



def VF_to_Normal(V_in, F_in):
    '''
    Get the surface normal map from vertex and face maps 
    input
    V, F -  2D np arrays, both with 3 cols for 3D objects 
    The vertex and face matrix for 3D shape 
    output
    NMap - 2D np array, with 3 cols 
    '''

    V = igl.eigen.MatrixXd(V_in)
    F = igl.eigen.MatrixXi(F_in)
    
    # Laplace-Beltrami Operator
    L = igl.eigen.SparseMatrixd()
    M = igl.eigen.SparseMatrixd()
    Minv = igl.eigen.SparseMatrixd()
    igl.cotmatrix(V, F, L)
    igl.massmatrix(V, F, igl.MASSMATRIX_TYPE_VORONOI, M)
    igl.invert_diag(M, Minv)
    HN = -Minv * (L * V)
    N_ = HN.rowwiseNormalized()
    H_ = HN.rowwiseNorm()
    N = np.asarray(N_)
    H = np.asarray(H_) # mean curvature
    return N

def V2Zmap(V, I_mask):
    V = np.asarray(V)
    pt_mask_loc = np.nonzero( I_mask )
    nz_rows = pt_mask_loc[0]
    nz_cols = pt_mask_loc[1]
    Zmap = np.zeros( I_mask.shape)
    Zmap[nz_rows, nz_cols] = V[:,2]
    return Zmap


def Z2V(z, K, UV):
    '''
    from the depth z to vertices index
    input:
        z (npt array) - array of depth
        K (3x3) - the inverse projection matrix for the camera 
        UV (npt x 3) - the image coordinates in homogeneous coordinates
    output:
        V
    '''

    npt = z.shape[0]
    KUV = K.dot( UV.transpose() )
    V = KUV * z.reshape([1, npt ])
    V = V.transpose() # size npt x 3 
    V = np.array(V, order='C')

    return V

if __name__ == '__main__':

    # read the test data #
    dat_fldr = '/home/chaoliu1/PS_BSSRDF/data/mitsu_faces/faces/gndTruth'
    ply_name = 'nearPS_GND_objDist_7_face_00001_20061015_00418_neutral_face05.ply'
    f_path = '{}/{}.mat'.format(dat_fldr,ply_name)
#    f_path = './sphere_gnd.mat'
    mat_info = sio.loadmat(f_path)
    dMap = mat_info['depthMap']
    nMap = mat_info['N']
    I_mask = mat_info['I'].sum(axis=2) > 0
    I_mask = I_mask * ( dMap > 1)

    K = np.eye(3)
    K[0,0] =300.4
    K[1,1] =300.4
    K[0:2,:] = 300.0

    # take the ROI
#    x_range = [245, 359]
#    y_range = [149 ,204]
#    I_mask = I_mask[y_range[0]: y_range[1], x_range[0]: x_range[1]]
 #   dMap = dMap[y_range[0]: y_range[1], x_range[0]: x_range[1]]

    mask_loc = np.nonzero( I_mask )
    m_rows = mask_loc[0]
    m_cols = mask_loc[1]
    dMap[ np.equal(I_mask, False)] = dMap.max()

    # From the ground truth depth map to the surface normal, vertices, and face matrices
    nMap_cal,V,F, L,N_Lap,  N_vert = Zmap2Nmap( dMap, I_mask, K)  
    nMap_cal[:,:,0] =  -nMap_cal[:,:,0]
    ZMap = V2Zmap(V, I_mask)

    # save to mat file #
    mdic={}
    mdic['nMap_cal'] = nMap_cal
    mdic['dMap'] = dMap
    sio.savemat( 'Zmap2NMap.mat', mdic)

    # plot figures #
    plt.figure()
    Zdiff = ZMap
    plt.imshow( Zdiff[m_rows.min():m_rows.max(),m_cols.min():m_cols.max()],
            vmax=5.0, vmin=2.5)
    plt.colorbar()
    plt.savefig('zmapback.png', bbox_inches='tight')

    plt.figure()
    wr_nmap = nMap_cal[m_rows.min():m_rows.max(), m_cols.min():m_cols.max(),:]
    plt.imshow( .5* wr_nmap + .5); 
    plt.savefig('nmap.png', bbox_inches='tight')

    plt.close('all')
    igl.writeOBJ('face.obj', -V, F)
    Va = np.asarray( V)
