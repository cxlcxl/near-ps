import sys
import numpy as np
import matplotlib.pyplot as plt
import scipy.io as sio
import pyigl as igl
import iglhelpers as iglhp
import math
from z2n import Zmap2Nmap as z2n
import ipdb
import math

def est_NormalDiff(nMap1, nMap2):
    '''
    Estimate the surface normal difference between two surface normal maps
    '''
    if np.ndim(nMap1) == 3 and np.ndim(nMap2) ==3:
        #make sure to normalize the surface normal maps first #
#        nMap1 /= np.sqrt(np.sum(nMap1**2, axis=2))[:,:,np.newaxis]
#        nMap2 /= np.sqrt(np.sum(nMap2**2, axis=2))[:,:,np.newaxis]
        # get nDiffMap #
        nDiffMap = np.arccos(np.sum(nMap1 * nMap2, axis=2) )
    elif np.ndim(nMap1) == 2 and np.ndim(nMap2) == 2:
        # each row is an observation #
#        nMap1 /= np.sqrt(np.sum(nMap1**2, axis=1))[:, np.newaxis]
#        nMap2 /= np.sqrt(np.sum(nMap2**2, axis=1))[:, np.newaxis]
        nDiffMap = np.arccos(np.sum(nMap1 * nMap2, axis=1) )

    return nDiffMap

# Same as gs_show_opt2.py, but remove the vertices on the boundary #
# also we add the comparison with ground truth function, when the input
# showing option is 1: show the gnd truth
def zv2Dmap(zv, varIndxMap):
    '''
    From the vector of variables to the depth map
    input:
        zv - the array of depth variables
        varIndxMap - the variable index map, the index is in column-major

    output:
        dMap
    '''

    varIndxMap = varIndxMap.astype(np.int32)
    dMap = np.zeros(varIndxMap.shape)
    assert( varIndxMap.max()+1 == zv.shape[0])

    nPix = dMap.shape[0] * dMap.shape[1]

    for ipt in range(nPix):
        loc_ = np.unravel_index(ipt, dMap.shape, order='C')
        if varIndxMap[loc_] > -1:
            dMap[loc_] = zv[varIndxMap[loc_]]
    #dMap[np.nonzero(varIndxMap+1)] = zv
    return dMap

def nv2Nmap(nM, varIndxMap):
    varIndxMap = varIndxMap.astype(np.int32)
    Nmap = np.zeros((varIndxMap.shape[0], varIndxMap.shape[1], 3))
    H, W, C = Nmap.shape
    nPix = Nmap.shape[0] * Nmap.shape[1]
    for ipt in range(nPix):
        loc_ = np.unravel_index(ipt, [W, H], order='C')
        if varIndxMap[loc_] > -1:
            for ich in range(3):
                Nmap[loc_[0], loc_[1],ich] = nM[varIndxMap[loc_], ich]
    return Nmap 


def rmF(F, varIdxRm):
    mask = np.ones((F.shape[0]))
    for idx_ in range(3):
        x = varIdxRm
        y = F[:, idx_]
        index = np.argsort(x)
        sorted_x = x[index]
        sorted_index = np.searchsorted(sorted_x, y)
        yindex = np.take(index, sorted_index, mode="clip")
        mask *= (x[yindex] != y).astype(np.float)

    mask = mask.astype(np.bool)
    F_new = F[mask,:]
    return F_new


def VF_rmBoundary(V, F, varIdxMap, I_mask, erodeIter=5):
    '''
    Remove the vertices on the mask boundary 
    inputs; V, F - nVert x 3 and nFace x 3 matrices 
            varIdxMap - same size as the image. The variable index map
            I_mask - mask image
    output: Vr, Fr - nVert x 3 and nFaceNew x 3 matrices
            nFaceNew < nFace since vert on the boundar are removed 
    ''' 
    varMap = varIdxMap.copy() 
    varMap += 1
    import scipy.ndimage.morphology as sci_morph
    I_maske = sci_morph.binary_erosion(I_mask, iterations= erodeIter)
    I_mask_rm = I_mask - I_maske
    # Get the variable index for the vertices to remove from visualization
    varIdxRm = []
    for irow, icol in zip(np.nonzero(I_mask_rm)[0], np.nonzero(I_mask_rm)[1]):
        # pix_cord = np.unravel_index(img_indx, I_mask_rm.shape) 
        varid_pix = varIdxMap[irow, icol]
        varIdxRm.append(varid_pix)

    varIdxRm = np.asarray(varIdxRm)
    F_rm = rmF(F, varIdxRm)
    return V, F_rm, I_maske 

def VF_rmBoundary_angle(V_, F_, vRange):
    '''
    Remove the vertices on the mask boundary 
    inputs; V, F - eigen Matrices 
            [min, max ]- min max of angles
    output: Vr, Fr - nVert x 3 and nFaceNew x 3 matrices
            nFaceNew < nFace since vert on the boundar are removed 
    ''' 
    K_ = igl.eigen.MatrixXd()
    igl.internal_angles(V_, F_, K_)
    K_a = iglhp.e2p(K_)
    indx_valid  = (K_a[:,0]<vRange[1]) & (K_a[:,1]<vRange[1]) & (K_a[:,2]<vRange[1])
    indx_valid *= (K_a[:,0]>vRange[0]) & (K_a[:,1]>vRange[0]) & (K_a[:,2]>vRange[0])
    F_p = iglhp.e2p(F_)
    F_p = F_p[indx_valid, :]
    F_rm = iglhp.p2e(F_p)
    return V_, F_rm

#------------------ MAIN ------------------------ #

inputfile   = sys.argv[1] 
if_show_gnd = 0

# mesh angle range #
mesh_angle = [0, math.pi]
# remove boundary pixels #
erode_Iter =  5

mat_info = sio.loadmat(inputfile)
varIndxMap = mat_info['varIndx_Map']

F_vars = mat_info['F_vars']
KUV_vars = mat_info['KUV_vars'].transpose()
K = mat_info['K']
I_mask = mat_info['I_mask']

if int(if_show_gnd) == 1: # show gnd 
    print('z_gnd')
    z_gnd = mat_info['z_gnd'].flatten()
    zMap_gnd = zv2Dmap(z_gnd, varIndxMap)
    V = KUV_vars * z_gnd.reshape(z_gnd.shape[0], 1)

    z_cur = mat_info['z_cur'].flatten()
    zMap_cur = zv2Dmap(z_cur, varIndxMap) 
    V_cur = KUV_vars * z_cur.reshape(z_cur.shape[0], 1)


elif int(if_show_gnd) == 2: # show initialization 
    print('z_init')
    z_init = mat_info['z_init'].flatten()
    V = KUV_vars * z_init.reshape(z_init.shape[0], 1)

elif int(if_show_gnd) == 0: # show the current results 
    print('z_cur');z_cur = mat_info['z_cur'].flatten()
    V = KUV_vars * z_cur.reshape(z_cur.shape[0], 1)
    print('Median z = {}'.format(np.median(z_cur)))

else:
    raise ValueError('unknown parameter for showing option = %d'%(int(if_show_gnd)))

# Remove the vertices on the boundar #
_, F_rm, I_mask_e = VF_rmBoundary(V, F_vars, varIndxMap, I_mask, erodeIter=erode_Iter)
F_vars = F_rm
V_ = np.zeros(V.shape, order='C')
V_[:] = V
F_ = np.zeros(F_vars.shape, order='C', dtype=np.int32)
F_[:] = F_vars.astype(np.int32)
V_e = iglhp.p2e(V_)
F_e = iglhp.p2e(F_)
V_ee, F_ee = VF_rmBoundary_angle(V_e, F_e, mesh_angle)

if int(if_show_gnd) ==1: # compare with current estimation 
    F_vars = mat_info['F_vars']
    _, F_rm, I_mask_e = VF_rmBoundary(V_cur, F_vars, varIndxMap, I_mask, erodeIter=erode_Iter)
    F_vars = F_rm
    V_ = np.zeros(V_cur.shape, order='C')
    V_[:] = V_cur
    F_ = np.zeros(F_vars.shape, order='C', dtype=np.int32)
    F_[:] = F_vars.astype(np.int32)
    V_e = iglhp.p2e(V_)
    F_e = iglhp.p2e(F_)
    Vcur_ee, Fcur_ee = VF_rmBoundary_angle(V_e, F_e, mesh_angle)

    Ncur_vertices = igl.eigen.MatrixXd()
    igl.per_vertex_normals(Vcur_ee, Fcur_ee, 
            igl.PER_VERTEX_NORMALS_WEIGHTING_TYPE_AREA, Ncur_vertices)
    Ncur_vertices = iglhp.e2p(Ncur_vertices)

    Ngnd_vertices = igl.eigen.MatrixXd()
    igl.per_vertex_normals(V_ee, F_ee, 
            igl.PER_VERTEX_NORMALS_WEIGHTING_TYPE_AREA, Ngnd_vertices)
    Ngnd_vertices = iglhp.e2p(Ngnd_vertices)

    Ngnd_vertices[np.isnan(Ngnd_vertices)] = 0. 
    Ncur_vertices[np.isnan(Ncur_vertices)] = 0. 

    Ngnd_map = nv2Nmap(Ngnd_vertices, varIndxMap)
    Ncur_map = nv2Nmap(Ncur_vertices, varIndxMap)
    


viewer = igl.viewer.Viewer()
viewer.data.set_mesh(V_ee, F_ee) 
# Hide wireframe
viewer.core.show_lines = False
#viewer.core.background_color.setConstant(1);
#viewer.core.background_color = iglhp.p2e(np.array([1.0, 1.0, 1.0, 1.0]))
#mesh color #
color = igl.eigen.MatrixXd([[.8, .8, .8]])
# viewer.data.set_colors(color)
viewer.launch()

# Print the difference between the gnd truth and the current estimation #
#diffMap = est_NormalDiff(Ngnd_map, Ncur_map)
#print('\n \n\n')
#print('---The mean error in surface normal in degrees is :%2.3f ----- '%\
#        (np.sum(diffMap * I_mask_e.astype(np.float)) / np.sum(I_mask_e) / 3.14159 * 180.))



# write mesh into ply file #
#plyfname = './res.ply'
#igl.writePLY(plyfname, V_ee, F_ee)
